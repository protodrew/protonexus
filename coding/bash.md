---
type:
tags: 
creation-date: 2023-07-19
modification-date: 2023-10-23
---
# Bash

miscellaneous bash notes and snippets

## Set

### -x
`set -x` allows for line by line output

example

```bash
set -x
echo Hi
echo Hello World
```

output

```
+ echo Hi
Hi
+ echo Hello World
Hello World
```

### -eo Pipefail

exists script on a failure either internally or from a pipeline, good as a safety

## Variables

backticks \`\` around a variable declaration will execute it as a command

```bash
name=`uname`
echo `$name
# output is result of uname command ex: "Linux"
```
