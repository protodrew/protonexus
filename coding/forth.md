---
type:
tags: [programming, concatenative_programming, forth]
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# Forth

see [Forth](https://concatenative.org/wiki/view/Forth) for definition

## Cheatsheet

### Words

: word *content* ;

	examples

	: star 42 emit ; (prints * to stdout)

	: stars 0 do star loop ; (prints stars equal to the previous stack number to stdout)

	: greet .” hello world” ; (prints the string to stdout)

> - Every : needs a ;
> - Every ." needs a "
*rules from chapter 1 of [starting forth](https://www.forth.com/starting-forth/)*

**if a word is defined with a word (word 2) in it's definition, that version will be compiled to the dictionary and will not be changed if the definition for word 2 is changed later in the program**