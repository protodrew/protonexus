---
type:
tags: 
creation-date: 2023-11-20
modification-date: 2023-12-26
---
# [[how_to_quickly|How to Quickly]] Set Your Default File Browser
## From the Commandline

using `xdg-mime default [.DESKTOP NAME] inode/directory` will quickly ensure all applications are using the correct file manager as their backend (some apps were opening folders in vscodium for me).

For Nautilus (my graphical file browser) the command is

```bash
xdg-mime default org.gnome.Nautilus.desktop inode/directory
```
