---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# [[how_to_quickly|How to Quickly]] Extract Image from Site Favicon

I have a collection of bookmarks I have displayed on my start page¹. Frequently I need to grab the icon for a new site, so if a website doesn't have a lot of pictures, hit f12 and search for `icon`. You may get different resolution options from there.

1 [via perfecthome](https://addons.mozilla.org/en-US/firefox/addon/perfect-home/)