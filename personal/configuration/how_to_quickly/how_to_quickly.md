---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-12-26
---
# How To Quickly

This is a collection of little tricks I've found myself needing to do more than once, so I've stored them for future reference

[[extract_image_from_favicon]]

[[restore_btrfs_snapshot_with_recovery]]

[[remove_times_from_kobo_highlights]]

[[set_default_file_manager]]