---
type: note
tags:
  - configuration
  - technology
creation-date: 2023-06-16
modification-date: 2023-10-20
---
# Hacking My Kobo Clara HD
## Helpful Links

[Browsing Gemini Sites on Kobo](https://letsdecentralize.org/tutorials/kobo-terminal.html)

[Basic Hacks for Kobo E-Readers](https://www.linux-magazine.com/Online/Features/Basic-Hacks-for-Kobo-E-Readers)

[Hacking my Kobo Clara HD](https://anarc.at/hardware/tablet/kobo-clara-hd/)

[Kobo Hacks and Utilities Index](https://www.mobileread.com/forums/showthread.php?t=295612)

**Kelbot Gemini to Epub Guide**

gemini://gemini.cyberbot.space/gemlog/geminitoepub.gmi