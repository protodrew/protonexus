---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Alias'

from [[../../works/moonspider/configuration]]

Here is a list of useful ```fish-functions``` that allow for me to interface with my terminal in a more useful day

# Terminal Greeting

```fish
fish_greeting - sets useful parameters I find useful, included dynamically creating some alias'

function fish_greeting
	todoist sync
	clear
	neofetch
	
	alias vim 'nvim'
	alias todoist '~/bin/./todoist_linux_amd64'
end
```

# General Use

```fish
# Up - shorthand for updates with both flatpak and apt

function up
    sudo apt update
    sudo apt upgrade
    flatpak update
    sudo apt autoremove
    sudo apt autoclean
end

```

## Todoist Related

```fish
# add-task - Adds a task to todoist-cli using the word or phrase surrounded in ' '

function add-task
    todoist q $argv
end
```

```fish
# complete-task - Complete task in todoist, pass item ID as argument

function complete-task
    todoist close $argv[1]
end
```

```move-task
# move-task - moves task to given time, takes item id and date

function move-task
    todoist modify $argv[1] -date $argv[2]
end
```

```fish
# list-tasks - lists tasks in fish, requires date or common  natural date signifiers as argument

function list-tasks
    todoist list -f $argv
end

```