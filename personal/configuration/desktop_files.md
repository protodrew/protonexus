---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# .Desktop Files

.desktop files are files that point towards an executable and provide some metadata in order to allow the system launcher to use them. To make a custom one, make a new file in `~/.local/share/applications/` with the name you want ending in .desktop, here is my base config.

```
[Desktop Entry]
Name=
Encoding=UTF-8
Version=0.0.0
Comment=NO COMMENT ENTERED
Icon=
Terminal=false
Type=Application
Keywords= Keyword 1;Keyword 2;
Categories=Cat 1;Cat 2;
Exec=PATH TO BINARY
```