---
type:
tags: 
creation-date: 2023-12-05
modification-date: 2023-12-27
---
# The Deboard-Buendia Program

things to do to escape the mundane and model the transcendental in your day to day life

1. upon waking attempt to record a dream, invent one otherwise
2. when walking notice nothing
3. when sitting, notice the hollow weight of the day and ask yourself where you'd rather be, ask yourself if there is a way to rapidly construct where you'd rather be immediately, ask yourself what you can do in the meantime
4. 1 hour after eating meals, reconcile your bodies parasympathetic dreaming with your bodied fore-brained rationality through expressive action
*(visual art, writing, audio, hooting/howling, drilling with a firearm, sketching the shape of the space you inhabit)*
5. when entering your home, move directly to your room and mark your intuitive sense of the world outside around in ink on the floor by the bed
*your brain is reconciling its shift between working, existing in public, and existing in private. Why aren't you doing the same?*
6. consume a piece of challenging dreamwork every day