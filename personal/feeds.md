---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# RSS Feeds

Ones marked with a ★ are minimal or no pictures or aren't reliant on offline links (I like to read them on my [hacked kobo Clara](https://anarc.at/hardware/tablet/kobo-clara-hd/)¹

[Public Domain Review](https://publicdomainreview.org/rss.xml)

[Critical Distance](https://critical-distance.com/feed/)

[meow — ctrl-c.club/~nat](https://ctrl-c.club/~nat/meow/rss.html)

[Candybox (Nathalie Lawhead)](http://www.nathalielawhead.com/candybox/feed)

[Analog Office](https://analogoffice.net/feed.xml) ★

[Theofuturism](http://theofuturism.substack.com/feed) ★

[Experimental History](http://experimentalhistory.substack.com/feed) ★

[Interconnected](https://interconnected.org/home/feed) ★

[Raptitude](https://raptitude.com/feed) ★

[Bandcamp Daily](https://daily.bandcamp.com/feed)

[Ribbon Farm](https://ribbonfarm.com/feed/) ★

[Winnie Lim](https://winnielim.org/feed/) ★

[Low Tech Magazine](https://lowtechmagazine.com/atom.xml) ★

[cblgh](https://cblgh.org/articles.xml) ★

[Oatmeal (eli.li)](https://eli.li/feed.rss) ★

[The Digital Antiquarian](https://www.filfre.net/feed/) ★

[Tedium: The Dull Side of the Internet.](https://feed.tedium.co/)
¹ article not by me