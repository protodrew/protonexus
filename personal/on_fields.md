---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# On Fields
People expect their fields of interest to be multifascited. This is largely a result of things like the major system in education that railroads people into a singular mode of understanding. This becomes a problem especially for STEM and business people who are now expecting their field to not only provide knowledge on the mechanics of their job, but also a lens with which to view the world. Society is complex and made up of many layers and you can't reduce that to one field without losing sight of reality
