---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-12-26
---
# Constructing the Foundation

*Last year I made do with what I had, this year I will build what I need.*

## What This means to Me

- Focus more on the parts of myself I like, and how to expand them, over the parts I don't and how to cut them out
- Build good habits rather than trying to break bad ones
- Set realistic goals and be ok with failing
- Building the parts of myself I want to maintain going forward

## What This means in the Concrete

- [ ] finding and establishing a relationship with a therapist
- [x] reading more books, journals, articles, and papers
- [x] expanding my obsidian vault to more areas of knowledge and interest
- [Start Often Finish rArely](https://tilde.town/~dozens/sofa/)

As a contradiction to point 1, I want to start cutting out the parts of myself that allow me to create more. Imposter syndrome, etc.

### Checkpoint

It's a bit over halfway through the year, so I wanted to write a little progress report on how I have felt this year, and some of the things I have accomplished that I am proud of.

The biggest thing that happened this year was my decision to take a break from school for a while. I'm not sure if I'll return to the college I was at, or what the future holds, but I am glad that I made the decision to take a break. The hypercompetitive environment combined with me trying to get a hold on depression & anxiety led to me doing far worse than I could have. I decided to take this break to get a job, so I can get some savings, and work on personal projects that I think would allow me to get into the industries I want to be a part of.

This year has been a great year for my notes and general learning. I've been pushing my knowledge in C, general Linux usage, and Godot to prepare for a new larger project. One thing I want to do in the back half of the year is make some games, so I can populate my Itch with some fun experiments and tests.

My mental health has had its ups and downs, but I do think that I'm going in the right direction. I have had a few sessions with a new therapist, but need to get a more consistent schedule set up. I'm trying to meditate more often, and recognize when I get overwhelmed and need to take some time to decompress. The biggest hurdles I want to hovercome are my fear of failure that makes finishing projects difficult, and my imposter syndrome that discourages me from stepping out of my comfort zone.

### Writing I Did Abt This

[[i_dont_care_whos_watching]]

[[authenticity_imposter_syndrome_and_enjoying_things]]

[[reflections_on_burnout]]

[[fighting_entropy_and_resisting_the_urge|fighting_entropy_and_restisting_the_urge_to_start_over]] (fragment)

#### [Relevant are.na channel](https://www.are.na/_protodrew/constructing-the-foundation)
