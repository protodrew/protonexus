---
type: note
tags:
  - music
  - personal
creation-date: 2023-01-01
modification-date: 2023-12-31
---
# Albums/EPs/Mixtapes/Compilations That I Liked 2023

| Name | Artist | Release Year | Genre |  |
| ---- | ---- | ---- | ---- | ---- |
| In A Silent Way | Miles Davis | 1969 | jazz |  |
| The 7th Hand | Immanual Wilkins | 2022 | jazz |  |
| In the Spirit of Ntu | Nduduzo Makhathini | 2022 | jazz |  |
| Full Life Consequences | Ur New Favorite War Criminals | 2021 | glitch rap |  |
| Spooky | Lush | 1992 | shoegaze |  |
| Material Fantasies | Yah Wav | 2020 | etherial hyperpop |  |
| The G.A.T (Gospel According To) | Mach-Hommy | 2017 | jazz rap |  |
| There Are Strings | Spring Heel Jack | 1995 | dubnb |  |
| Softly… | Luiz Bonfá | 1966 | bossa nova |  |
| Another Green World | Brian Eno | 1977 | ambient post-rock |  |
| Evening Time | Jackie Mittoo & The Soul Vendors | 1968 | downtempo rocksteady |  |
| Original Pirate Material | The Streets | 2004 | alternative spoken word |  |
| Small Town Smoker | Color Cassette | 2023 | experimental ambient |  |
| Star Escalator | Sensorama | 1998 | deconstructed house |  |
| Let's Start Here | Lil Yachty | 2023 | psych rock |  |
| Honey | Samia | 2023 | alternative pop |  |
| Mortal | Blackwinterwells | 2023 | hyperpop |  |
| Between All Things | OHMA | 2022 | ambient pop |  |
| Scaring The Hoes | Danny Brown x JPEGMAFIA | 2023 | experimental rap |  |
| 10,000 Gecs | 100 Gecs | 2023 | uhm |  |
| Music from Patch Cord Productions | Mort Garson | 2020 | electronic |  |
| Yes Lawd! | NxWorries | 2016 | hip hop, soul |  |
| Our Bones Are Artifacts | Gao the Arsonist | 2022 | experimental rap |  |
| Paint the World | Chick Corea Electric Band II | 1993 | electronic jazz |  |
| Mother Earth's Plantasia | Mort Garson | 1970 | electronic |  |
| Who Will Cut Our Hair When We're Gone? | The Unicorns | 2014 | indie |  |
| Another Blue | Crosslegged | 2023 | alternative |  |
| sf44 | quinn | 2023 | digicore |  |
| It's Almost Dry | Pusha-T | 2022 | rap |  |
| Ambient 2: The Plateaux Of Mirror | Harold Budd, Brian Eno | 1980 | ambient |  |
| Running as Fast as I can | William Crooks | 2022 | experimental rap |  |
| I Can't Wait | Tia Corine | 2022 | rap |  |
| Friends That Break Your Heart | James Blake | 2023 | indie rnb |  |
| Gary Hates Car Culture | Venetian Snares | 2019 | breakcore |  |
| Wave | Antonio Carlos Jobím | 1967 | Bossa Nova |  |
| BLP Kosher and the Magic Driedel | BLP Kosher | 2022 | rap |  |
| ultratronics | Ryoji Ikeda | 2022 | noise, deconstructed club |  |
| secret life | Fred Again.., Brian Eno | 2023 | ambient pop |  |
| TW 2052 | KayCyy | 2023 | experimental rap |  |
| F65 | IDK | 2023 | rap |  |
| Leather Blvd. | B. Cool-Aid | 2023 | neo-Soul |  |
| Finally Lost | 222 | 2023 | cloud rap |  |
| 2 | 2hollis | 2023 | electrorap |  |
| Crossover (Deluxe) | Dua Saleh | 2022 | experimental rnb |  |
| You Can't Sit With Us | PIVOT Gang | 2019 | rap |  |
| Interstate185 | quinn | 2023 | digicore |  |
| Lowkey Superstar | Kari Faux | 2020 | hip hop |  |
| 1977 | µ-Ziq | 2023 | ambient techno |  |
| Dealer's Choice | Dealers of God | 2023 | uhh |  |
| All American | Redd40 | 2023 | underground rap |  |
| Reaching for the Stars | The Whatnauts | 1971 | soul |  |
| sxmneydrgs | lucki | 2023 | underground rap |  |
| Only Diamonds Cut Diamonds | Vegyn | 2019 | idm |  |
| Baddow Moods | Ceephax Acid Crew | 2022 | acid idm |  |
| Marshmallow | The Sweet Enoughs | 2020 | lounge |  |
| giratinightcore: emerald | Gingus | 2022 | dariacore |  |
| QUEEF UNDERGROUND | QUEEF JERKY | 2023 | experimental comedy rap |  |
| Tribes of Da Underground Vol.4 | Various | 1998 | acid jazz |  |
| Maps | Billy Woods & Kenny Segal | 2023 | abstract hip hop |  |
| How Do You Sleep At Night? | Teezo Touchdown | 2023 | RocknB |  |
| MYRTLE BROADWAY & THE BIG BANG THEORY | Can of Bliss | 2023 | hyperpunk |  |
| THE MOST UNKNOWN MIXTAPE | triplesixdelete | 2022 | aggro electronic |  |
| Easy Listening | Quickly, Quickly | 2023 | indie pop |  |
| Fanfare | Dorian Electra | 2023 | hyperpop |  |
| All American 2 | Redd40 | 2023 | underground rap |  |
| Quiet Music For Young People | Dana & Alden | 2023 | jazz |  |
| FoolHardy | Cr1tter | 2023 | underground rap |  |
| Underdog Tale | Cr1tter | 2023 | underground rap |  |
| Sausalito | Divorce From New York | 2022 | broken beat |  |
| Traditional Synthesizer Music | Venetian Snares | 2016 | breakcore |  |
| End of a Line or Part of a Circle | Tristan Arp | 2023 | experimental techno |  |
| Again | Oneohtrix Point Never | 2023 | experimental electronic |  |
| Eli & Harry | The Breathing Effect | 2023 | jazz fusion |  |
| Poisoner | Cr1tter | 2023 | underground rap |  |
| Songs Before Bed | Otto Benson (formerly pudding club) | 2022 | ambient pop |  |
| daydreamer | nour | 2023 | indie pop |  |
| Animus | Oblique Occasions | 2023 | barber beats |  |
| The Curse of Colonialism III | modest by default | 2023 | barber beats |  |
| In Parallel | Salamanda | 2023 | experimental ambient |  |
| New Blue Sun | Andre 3000 | 2023 | nuage |  |
| Spirits | The Circling Sun | 2023 | jazz |  |
| Minari | Bokoya & Gianni Brezo | 2023 | jazz fusion |  |
| The Self Elastic | Tristan Arp | 2023 | nuage |  |
| Plays Music | The Music Quintet | 2022 | jazz |  |
| Psyché | Psyché | 2023 | psychedelic |  |
| Palace of a Thousand Sounds | The Whatitdo Archive Group | 2023 | jazz fusion |  |
| And Then You Pray For Me | Westside Gunn | 2023 | rap |  |
| Move San Let Gate | TWEAKS | 2023 | experimental |  |
| Older Now EP | TWEAKS | 2021 | experimental |  |
| #1 KO Queen | KO Queen | 2023 | hardcore |  |
| space for snow | cr1tter | 2023 | underground rap |  |
| Americana | DJ Lucas | 2023 | rap |  |
