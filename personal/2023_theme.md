---
tags:
  - theme
  - mental_health
creation-date: 2023-07-02
modification-date: 2023-12-27
type: note
aliases:
  - tending the garden
---
# 2023_theme
2# Tending the Garden

*2023 will be the year I focus on positive development*

## What This means to Me

I have envisioned my life almost entirely as a series of goals, and every other component of my life is tertiary. This year, I want to deprioritize goals in favor of thinking about areas I would like to improve, and moving towards them. This ties into and extends my year of [[2022_theme|constructing the foundation]] by way of building positive habits and taking the time to observe what I want to do with my time and prioritizing it that way.

I want to set short term goals when it comes to my professional life and execute them reasonably. I might look into some alternate to-do systems, either within todoist or other applications.

## What This means in the Concrete

Now that I have built journaling into my life. I want to integrate it into my day more, doing a couple of checkpoints throughout the day, rather than just when I'm about to go to bed. I'm not always the most verbose when I am abt to go to sleep, and I feel like sometimes things can get left out because I don't feel like writing them.

A nice mantra I have been keeping in my mind while I've been living alone is "if there is something small I can do for future me, do it now". It helps as a framing tool to recontextualize chores into an act of self-compassion and caring in a way that helps me feel better and get necessary things done even when I'm struggling with depression.

### March Update

I feel like I haven't met the goals I've set out for myself yet. I've been really lost the past year or two, but this year especially I feel like I am being pulled in 100 directions by all of the thngs I want to do.

It's time to set some concrete goals to improve myself.

- on days where I don't work, go on a walk/bike ride or at least sit on the porch for a while
- ~~try to wake up and jot down my thoughts in my journal first thing.~~ this never really stuck
- reduce eating out when not with friends or for some kind of event.

It's been really hard to find a discipline to follow, as soon as I find something I feel like I am either entranced by something newer and shinier, realize I have no real reason to learn, or I find that it is harder than I expected and lose interest. While reading around I found this article [How to Commit to Learning a Language When You Can't Do It Regularly](https://www.lindsaydoeslanguages.com/how-to-commit-to-learning-a-language-when-you-cant-do-it-regularly/). Seemed like a similar problem to the one I am having of finding it difficult to regiment any kind of learning at the moment. The key ideas here are

- set wider time-goals and check ins
This will be a tricky one because I'm currently setting no time goals, when I'm learning something new I just dive into it until I get bored and then move on. I think I will need to spend some considerable time re-learning how to learn.

- count everything you do
This is a mindset thing I haven't tried before, mestly because I have a tendency to always be hard on myself and find reasons to discount my work. Going to add a section to my journal called "what I did", to encourage listing these more often.

### May Update

two months later and I feel like I am making some progress, I'm making the decision to table some of my long projects for a while (going to resume really working on remnant constructivist either on vacation or when I really want to), and recontextualizing how I view myself.

Had a nice talk with Nathan about improving ourselves, and I've realized how I've had a lot of things that are long-term and short-term mixed in my head. She mentioned how one of our mutual co-workers has a punnett square for internal vs external struggles and short vs long term needs. For example making sure you eat breakfast is internal and short term, while wanting to exercise more is internal and long term. It's an interesting mental system, and I want to see if I can integrate any of these ideas into my life.

I've come back to the garden metaphor, thinking of improving my life not as trying to instill habits into my brain from some unknown willpower, to asking as a nuturer. Bodies and lives aren't sculptures, it's futile to try and shape them to your exact desires (which if you are growing will probably be vastly different from when you started). I view my life as a series of branches on a bonsai tree or plots in a garden: maintaining them will help them grow to their fullest potential, but I cannot predict how they will mature.

### August Update

created a personal [[goals]] document to track some short and long term goals. I'm categorizing them into Professional / Academic, Personal / Artistic, and Personal / Human. I want to make progress in all aspects of my life and that is going to start to require better planning. Reading [[how_to_do_nothing]] was instrumental in helping me rethink my internal personal values system, and I hope to implement a slower more deliberate lifestyle for the rest of the year.

### November Update

Coming to the end of November, and I'm starting to reflect on the year as a whole and thinking about what I want to focus on next year. The past few months have been busy, between getting a cat, applying for a full time position at the museum, and trying to rediscover my love for creating music (first in Ableton, now I'm back to [[bespoke_synth|bespoke]]). I'm really proud of the amount of stuff I've gotten done and the place I am mentally.

Going to DIY shows has been a huge motivator, and helped me figure out the communities I want to be a part of in the long term. Making music again has been really rewarding, and I'm hoping to play a show at my local venue some time early next year. I love how much music I've discovered this year. Between all the [[albums_i_liked_2023|albums]], mixes, shows, and artists I've gotten to listen to, it has been easy to get inspired and find new sounds to love. I have this little thought pattern I've noticed recently where I get a bit sad sometimes when listening to a good song because I feel like I can't make something that good ever. It's obviously dumb and has been something I've been deconstructing over the past few months, but it is there and continues to be something in the back of my mind.

The thing I'm the most proud of getting a handle on is the speed of my life. I don't feel as constantly pressured to be making or doing things outside what I need to do, and the things I need to do don't feel like pointless obstacles. Been embracing the minutiae of everyday life more and finding a lot more peace in my life. My sleep schedule has been a lot better, and it's been helping my anxiety and energy levels a lot.

I didn't realize how much my mindset needed to change to heal from burnout. The culture around college and work is so unbelievably toxic, that there were multiple times I thought I was "out of the weeds" on it only to find myself depressed and demotivated within a few weeks. I now know that I'll probably never heal from burnout completely, especially while my desire to make games has all but evaporated for the time being. That being said, I'm happy with my life and see a path to a future, and that's all I can really hope for.

### Related Writing I Did

- [[growing_in_the_wrong_places]]
- [[i_didnt_want_to_make_anything]]
- [[reflections_on_burnout]]
- [[authenticity_imposter_syndrome_and_enjoying_things]]

## [Relevant are.na channel](https://www.are.na/protodrew/tending-the-garden)

