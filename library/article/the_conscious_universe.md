---
tags: [philosophy, culture, metaphysics, spirituality]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# The Conscious Universe

## Joe Zadeh

### [source](https://www.noemamag.com/the-conscious-universe/)

---

> Consciousness didn't emerge or flicker into existence; it has always been there — the intrinsic nature of us and everything around us

*The Conscious Universe* explores the inception and growth of Panpsychism, the theory that all matter in the universe has consciousness. The theory is one that existed as far back as human beings existed, encouraged pagan integration with nature, and seeing oneself as a larger part of the natural world. The death of this theory came with the revolution of monothestic religions (namely christianity), which saw Man as above nature, and matter as inert.

Science continued to work with this framework, and remained relatively static in its idea of matter as made up of inert building blocks, first particles, then atoms, now quarks. However, as we get down to the constants of the universe, it becomes more difficult to describe what things actually are.

> Physical science only tells us what stuff does, not what stuff is. It's not telling us the underlying nature of the stuff that is behaving in this way.
> — *Philip Goff*

Electrons merely being the set of properties that we ascribe to electrons makes it difficult to understand the birth of undeniable consciousness within the universe. This becomes more complicated as our understandings of what quantifies intelligence develop. From being the exclusive domain of Man and higher animals, scientists have observed bees making collective decisions, slime mold prefer certain foods to others, and pea plants are able to grow towards the sound of water and communicate its location to nearby plants.

Modern panpsychism as an ideology attempt to quantify this in the current model of the world not by upending fundamental understandings, but suggesting that we are looking at the model in the wrong way. It does not attempt to tell you that the rocks can feel or that the air can hear, but rather view growth, change, and memory (genetic or individual), as responses to stimuli that exist within all organic matter.

This becomes especially useful in the modern era, where the threat of climate change paralyzes us without the proper framework to think of ourselves as a part of nature and the world.

> By destroying pagan animism, Christianity made it possible to exploit nature in a mood of indifference to the feelings of natural objects.
> — *Lynn Townsend White Jr*

> Our inability to fully comprehend the widespread decline of the natural world could be a consequence of our refusal to see ourselves as part of it.
> — *Joe Zadeh*

Anthropocentric thinking created the dichotomy between man and nature that allowed the scientific revolution to thrive on exploitation of the natural environment for the gain of humans, rather than seeing those two things as connected.

> Despite Copernicus, all the cosmos rotates around our little globe. Despite Darwin, we are *not*, in our hearts, part of the natural process.
> — *Lynn Townsend White Jr*

> This is the same narrative that assures us that, however bad things get, we are going to be saved at the last minute — whether by the market, by philanthropic billionaires or by technological wizards.
> — Naomi Klein

Viewing the world through a panpsychist lens allows us to question things we consider fundamental, while maintaining rooted into the reality of the world that has been established through the physical science. These two schools of thought are not at odds with each other, but actually complement each other and provide us with a larger context to see ourselves, and a path to envision a better future.
