---
tags: [culture, solidarity, tactics]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Establishing a Research Agreement with an Indigenous Community

## Alain Cuerrier

### [Source](https://www.bgci.org/our-work/projects-and-case-studies/establishing-a-research-agreement-with-an-indigenous-community/)

---

*abstract*

In 2003 a research project began to study the antidiabetic activities of the Cree Nation of Eeyou Itschee. This is a case study of the methods they took to ensure they were not disrespecting the Cree people or their sovereignty.

#### Core Ideas

They first wrote a letter on what the ethics of working together would entail. Even though it was not finalized until 2009, the researchers abided by the draft as if it was in full force.

> We described the project and its purpose and made clear that Cree communities could at any time 1) withdraw from parts of the project they do not agree with, and 2) keep any information confidential

After research had concluded they sat down with elders and knowledge holders to discuss the research results and gain feedback.

The decision to patent was left up to the Cree people and they would be given a majority share if they decided to proceed with one.

> New molecules were discovered during the project. Our research group sat down with community members and together discussed the meaning and implications of patenting and other avenues. Cree people felt that patenting was culturally inappropriate and, in the spirit of helping as many diabetics as possible, indicated that a publication would be best, which the team did.

Another thing discussed was how to credit the participating community members, this was decided on a person by person basis, and whether they wanted to be in the authors, acknowledgements, or one of the thanked anonymous members was up to them.

Continued communication was made with the participating Cree communities to ensure they were happy with the draft and felt that the project was connected to their needs.

> During those meetings, a lay language booklet summarising all the new findings was left behind for Cree people to consult. The booklet was translated into coastal and inland Cree

Finally, the researchers invested in workshops bringing the Cree youth with the Elders in order to bridge the gap between generations, an upcoming book on Cree antidiabetic medicines is being published and will be translated into local languages.
