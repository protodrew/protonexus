---
tags: [psychology, mental_health]
type: article
alias: socially_acceptable_anxiety
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Socially Acceptable Anxiety is Still Anxiety

## Adam Mastroianni

### [source](https://experimentalhistory.substack.com/p/socially-acceptable-anxiety-is-still)

---

Adam sits with the idea of anxiety as a socially acceptable negative thought pattern, why that might be the case, why that is a problem, and what to do about it.

He presumes that there are a lot of forms of anxiety deemed "socially acceptable", including avoiding conversations, checking the likes on a social media post, and incessantly following politics.

All of these are negative patterns that we find in our lives, and yet it is not treated as a problem that can be or should be solved. Adam thinks this is probably due to a few factors, but the ones that stuck out to me are that anxiety isn't antithetical to being productive and that suggesting to people that we should stop worrying feels like suggesting people stop caring about an issue.

As for a solution, treating anxiety like an addiction is a way that we can attempt to offer support to people in our lives. Encouraging people to go to therapy and refusing to enable or encourage that anxiety is a way that we can offer each other support without stigmatizing anxiety or treating it as a negative trait.

In my opinion the best line in the article goes to Adam's friend Clayton:

> when you let go of the idea that one big insight is going to make you feel good again and you're ready to start the long-term work you'll have to do, call me back.

As someone who is pretty anxious, this resonated with me and re-framed the idea of anxious thought patterns in a way I haven't seen before. This is definitely something I'm going to be musing on for quite a while.
