---
tags: [productivity]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Excuse Me but why Are You Eating so Many Frogs

## Adam Mastroianni

### [source](https://experimentalhistory.substack.com/p/excuse-me-but-why-are-you-eating)

---

Adam Mastroianni has experienced first and secondhand people's relationships with productivity eroding their personhood. The article presents the following ideas

#### You Aren't Lazy

The author has seen this both in the form of critique from business and productivity "gurus", and as a common sentiment expressed from other people about themselves. Human beings aren't inherently lazy, and we don't simply want to spend all of our time doing nothing. We have just been collectively pushing ourselves in areas that burn us out.

#### Your Unconscious Brain is an Ally

The common conception that we are working against our base instincts is rooted in participation bias. Because most of the problems that we solve through our unconscious brains require no thought, we only see the problems we have to work on together.

#### People Wield Laziness as a Threat

In a society dominated by constant productivity at all costs, people pray on our insecurities and encourage us to push ourselves as hard as we can through appeals to this perceived laziness.

Sometimes you have to do things you don't want to, and some people have to do more things they don't want than others, but it's unfair to think of ourselves as lazy beings when we are not endlessly productive regardless of any other factors.
