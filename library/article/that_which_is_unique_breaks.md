---
tags: [sustainability, philosophy, design]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# That Which is Unique, Breaks

## Simon Sarris

### [source](https://simonsarris.substack.com/p/that-which-is-unique-breaks)

---

That which is unique, breaks is a rumination on the commodification that has overtaken modern society. Sarris identifies how commoditization has infested our public spaces, our personal lives, and the design of our world. However, all is not lost in the eyes of Sarris; and this is not a premature obituary of a dead empire, rather a stumbling block as humans learn to inject our natural love and creation into the technology we harness.

> Love and effort create magnificent places. Genius inhabits them. People go to them because they know such places and landscapes offer consolation of the soul, and the soul is not fooled by substitutes.

Commoditization is not only an action that a society utilizes on a facet of our lives, but is also an ideology upon which an understanding of our world and ourselves. Through seeing humans as parameters and systems as a means to an end, goods become interchangeable and replaceable by design.

> It does not matter who made the copper bar, only that it is 99.9% pure.

> That which is unique, breaks. When finished objects become commodities they break too, but they are easily replaced. When you break a chair, you buy another chair. We know well how to make one thousand chairs. They sit in boxes, lining the warehouses, ready for two-day shipping.

> But when the unique breaks, we might mend.

I found this maps cleanly onto the situationist critiques of the lack of humanity in modern life, and the desire of Sarris to return to the act of mending.

Mending is seen by Sarris as not only repairing something to extend it's lifetime, but an act that allows someone to build, and from the user to become the creator. It is through this mending that we are able to comprehend human problems at a human scale, rather than attempting to apply a societal scale.

We are not good at building one fountain anymore, only 10,000. However, this doesn't have to be the case forever.

> Remember: History happens only once. There is no reason things must remain or return to any particular state.
