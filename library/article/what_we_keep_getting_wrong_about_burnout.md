---
tags: [productivity, mental_health]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# What We Keep Getting Wrong About Burnout

## Eve Ettinger

### [source](https://www.bustle.com/wellness/burnout-definition-what-we-get-wrong)

---

> Burnout is typically seen as something related to work, and is viewed as a sort of rock bottom position, what it actually is, is a response to complex and lengthy distress

Interesting points

- burnout has only been talked about seriously in the last 5 or so years, despite being something that has affected marginalized people extremely for decades
- on the other hand, many people in positions they see as advantageous struggle to see their own feelings as burnout, often due to guilt
