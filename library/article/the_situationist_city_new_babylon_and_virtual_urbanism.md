---
tags: [situationism, urbanism, philosophy]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# The Situationist City: New Babylon and Virtual Urbanism

## JR

### [source](https://jrenglish.me/the-situationist-city-new-babylon-and-virtual-urbanism)

---

The article discusses the formation of the situationist international through the various avant-guarde and marxist groups that coalesced under Guy Debord's idea of the spectacle. They sought to use urbanism and psychogeography to both expose the flaws of advanced capitalism and moderism, but also as a proving ground to apply their concepts to the physical world.

One of the main critiques of modernism and modern urbanist movements were the auto-centric philosophies guiding the way that cities were designed. Deboard said of the idea of the car that it was:

> at the center of this general propaganda, both as a supreme good of an alienated life and as an essential product of the capitalist market.
> — Guy Deboard

It was not only destructive as a commodity, but also carved up the city into freeways which lessened the ideas of community and social relation that comes with denser environments.

There was somewhat of a split amongst the situationists at this time as Nieuwenhuis grew tired of situationists existing only as propagandists, and began to imaging a new unitary urbanist utopia which he called "New Babylon"

It was largely criticized by other situationists who claimed that the project

> "reproduce(d) the same alienating conditions of the urban landscape" and "closely mirrored the modernist large-scale urban re-development so criticized by the S.I."

The thesis of this article comes right at the end

> The true realization of Nieuwenhuis' vision was in something he could not have fully foreseen: the internet. His visualizations show linear megastructures sprawling across the world, a network of hubs and links functioning as conduits for the aimless wanderings of its inhabitants as they bounce freely from one experience to the next.

> A place where everything is temporary, where individuals make and end contact easily, where anyone can see anything they want whenever they want. Though the physical limitations of an endlessly reconfigurable world may have been quite obvious, the virtual realm is the perfect environment for the concept. There is no average density or lot size, no ecology or pattern. The internet is New Babylon.
