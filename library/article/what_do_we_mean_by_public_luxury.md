---
tags: [sustainability, urbanism, culture]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# What Do We Mean by Public Luxury

## Pat Kane

### [source](https://www.thealternative.org.uk/dailyalternative/2022/1/30/what-is-the-case-for-public-luxury)

---

The article is a simple outline of why public spaces not only produce less waste, but also can provide more luxury for the people in a society. Here are some points I found particularly interesting

> Why is it that every rich person must have their own private pool that lays empty most of the time, or that professional freelancers are expected to spend thousands on equipment?

It largely outlines why things like well funded public trasportation, public spaces without the expectation of payment, and municipal car providing allows for a more vibrant society

[the average car is used 4% of the time](https://www.racfoundation.org/motoring-faqs/mobility#a5)
