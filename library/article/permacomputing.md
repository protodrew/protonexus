---
tags: [sustainability, computing]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Permacomputing

## Viznut

### [source](http://viznut.fi/texts-en/permacomputing.html)

---

Thoughts on applying permacultural strategies to the increasingly complex and resource-heavy world of computing.

> In both computing and agriculture, a major issue is that problems are too often "solved" by increasing controllability and resource use.

#### Core Ideas

the technology world has been using a dramatically increasing amount of artificial energy consumption, and while computing is not a primary draw of power right now, it is the fastest growing. Computers are also not succeeding at their stated goals to make accessing information quicker and easier as they become more advanced and resource intensive for diminishing returns. Adopting a permacultural philosophy to the way we value and think about computing would not only allow for computing to be more sustainable, but a better component of our society and daily lives. That serves primarily to help us rather than exist as a product.

### Energy

In order to have a more permanent environment we need to emphasize resource-sensitivity. The main resource computers use is electricity, which means we will need to change our usage patterns and generation strategies.

Intense computation that is not urgent (ex: long machine learning batches or blockchain related work) would take place during times of surplus energy

At times of low energy, computation process will scale down, reducing background tasks and decreasing clock frequencies

It is important to note that the two above solutions will require new computing infrastructure that allows it to be aware of the state of the grid

Make use of more long-term energy storage, the article presents flywheels as an alternative to chemical batteries due to long lifecycle and no need for rare earth metals

### Silicon

fabrication for computer components is a complex process that requires a large quantity of energy and nonrenewable, toxic materials. Yet, they are treated as disposable and are often discarded mere years after their use to toxify a landfill

We need to treat computer chips as precious and plan for longevity rather than obsolescence.

Broken devicies should be repaired and if a new device is needed, it should be made out of already existing components if possible. Chips should be designed with open and flexible standards so they can be reused later

Provide more redundancy in complex parts. For example, a multicore chip with 3 dead cores should be still functional, and if there are several partially functioning cores, they should be able to work together as one core

Put restricitons in place on what chips can be used for what purposes (gamers dont need a 16 core CPU and an RTX 3090)

Optimize programs, especially games. Most software is designed to take full advantage of whatever hardware it is on rather than minimize resource usage and function with older systems. This encourages the cycle of increased power and graphics and storage, rather than

### Miscellaneous Suggestions

when displays will be kept in a situation with bright-external light, transflective displays should be used so it isn't unnecessarily relying on a competing backlight.

make community access to computers and computing resources (via a library socialism-esque system) easier so people who don't rely on technology day to day don't need to waste resources on a computer
