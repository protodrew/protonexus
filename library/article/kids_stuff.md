---
tags: [culture, capitalism, technology]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Kids' Stuff

## Richard Woodall

### [source](https://reallifemag.com/kids-stuff/)

---

This article tries to answer the question that has grown from the explosion of PFP NFT projects like bored ape

*Why do modern technology products look like toys?*

The answer to this is complicated, and follows the trends in marketing from decades. The article starts with a quote from Chris Dixon, former venture capitalist and current web3 investor

> the reason big new things sneak by incumbents is that the next big thing always starts out being dismissed as a 'toy.'
> [_source_](https://cdixon.org/2010/01/03/the-next-big-thing-will-start-out-looking-like-a-toy)\_

This was not only a false claim at the time, but also an unknowing grim prophecy. Claims like this come from a society that deifies stories of innovation, and spends an inordinate amount of time on those who didn't believe. People who sold their stock, and left when they had ideological conflicts. *What fools they were, look at how much money they would have if they made different choices!*

> To the child's mind, there is no such thing as profit; the truck completes its imaginary logistical adventures for their own sake

Chris' claim would later turn out to be unfolding in the opposite direction. Toys have become the culture, and a lot of that is due to how childhood was and is perceived. Marketing towards children began in the Reagan era when advertising restrictions began to relax, and you can see the industry shift away from tools that promote creativity, to objects that represent creativity. Things like blocks or more simplistic things that could be used as a canvas that children could exert their creativity onto, and became action figures that promised an entrance to a preconstructed universe of possibilities. Later even this idea of engaging with it in a precapitalist sense would be sold back as nostalgia, and the cycle continues.

*Commoditoys* as the article calls them have become the basis for whole universes and franchises that garnered billions of dollars. So it makes sense that industries struggling to be seen as legitimate would turn towards established paradigms. Web3 projects are trying to emulate toys to promise a universe, and make consumers feel that they are a piece of a greater whole.

We can see this in project like Fortnite or Roblox that attempt to commodify creativity into child labor that turns the consumers into the producers, promising money, clout, or fun in exchange for the creative forces of children.

The consequences of this have been dire, and have stripped the identity of a child from one who lives in a state unaware of the forces of capital, to one obssesed with consuming for its own sake. The creation of this new type of consumer have reduced interaction to tribalism and relatability through the medium's consumed.

> Faced with attempts to reconfigure childhood as an infinite cycle of commodification, the best response may not be to tell everyone to grow up, but instead to decide what kind of kids we want to be.
