---
tags: [sustainability]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Scooping Plastic Out of the Ocean is a Losing Game

## Ryan Stuart

---

*abstract*

The desire to be part of the solution unites all cleanup efforts. But passion can be misplaced. The unfortunate reality is that there is no key to cleaning up the ocean. The solution is not flashy or sensational. It's not an entrepreneur with a big idea—a person we can look to as the one who will solve the problem. It's the boring stuff: when you use plastic, dispose of it properly. If you want to do more, help clean up a river, harbor, or beach. But really, we need to slow the flow of garbage trucks. Because right now, they're speeding up. We are using more plastic every year. By 2050, the garbage trucks could be unloading every 15 seconds instead of every minute. We don't have all that long before the ocean is teeming with more plastic than fish.

### The Problem

- 8 million tonnes of plastic ends up in the ocean every year<sup>1</sup>
- 1 million seabirds and 100 thousand marine mammals die due to plastic consumption or related damages<sup>2</sup>

#### What Efforts Are Being Made?

There have been multiple incentives like a korean program<sup>3</sup> that pays fisherment to collect plastic, to many automated plastic collectors.

Why are they not enough?

> We're making the problem worse at a pace that far exceeds what we can possibly clean up. ~ John Hocevar

Most of the plastic in the oceans is either too far away or too small to be ever cleaned up. A lot of these efforts end up spending money on an issue without ever thinking about the consequences. For example, [[the_ocean_cleanup_story]], which shows that just because you can throw millions at a problem doesn't mean you can fix it.

> I do worry [cleanup efforts] are distracting us from the real solution—closing the tap[removing sources that put plastic into the ocean]. ~ Yonathan Shiran

Alongside the Pew Charitable Trusts, Yonathan Shiran released a study<sup>4</sup> that found that no cleanup effort is cheaper and/or more effective than just preventing plastic from entering the environment.

However, that is not to say removing plastic from the ocean is an easy task. 2 billion people are currently living without any form of waste collection (although lower-income people produce far less plastic waste per capita), and rich countrys don't have any reason to put any environmental waste management regulations into place.

#### The Solution

> the best way to clean up the ocean is to stop trying to clean up the ocean

Most plastic comes from freshwater sources, so preventing it from ever reaching the ocean will be far more successful than trying to collect it while it is in the ocean.

Another successful project is the humble beach cleanup, which is effective at preventing it from returning to the ocean. Research shows that a majority of floating plastic will wash up on a beach eventually.

Fishing gear, which makes up between 500,000-1,000,000 tonnes of waste plastic. Is also relatively easy to prevent from reaching the ocean. 470 tonnes have been collected voluntarily, and many companies have been buying them to recycle into other products.

[1](https://www.science.org/doi/abs/10.1126/science.1260352)

[2](https://www2.deloitte.com/content/dam/Deloitte/my/Documents/risk/my-risk-sdg14-the-price-tag-of-plastic-pollution.pdf)

[3](https://www.researchgate.net/publication/23483476_The_incentive_program_for_fishermen_to_collect_marine_debris_in_Korea)

[4](https://science.sciencemag.org/content/369/6510/1455)
