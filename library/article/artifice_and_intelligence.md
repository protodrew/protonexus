---
tags: [communication, computing, culture]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Artifice and Intelligence

## Emily Tucker

### [source](https://techpolicy.press/artifice-and-intelligence/)

---

The tech policy press wrote this post to explain why they are going to stop using the term artificial intelligence for linguistic reasons. They point to Alan Turing's own quote¹

> The original question, 'Can machines think?' I believe to be too meaningless to deserve discussion. Nevertheless, I believe that at the end of the century the use of words and general educated opinion will have altered so much that one will be able to speak of machines thinking without expecting to be contradicted.

As evidence that rather than actually determining the thinking capabilities of machines, they have changed the definition of intelligence to separate it from consciousnes and sapience.

Tucker speaks to the colonization of imagination in the realm of human intelligence into something that can be hollowly recreated by a computer. This is done partially for marketing reasons, and to act as a way to ascribe magical properties to it. It is intentional that the discussions about AI are frought with complex buzzwords and technical discussions, because discussing the mechanisms of how it actually works would reveal that it is not, in fact, an omniscient god.

They suggest the following four principles to talking about AI

1. Be as specific as possible about what the technology in question is and how it works.
2. Identify any obstacles to our own understanding of a technology that result from failures of corporate or government transparency.
3. Name the corporations responsible for creating and spreading the technological product.
4. Attribute agency to the human actors building and using the technology, never to the technology itself.
