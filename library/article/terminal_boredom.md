---
tags: [technology, minimalism, computing, communication]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Terminal Boredom

## Or how to Go on with Life when less is Indeed less

# Applied Language

# [source](https://applied-langua.ge/posts/terminal-boredom.html)

---

This article attempts to outline the writer's reservations with the "small tech" movement, and explains this through the study of a few cases, the primary being the [Gemini Protocol](https://gemini.circumlunar.space/). The writer makes the following points through the lens of social ecology.

# Small ≠ Good

This first portion is very simply outlining that classifying tech as "small" does not inherently make it good, usable, or liberatory.

# Small Tech = Futurism

Futurism is a descriptive term for a specific brand of forward-thinking: one that roots itself in the constraints of our present society, while attempting to "revolutionize" our interactions with those constraints. The example given is

> Most futurists start out with the idea, "you got a shopping mall, what do you do then?" Well, the first question to be asked is, "why the hell do you have a shopping mall?"
> — *Murray Bookchin*

The author makes the case that the recent "fetishization" of minimalism in technology is rooted in futurist thought. Those who attempt to redefine the methods we engage in interactions without redefining the interactions are doomed to have something that neither solves the problems with the original system or creates a totally new system.

# Small Tech Doesn't Breed Accessibility

This section uses the case study of a [gemlog](gemini://drewdevault.com/2021/05/20/How-I-choose-a-license.gmi) from Drew Devault discussing how he chooses licenses. Where the author makes the claim that this chould have been displayed better in another format and that this is an example of how simple protocols direct people towards inaccessible design.

# Minimalist Software Isn't Minimal

This focuses on a specific line from the Gemini FAQ "Experiments suggest that a very basic interactive client takes more like a minimum of 100 lines of code, and a comfortable fit and moderate feature completeness need more like 200 lines."

The author takes issue with this, showing that a lot of the most popular gemini clients use thousands of lines of code.

# My Thoughts

This article makes some good points, but applies it to the wrong people. Indeed, groups like suckless or FSF evangilists who pursue technologies on a purely ideological basis fail to meet the needs of people who do not pass their litnus test of a pure computer user. This is a problem that has been outlined before and discourse surrounding this will surely outlive us all. However, the direction of this towards projects like gemini seems to miss the list at the beginning of the Gemini protocol website:

Gemini is a new internet protocol which:

- Is heavier than gopher
- Is lighter than the web
- Will not replace either

Overall, this reads as someone who feels personally wronged by someone in these movements attempting to frame the entire movement of alternative software as not just useless, but harmful. I can't really see that. This isn't a Google situation where a change is being foisted upon the end user and the market-share of the project will force it to become standard. All of the projects that are talked about here are hobbiest projects targeting hobbiest users. Nobody is saying that Gemini is the future of the web or that everyone should use it for everything. Projects that target a very specific use-case are not inherently bad because they don't target *your* use case.

There also seems to be some confusion between whether something is a user decision or an implementer decision. The example with Drew Devault's license flowchart could at best reach the conclusion "this isn't the best format to display this type of data", but is instead extrapolated to "this is an example of how minimalism hurts users". This flow chart was very clearly a stylistic choice from someone who prefers to use plain-text (as evident from all of Drew's projects), not the damning indictment the author seems to think it is.

The "what is minimal" section is the most confusing of them all. The author spends a lot of time upset at the idea of minimalism, and yet seems annoyed that clients have ended up using more lines of code than what gemini says a minimum spec would need. I feel like this is a no win scenario, if all implementations were 200 line terminal applications we would probably have just had a similar complaint. It also confuses a lot of aspects of the minimalism that Gemini wants users to experience. It is a text based medium designed to be delivered in a small, single package. It isn't a code-golpher spec-hack that is designed to be programmed in a series of 100 lines, the claim that Gemini makes seems to just encourage people to attempt to write a client by showing how simple it is compared to the **21 million lines of code** that Firefox and similar web browsers use.

Overall, this article spreads itself way too thin, and comes off as half-baked rants from someone who has a decent amount of technical knowledge, a small amount of sociology understanding, and 0 nuance.
