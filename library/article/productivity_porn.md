---
tags: [productivity, philosophy, organization]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Productivity Porn

## Caleb Schoepp

### [source](https://calebschoepp.com/blog/2022/productivity-porn/)

---

Caleb lays out an excellent breakdown of how social media hurts us all in a way that a lot of us know, but then goes into specifically things that are marketed to people with specific interests or a desire to be productive or make things. All of them are ultimately sapping your motivation by making you think that you are learning or being productive, where in reality they are likely as much of a time suck as a bunch of dog gifs.

The truly troubling part in the author's eyes are the following

- It saps intrinsic motivation
- it is deceptive and elusive (often we don't realize the ratio of us thinking about doing things vs doing the actual thing)
- It is necessary in moderation, because you **do** actually need to learn how to do things better over time.
- It is found in all forms of non-fiction media

As a side note, it's interesting to see an article reference [Internet Shaquille](https://www.youtube.com/watch?v=gYwkKaK5yeQ&ab_channel=InternetShaquille), who is just a great palette cleanser for this kind of stuff in the cooking world.

I originally found this essay through ratfactor's card on [Simultanerous Learning vs Simultaneous Doing](https://ratfactor.com/cards/learning-vs-doing). Which is in itself a great personal account of how they want to balance "learning projects" vs "doing projects".
