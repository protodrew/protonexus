---
tags: [game_development,education,labor]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---
# Understanding the Barriers Facing Games Students
## And How To Remove Them
## Marie Dealessandri
### [source](https://www.gamesindustry.biz/understanding-the-barriers-facing-games-students-and-how-to-remove-them)
---

Overviewing a [study](https://first3yearsproject.com/) that is following game development student post-graduation. The first thing that was found was that most guards are fearful about working in the games industry due to poor working conditions.

> Most new grads we talked to] really desperately want to work in the games industry. It's often a dream they've had since they were kids but also, overwhelmingly, they are afraid of what that's going to be like.

Worries involve the notorious long hours and crunch, but a significant amount of people are worried about discrimination upon entering the industry. Students really want to work in games, but don't want to deal with the inherent toxicity of the industry.

Largest concerns across the board

- Crunch

> Crunch is kind of how university works, but a lot of students are hearing that crunch is bad and that's not how you should make games, but they're not learning any other way to make games in their program

- Challenges getting work experience

> About 75% of the institutions that we've heard about have some kind of work experience placement. But it is often up to the student to find the internship and it's often not guaranteed, or there's not enough for everybody in the program."

- Lack of career counselling

> Some people have been referred to their university's career centre, but generally we hear that a university career centre is not very well equipped to help people in games or in any creative industry

- Lack of faculty diversity

> As opposed to crunch where a lot of professors are very aware of it and talking about it, discrimination and harassment might come up but often the professors don't really have the lived experience of how to deal with that in the workplace, which is a shortcoming that a lot of students identify

- Difficulty to build a portfolio

> most students we talked to have heard from their professors that it's not enough to have just your coursework projects in your portfolios. But of course only certain groups of students have the time and capacity to do these side projects that build that robust portfolio that their professors tell them the industry wants.

### Provided Suggestions of what Can Be Done

- Having meaningful conversations and diversifying academia

> It's really important to remember that these are conversations you need to have with *all groups of students* because the marginalised students will be talking about this among themselves already. But really it's the cis-het white students who really need to be part of those conversations, and recognising that that is part of the cultural problem.

- Addressing crunch in education as a barrier to equity

> We need to think about how the way we structure a program not only informs the kind of skills that the students come out with but also… **Can they come out?** And do they come out in a place where they're prepared to work in the industry for the long term and they're not just going to crash and burn in three years because all they learnt how to do is this intense dev cycle?

- Providing a path for growth

> We also want to make sure you're investing in talent. There are companies that invest in their education, invest in coursework for them, getting them mentors, providing them opportunities for growth so that they might be a good asset not just to the company, but as they progress in their career.

having just left a game major I can affirm a lot of these sentiments and it's been really refreshing to see someone echo a lot of the thoughts I have. A lot of this has reaffirmed my lack of desire to enter a traditional games space, and I really want to see if there are alternative spaces within interactive media that have the potential to bring about systemic change.