---
tags: [economics, game_design]
type: article
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# "Play-to-earn" And Bullshit Jobs

## Paul Butler

### [source](https://paulbutler.org/2021/play-to-earn-and-bullshit-jobs/)

---

> An exploration as to how play-to-earn games, through their use of crypto, and by centering income over actual gameplay, leads only to the creation of more "Bullshit Jobs", as represented by David Gareber

The article begins by giving a synopsis of the concept of "Bullshit Jobs", as jobs that exist only to allow for the flow of capital, and do not actually contribute anything tangible to society. It then goes into discussion of NFT games like Axie Infinity, which are a new sort of class of games that exist only to generate revenue, and encourage people to engage in the game to allow for the flow of capital.

The article does go into how the game allowed Filipinos to make money through the pandemic, but explains that this is a perfect example of a Bullshit Job.

The article then goes into an explanation of "grinding", or spending time to get materials or rare items that is often used to allow for the game to market power ups or time savers to the players. It then goes to discuss how the developers have attempted to market is as decentralized element of web3, but really it is just a regular game with NFT architecture slapped on top to imply more value to the project.

The game itself isn't seen as fun and people aren't really playing it as a game, rather it just becomes a new form of work that serves only to benefit people who have been there longer (see: ponzi schemes)
