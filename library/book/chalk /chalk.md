---
tags: art, biography
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Chalk

## *the Art and Erasure of Cy Twombly*

## Joshua Rivkin

---

Chalk is equal parts a biography of the incredible [Cy Twombly](https://en.wikipedia.org/wiki/Cy_Twombly), but also a personal story of Rivkin's fascination with the mythos and mystery surrounding the man. Cy Twombly's interviews and writings can be counted on one hand, and the man was famously guarded when it comes to his personal history. Rivkin takes immense pleasure in the chase of piecing together information through quotes from close friends and admirers, as well as the photographs from his momentary lover [Robert Rauschenberg](https://en.wikipedia.org/wiki/Robert_Rauschenberg).
