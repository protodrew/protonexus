---
tags: [history, anthropology]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Collapse of Complex Societies

## Signs of Collapse

large amounts of leaders that do not last long. loss of complexity in governance (especially in outer territories). Increase in fighting, squabbles between provinces, and loss in quality of life among everyone, but especially the ruling class.