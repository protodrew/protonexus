---
tags: [technology, economics]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Proposed Solutions in [[abolish_silicon_valley]]

Reclaiming entrepreneurship

- publicly owned fund for entrepreneurs (both for new products and for businesses to transition to worker coops or alternative management models)
- judgement based on practicality rather than market dominance
- fair split between workers and founders
- eliminate private investment funds

> A company that's able to mint billions is too large to be run for the benefit of a small number of shareholders

Reclaiming work:

- raise minimum wage and integrate minimum raises to account for inflation
- universal healthcare and better unemployment to allow workers more autonomy
- unionization in all industries to allow for workers a way to organize against toxic environments
- migrant worker benefits that allow them to switch jobs without being dependent on their employer
- worker representatives on corporate boards (if not a removal of boards entirely)
- institute a maximum wage based on the lowest wage (including contractors)

Reclaiming Public Services:

- universal healthcare, publicly funded primary and secondary education, public banking
- nationalize mobility (scooter companies, public transportation, etc)
- combine the concepts from private shared office spaces with public libraries

Reclaiming Intellectual Property:

- reduce copyright and trademark expiration dates
- patents should only be granted under specific circumstances, and not traceable
- work funded through public grants should have open licenses
- source code should be opened after a certain amount of time (or alternatively, at minimum, a public and open API that contains a large amount of the featureset)
- data should be in control of the people, and not seen or used as an asset for the company

> Public interest should triumph over concerns of private investment

> we should see data as belonging to the people who created it, and to the public at large

Reclaiming Culture:

- remove the ability to advertise a product that doesn't meet an ethical standard
- remove advertising from spaces that accept public funding
- greatly reduce abilities to target advertisement, and mandate opt-in
- political advertising should be public record
- ad-based services that would struggle under new regulations should receive public funding (3)
