---
tags:
  - fiction
type: book
creation-date: 2023-10-23
modification-date: 2023-12-27
---
# Foucault's Pendulum
## Umberto Eco
---

Foucault's Pendulum is an esoteric satirical novel centering academic laborers in the Bohemian streets of 1960s Milan who create and become obsessed with their own invented conspiracy theory. The book mocks the detached intellectualism of the academic left, and its structure feels like putting together broken memories.

We open to Casaubon (the narrator) hiding in a museum, suspecting his friend Belbo was kidnapped by the same secret society that hunts him. While hiding, he worries that he has been going mad, and that the plan he is preparing to expose is nothing more than an invention.

The story flashes back to his meeting with Belbo and Diotallevi, who work as editors at an academic publishing house. They spend their time in a bar and exchange stories and obscure self-important banter, like intellectual fencing or confounding interpretive dance. They find a shared cynacism and jaded detachment from the world around them.