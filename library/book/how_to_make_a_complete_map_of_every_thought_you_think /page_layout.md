---
tags: [organization,knowledge_management,productivity]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Page Layout

from [[general_principles]]

the book claims that each piece of information will have the following

- Content
- Date
- Title
- Page ID/Number
- Sequence Identification
- Archival Mark

Content, Date, and Title are pretty self-explanatory, but the final three work in sequence

The Page ID will have information such as if there is relevant information on the next pages, for example, given P27 indicating a point of interest, P27-1 indicates the beginning of a sequence of pages.

The sequence identification is a glyph at the bottom right that indicates whether this note continues, which aims to provide more context, a simple arrow to the right denotes there is more, while an empty box indicates the stop. However, if you go back to that note and extend it, putting an arrow in that box will allow you to know that it continued after it's original stopping point.

The archival mark (which can be whatever glyph you want), is put on a page to indicate that it is no longer necessary info to keep in the main binder, but you should keep because other links depend on it.
