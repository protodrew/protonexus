---
tags: [organization, knowledge_management, productivity]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Information Density

from [[general_principles]]

Information density is not just how much information you should cram on one page, though at times that is helpful. It posits that things like tables/maps of contents' should be as dense as possible, while points of interests should have plenty of room for nonlinear additions.
