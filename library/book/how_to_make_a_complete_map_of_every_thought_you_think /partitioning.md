---
tags: [organization,knowledge_management,productivity]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Partitioning

from [[general_principles]]

this is another subject that is pretty abstract, it relies on having a style guide that allows you to define the space that a note occupies or appears. The latter of the two is more relevant to me personally, as physical space is somewhat unnecessary.
