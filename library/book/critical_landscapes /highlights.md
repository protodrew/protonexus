---
type:
tags: 
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Critical Landscapes: Art, Space, Politics

## 2015

# Introduction: Contemporary Art and the Politics of Land Use

# Page 15

Certain artists negotiate the legacy of 1960s and 1970s Land art or the conditions of the global art world, while others actively eschew art-world reference points, choosing instead to position their work relative to disciplines such as cultural geography or urban planning, community-based activism,

# Page 15

the writings of the French Marxist philosopher Henri Lefebvre are a key influence. In his landmark book The Production of Space (originally published in 1974 and translated into English in 1991), Lefebvre traces the transformation of space under capitalism and, in so doing, theorizes space itself as a product of social relations and processes.

# Page 17

The concept of landscape has, in fact, been hitched from the beginning to aspects of human use—labor, property, domestication, jurisdiction, and so on—with a dialectic between humans and the land at its core.

# Page 19

A considerable part of what we call the natural landscape … is the product of human design and human labour, and in admiring it as natural it matters very much whether we suppress that fact of labour or acknowledge it

# Page 20

the American artist Robert Smithson wrote about the "dialectical landscape" in the last published essay of his life, and from the mid-1960s onward preferred explicitly "disrupted," or "pulverized," sites to those that evoked a sense of untouched beauty.11 He was critical of the ecology movement quickly gaining steam at the time and derided a budding "wilderness cult" for its reductive division between the human and non-human worlds, arguing, "Spiritualism widens the split between man and nature."

# Page 24

CLUI's Land Use Database,

# Page 25

Many of the artists and artist collectives included in this volume follow CLUI's lead in avoiding the ghettoizing term "art" to characterize their activities of careful observation, data collection, and general concern with producing accurate representations of the conditions that form the environments in which we live.

# Page 28

distances have widened between the places where decisions regarding land use are made and where they are enacted

# Page 28

Whereas Ingold's medieval worker-shapers, who "with foot, axe, and plough … trod, hacked, and scratched their lines into the earth," performing labor that was "close-up, in an immediate, muscular and visceral engagement with wood, grass, and soil," in our own day and age, a whole series of geographically dispersed factors and agents, some virtual and others "analog" (e.g., international trade and patenting laws, migrant worker streams, stock market figures, transportation systems) prefigure any moment before foot or plough pierces ground

# Page 28

various neoliberal economic policies such as the deregulation of financial markets, together with the expansion of multinational corporations and advancements in telecommunication technologies, have ushered in an era in which the global economy operates with unprecedented power and swiftness.

# Page 28

The sociologist Saskia Sassen has described "finance [as] the engine of our time"—a vast force that is "flattening everything around us" as it "grabs more and more terrain" amid an ongoing shift from a world organized largely by national territories to one of global connectivity and jurisdiction.

# Page 29

this accelerated, all-encompassing urbanization involves the displacement, or "systemic expulsion," of humans on a massive scale resulting from a number of practices and processes, among them: mega-development projects such as the construction of dams in China; desertification owing to deforestation and over-farming; and neocolonial land grabs, wherein large tracts of land, especially in sub-Saharan Africa, are sold off to foreign investors

# Page 30

He concludes, making clear his own ethical-political orientation: "Documenting the transnational networks of power that characterize contemporary imperialism and thereby contributing to bonds of solidarity between the dispossessed in the global North and South is the key task for contemporary artist-activists."

# Page 31

The rise of corporatized, de-diversified agriculture coincides with a growing nostalgia for the agrarian past, before pesticides and fertilizers were needed to grow "conventional" produce, as reflected in the current explosion of urban gardening movements as well as markets for organic, artisanal, and slow foods among a largely affluent and "progressive" demographic in the United States, Europe, and elsewhere.

# Page 32

In countless urban "renewal" projects, the incorporation of nature (e.g., the addition of parks or other "green space," lakefront development, the "revitalization" of river ways) and, more generally, an imperative of "sustainability," are cornerstones.

# Page 32

Such gentrification efforts, while purportedly ecological in character, are of course also (if not ultimately) meant to generate revenue, whether by boosting tourism or attracting potential new investors, businesses, and inhabitants; they always involve some degree of displacement, too. If "greenwashing" is prevalent in the current development and marketing of cities,

# Page 32

we might also consider "artwashing," wherein art and the "culture class" associated with it are likewise instrumentalized to drive up real estate values. The American artist and critic Martha Rosler bluntly states: "The artist's assigned role in urban planning is to gentrify space for the rich

# Page 34

Meanwhile, with the rapid melting of polar ice, the Arctic has become a geopolitical hotspot as northern countries vie to stake claims on potential new routes for shipping and oil exploration. The impossibility of disentangling the human from the "natural" is made especially clear,

# Part I. Against the Abstraction of Space

# Page 40

the abstract economic, social, and power relations that produce lived realities within specific locations around the globe, from the Sahara Desert to the city of Detroit. If such relations remain unexpressed, those who populate these spaces, or consume these sites as tourists or visitors, participate unwittingly in the dynamics of power and control that shape a site

# 1. Julian Myers-Szupinska

# Page 42

The book is Henri Lefebvre's The Production of Space, in its English translation by the erstwhile English Situationist Donald Nicholson-Smith.