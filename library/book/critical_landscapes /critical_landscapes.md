---
type:
tags: 
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Critical Landscapes
## [[library/book/critical_landscapes/highlights|highlights]]

the discussion of tours reminds me of the experimental shows mentioned in [[../how_to_do_nothing/how_to_do_nothing]], specifically the one where guests watch a sunset in silence for 45 minutes. Both are attempts to take people out of their atomized self conceptions and affix them in a space and time.