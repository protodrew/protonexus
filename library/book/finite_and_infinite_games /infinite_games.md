---
tags: [philosophy, psychology, culture]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Infinite Games

from [[finite_and_infinite_games]]

While Infinite games share the [[common_traits_of_games]], they are different in every other way from [[finite_games|finite games]]. They occur always and the only purpose of the game is to extend it.

Finite games can exist within infinite games, but the inverse is not true. Infinite games can involve performance but do not require it in the same way as finite game.
