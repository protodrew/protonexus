---
tags: [philosophy, psychology, culture]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Finite and Infinite Games

## James P. Carse

---

Finite and Infinite Games is a book that uses the the concept of play and games to synthesize game theory (not the math one), and philosophy.

[[common_traits_of_games]]

[[finite_games]]

[[infinite_games]]

Special interest is taken to the idea of performance within finite games. How within imperial hegemony acts as both a conscious suspension of disbelief, but a veil to introspection about the nature of their existence / optionality of participation.
