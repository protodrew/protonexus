---
tags: [philosophy, psychology, culture]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Common Traits of Games

from [[finite_and_infinite_games]]

The only commonality between both [[finite_games]] and [[infinite_games]] is that they both must opt into. While it is obvious for an infinite game that your willing participation is required, but the concealment we do to our own minds about what we *must do* makes it appear impossible to opt out.

This concept strikes me as similar to the concept of "Capitalist Realism" from the book of the same name. Author Mark Fisher uses the term to describe the feeling that capitalism is not only the best system, but the only system that can exist.
