---
tags: [cooking]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Salt Fat Acid Heat
## Samin Nosrat
---
## Salt

this section had a good breakdown of the functions of salt as aids in osmosis and diffusion. I already have the salts I'm comfortable wit, those being: whatever for salt that will dissolve (right now I have 1 billion kinds but I mostly use the morton table salts), morton kosher salt for most cooking, and morton or maldon for finishing.

The focus on the timing of salts is very true, and I was pleased to have an answer to some good amounts of time to avoid. Not that I've been salting my kale a week in advance, but it's good to see it in a nice little chart. Relevant ones for my vegetarian cooking are 15 min before cooking for vegetables, right as cooking for mushrooms (didn't consider this one before), and while they didn't mention tofu, I like to either boil in salted water then leave on towels to dry, or salt up to 2-3 hours before cooking (though I usually only do it for 1 because I lose track of time).