---
tags: [culture, history, anthropology]
type: book
alias: The Dialectics of Art Highlights
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# Highlights
## From [[the_dialectics_of_art]]
### Page 25 @ 28 May 2023
*Secondarily, my aim is to define art for what I will call the capitalist or bourgeois era: that is, roughly from the beginning of the Renaissance (in Europe) through to the global present.*
### Page 25 @ 28 May 2023
*the modern use of the word 'art' to denote a specific sphere of activities and objects, comprising painting, music, literature and so on, distinct from other nonartistic activities and objects, did not apply to many or most of those periods or cultures*
### Page 26 @ 28 May 2023
*the concept of art, as it is widely used today and as I want to define it, did not exist before the bourgeois era. The word existed, but its meaning, and thus its use, was fundamentally different.*
### Page 27 @ 28 May 2023
*The Marxist art historian Arnold Hauser identifies Michelangelo as a key figure in this process and argues that '[t]he fundamentally new element in the Renaissance conception of art is the discovery of the concept of genius, and the idea that the work of art is the creation of an autocratic personality, that this personality transcends tradition, theory and rules.'*
### Page 28 @ 28 May 2023
*It is worth noting here that the first public art galleries and museums emerged in the eighteenth century and that the world's largest and historically most important art museum, the Louvre, was established in 1793 as a direct result of the French Revolution*
### Page 28 @ 28 May 2023
*'All social life is essentially practical,' he writes in his Theses on Feuerbach. 'All mysteries which lead theory to mysticism find their rational solution in human practice and in the comprehension of this practice.'*
### Page 30 @ 28 May 2023
*[E]ither all works of visual art have some common quality, or when we speak of 'works of art' we gibber. Everyone speaks of 'art', making a mental classification by which he distinguishes the class 'works of art' from all other classes. What is the justification of this classification?*
### Page 30 @ 28 May 2023
*Only one answer seems possible – significant form.*
### Page 30 @ 28 May 2023
*The aesthetic emotion, he claims, is a distinct emotion independent of and different from all other emotions, and independent of all knowledge and experience of the world, 'which all sensitive people agree' is 'provoked by works of art'. If (like me) you don't agree, that simply proves that you are insensitive, like 'a deaf man at a concert'. This is as circular and empty as the notion that art is what artists say it is,*
### Page 32 @ 28 May 2023
*Once we do look, it is evident that all works of art – symphonies and poems and dances and songs and films, as well as paintings and sculptures – have in common the fact that they are all produced by human labour, mental and physical.*
### Page 32 @ 28 May 2023
*Paul Cézanne's numerous Mont Sainte-Victoire paintings are art of a high order; Mont Sainte-Victoire itself is not.*
### Page 34 @ 28 May 2023
*In capitalist society, the labour of the overwhelming majority of people – especially, but not only, working-class people – might be thought in many respects to more closely resemble that of a bee than that of an architect. By contrast, creativity, which Marx ascribes to all labour, appears to be the province of a small minority, primarily artists.18 Marx calls this kind of uncreative work 'alienated labour'.*
### Page 35 @ 28 May 2023
*The worker therefore only feels himself outside his work, and in his work feels outside himself. He feels at home when he is not working, and when he is working he does not feel at home. His labor is therefore not voluntary, but coerced; it is forced labor. It is therefore not the satisfaction of a need; it is merely a means to satisfy needs external to it*
### Page 36 @ 28 May 2023
*in case after case it is clear that neither economic compulsion nor economic acquisitiveness is the motivation for artistic labour*
### Page 36 @ 31 May 2023
*there is something more fundamental at play than the artist's subjective motivations or attitude. Even where the artist is substantially motivated by personal gain, as seems to have been the case with Salvador Dalí, for example*
### Page 37 @ 31 May 2023
*artistic labour it is the artist who controls the process of production (and in most cases also owns his or her immediate means of production). This is not only true empirically – painters control the making of their paintings, poets the writing of their poetry, composers the composing of their music and so on – but it must be so, or 'artists' would disappear as a social category. Art would simply be manufactured in mass production like shoes, cars or mobile phones.*
### Page 37 @ 31 May 2023
*This, then, is the first element in the definition of art I wish to propose: art is work produced by unalienated human labour.*
### Page 38 @ 31 May 2023
*artworks, in contrast to vegetables, games in the park and country walks, are charged with meaning to a qualitatively different degree: they make statements to and about the world; they possess and express an intellectual and/or emotional content*
### Page 38 @ 31 May 2023
*My argument is that it is not the physical nature of the object itself which decides whether or not it is art – any object has the capacity to be made a work of art – but the nature of the labour involved. It is the labour of Long, Duchamp and Warhol that is charged with meaning and produces work that embodies that meaning, that is to say, has content.*
### Page 38 @ 31 May 2023
*However, the same is clearly true of political campaign work and works of scientific and social theory: they are the products of labour under the control of the producer, and they are packed with meaning. Yet they are not regarded as art. Why not?*
### Page 39 @ 31 May 2023
*. In a work of scientific or social theory, the content is not only much more important than the form, but it is also to a large extent detachable from it. In a work of art, the content/meaning is totally bound up with the form and is inseparable from it*
### Page 39 @ 31 May 2023
*by 'content' I mean not the 'subject matter' but the sum total of a work's emotional/intellectual/psychological meanings*
### Page 41 @ 31 May 2023
*call art. In a poem it is the specific combination of words in a specific order, with all their associations and their sound (rhythm, rhyme, alliteration, assonance, onomatopoeia) that make up the poem's content*
### Page 41 @ 31 May 2023
*this unity of form and content in art, across the board, is recognised in practice by both artists (in producing the work) and by critics (in assessing it*
### Page 41 @ 31 May 2023
*this unity or fusion is not absolute, nor always perfectly realised. Rather, it constitutes a goal towards which art and artist are constantly striving and by which the work can be judged*
### Page 42 @ 31 May 2023
*'A writer must of course earn a living to exist and be able to write, but he must in no sense exist and write so as to earn a living…. In no sense does the writer regard his works as a means. They are ends in themselves…. The first freedom of the press consists in its not being a business*
### Page 45 @ 01 June 2023
*most interesting is the claim that 'capitalist production is hostile to certain branches of spiritual production, for example, art and poetry'. The truth or otherwise of this claim depends, partly, on what we think it means.30
It could be taken to mean that the capitalist class (the bourgeoisie), taken as a whole, are hostile to art. There may be an element of truth in this, in that many, probably most, capitalists are vulgar boors and philistines interested only in making money*
### Page 45 @ 01 June 2023
*If, however, we interpret Marx's statement to mean that there is an inherent contradiction between capitalist methods of production and the production of art, and that art cannot and will not be produced by the normal methods of capitalist production (i.e., by means of wage labour in which the employer determines more or less entirely the work of the worker and owns from the outset the products of that labour), then I think this statement is true. It is also precisely what I am arguing when I say that art is a product of unalienated labour.*
### Page 46 @ 01 June 2023
*With the arrival of capitalism, however, the memory of this was lost. '[I]n short the whole civilised world had forgotten that there had ever been an art made by the people for the people as a joy for the maker and the user.' Art, he maintains, 'is man's expression of his joy in labour'*
### Page 47 @ 01 June 2023
*Under these circumstances it was a matter of course that a man, being master of his work should choose to make it pleasanter to himself by exercising on it that love of beauty which is common to all men till it is crushed out of them by the mere bitter struggle for life called competition for wages, and by subjection to a master who also is struggling for profit against other competitors.*
### Page 47 @ 01 June 2023
*When the workers organise work for the benefit of the workers, that is to say of the whole people, they will once more know what is meant by art.*
### Page 52 @ 01 June 2023
*if Marx was not really in control of the content of Capital and the pickets are not voluntarily and purposefully organising the picket line but are still more or less completely dominated by alienation in this work, then changing the world becomes impossible*
### Page 53 @ 01 June 2023
*all art without exception rests on alienated labour: not only did Michelangelo depend on quarry workers in Carrara to produce his block of marble for the David and other labourers to transport it to Florence, but any painter requires paints and canvas,*
### Page 56 @ 01 June 2023
*The dividing line, or more accurately, the transition zone, lies within genres, not between them. Flamenco, for example, contains unambiguous art (and of a very high order) as well as completely manufactured and commercialised 'shows', based on highly alienated labour, for tourists*
### Page 56 @ 01 June 2023
*In filmmaking, for example, there is a relatively high proportion of alienated labour, both in terms of technicians, camera operators, extras and the like, and in terms of the preponderance of films totally subordinated to market forces and containing minimal elements of art: for instance, James Bond movies, CGI superhero series or pornography. Nevertheless, this doesn't entirely prevent artistic work, in terms of screenwriting, direction and acting, being done even within the framework of Hollywood, as well as in film worldwide.*
### Page 56 @ 01 June 2023
*The role of the novelist's and poet's self-determined artistic labour, with its concomitant goal of fusing content and form, remains more entrenched. At the same time, there clearly exists a substantial layer of 'pulp fiction', in which the writer may have formal control over their own labour while in reality having completely internalised the requirements of the publisher for formulaic hackwork: for example, the romances published by Mills & Boon.*
### Page 57 @ 01 June 2023
*Alongside what is often called 'fine art', there are the adjacent and related categories or disciplines of graphic design, product design, interior design and illustration.*
### Page 58 @ 01 June 2023
*I would go further than Berger's formulation. Advertising is not a 'moribund form of art'; while it is alive, well and spreading, it is not art. It is a caricature and mockery of art – art's shadow, its dark side – because it is, to the nth degree, the product of alienated labour in which the content of advertising images are determined 100 per cent by the commissioning 'client' and the pursuit of profit*
---
would partially disagree with his statement on the intellectual creation being done by the client. I think artists move to advertising as a way to exercise crestivity with the safety of alienated labor
### Page 59 @ 01 June 2023
*First, it follows that almost all the works around which the 'But is it art?' controversies have raged, for the last century or so, are indeed art: Picasso's Les Desmoiselles and cubist works, Malevich's Black Square, Duchamp's Fountain, Brâncuși's Bird, Pollock's drip paintings, Rothko's colour fields, Warhol's Brillo Boxes, Lichtenstein's Whaam!, Andre's Bricks (Equivalent VIII), Richard Long's A Line Made by Walking, Hirst's shark, Whiteread's House, Emin's My Bed and the rest.*
### Page 59 @ 01 June 2023
*Second, it follows that art in general represents within capitalist society a positive human value – a 'model of human freedom', as Berger put it – to be supported and nourished. I do not mean by this that all art is 'good' or beneficial to humanity. Some may indeed do more harm than good, such as Rudyard Kipling's colonialist poem 'The White Man's Burden', D. W. Griffith's Civil War drama Birth of a Nation, Leni Riefenstahl's Nazi propaganda film Triumph of the Will, or many of the paintings of Balthus. But overall, art is a benefit to mankind.*
### Page 59 @ 01 June 2023
*Third, it follows that in capitalist society art exists and will continue to exist in a state of siege. This state of affairs is not conjunctural but permanent. It includes direct attacks, like cuts to funding in times of austerity, and, under authoritarian regimes, censorship and state repression; but most fundamentally it consists of the antagonism that exists between capitalist social relations and production processes and artistic production.*
### Page 60 @ 01 June 2023
*the human impulses towards creative labour are far too universal and powerful*
### Page 60 @ 01 June 2023
*How many actors who aspired to play Hamlet or Cleopatra end up earning a living doing voiceovers for adverts? How many painters who dreamt of being Picasso end up surviving by teaching art in school or college with little practice of their own – or simply give up?*
### Page 60 @ 01 June 2023
*Andy Warhol made it from commercial illustrator to world's most famous artist, Salman Rushdie from advertising copywriter to Midnight's Children. Orson Welles, after Citizen Kane, made sherry adverts to finance his films but often failed to complete his projects.*
### Page 60 @ 01 June 2023
*Art, in its many forms, is a kind of spiritual health food which contributes to the development of the human personality, especially among working-class people – that is, when they can overcome the numerous economic, social and psychological barriers to accessing it*
### Page 61 @ 01 June 2023
*Alliances between the Left and art must be based on mutual respect and are most likely to take the form of a multitude of episodic collaborations rather than a formal arrangement or structure.*
### Page 62 @ 01 June 2023
*the overthrow of capitalism and the initiation of the transformation of society into a socialist one will produce a complex dialectical development of art. It will, in the first place, unleash the creative energies of large sections of the population and generate, as it started to do (even in the most desperate of circumstances) in Russia after 1917, a huge wave of artistic enthusiasm, innovation and experiment which will reach layers of society hitherto largely untouched by artistic culture. On the other hand, as the transition to socialism becomes fully realised, 'art' as a separate category, defined by its opposition to the predominant alienated labour, will cease to exist because alienated labour will cease to exist*
### Page 62 @ 01 June 2023
*Marx and Engels put it in The German Ideology, 'In a communist society there are no painters but at most people who engage in painting among other activities*
### Page 62 @ 01 June 2023
*Capitalist society, with its division of mental and manual labour, its fragmentation and alienation, gives rise to a separation of art and the artist from the mass of people on the one hand, and from productive work on the other.*
### Page 62 @ 01 June 2023
*Art becomes a privileged arena in which the minority express themselves creatively while the majority are condemned to mechanical, non-expressive, non-creative labour.*
## 2. How We Judge Art
### Page 64 @ 01 June 2023
*Far more art is produced than is going to survive, so decisions have to be made as to what will be preserved and what will be discarded and destroyed. Within what is preserved, another multitude of decisions arise as to what will be displayed and where. Which works will hang in the Louvre or the Uffizi?*
### Page 64 @ 01 June 2023
*if judgment is unavoidable, this raises the questions of how the judgment is made, by whom and on the basis of which criteria*
### Page 65 @ 01 June 2023
*Both the average person and the so-called art lover are heavily influenced by what they have heard and read. They don't stand in front of a painting they've been told is by van Gogh and look at it with the same eyes as they look at one by an unknown artist. This applies just as strongly, perhaps even more so, to 'the expert' – the art historian or professional critic – on whom there is social pressure to 'appreciate' the Vermeer, whereas the layperson might just say, 'It does nothing for me'.*
### Page 65 @ 01 June 2023
*judgment of art is a social process. It operates through a range of institutions and strategically placed individuals within those institutions. The process changes over time*
### Page 65 @ 01 June 2023
*One reason it seems mysterious is that it involves the art market, which works, to a considerable extent, anonymously. Another reason is that many of the key decisions are made in secret and never publicly explained. If the Tate Modern decides to commission a work for its Turbine Hall or hold a 'blockbuster' exhibition for a certain artist, this will undoubtedly boost that artist's reputation. But why that artist is chosen and not another –*
### Page 65 @ 01 June 2023
*need never be publicly articulated*
### Page 66 @ 01 June 2023
*Very few artists have generated truly global reputations, and, for obvious historical reasons, they have been overwhelmingly European*
### Page 66 @ 01 June 2023
*When it comes to the operation of the art world today,3 it is possible to identify the following actors in the formation of aesthetic judgments: critics (journalistic and academic), historians, museum and gallery directors and curators, dealers, collectors, the art-engaged public and artists themselves.*
### Page 66 @ 01 June 2023
*the critics may appear to play the central role, especially because their judgments are public. But in my view their influence is generally overrated. Much more important are museum directors, dealers and, above all, collectors: they have much greater institutional and economic power, and we should not ignore the extent to which the critics actually depend on them*
### Page 67 @ 01 June 2023
*Most of the time they are far more influenced by art world 'movers and shakers' than they are influencing. Nevertheless, it would be wrong to discount the art-engaged public altogether. For example, Tracey Emin's intense popularity with a certain layer (mainly of young women) has, I think, had an influence on directors and critics*
### Page 69 @ 01 June 2023
*There has long existed a tendency within art criticism to argue that aesthetic evaluation is a matter of 'intuition'. This goes back at least to Clive Bell in 1914, and his 'aesthetic hypothesis' that 'the starting-point for all systems of aesthetics must be the personal experience of a peculiar emotion'.*
### Page 69 @ 01 June 2023
*In my opinion there is a strong element of upper-class snobbery in this view*
### Page 69 @ 01 June 2023
*Nevertheless, I am not dismissing the idea of intuition out of hand. As a chess player, I know that a great player like Mikhail Tal or Garry Kasparov can play blitz or lightning chess in which they produce profound and brilliant sequences of moves at a speed that precludes the possibility that they have been thought through or calculated.*
### Page 70 @ 01 June 2023
*The criteria of mimesis or imitation (or 'naturalism') is, of course, ancient, going back to Plato and Aristotle*
### Page 71 @ 01 June 2023
*Today all of this persists in popular discourse about art, in that works which entirely lack mimetic qualities are, even now, often considered not to be art, while others will be praised on the grounds that they make you feel 'as if you were there' or 'can touch that velvet'. However, it is clear that since the invention of photography and its successors, the criteria of mimesis has declined in importance,*
### Page 72 @ 01 June 2023
*the refusal to grant the status of art to works such as Duchamp's Fountain and Carl Andre's Equivalent VIII. The criteria of skill has long been closely associated with that of mimesis, but it is not necessary that it should be. Skill or technique may be required for and manifested in many nonmimetic works*
### Page 73 @ 03 June 2023
*his Poetics, Aristotle wrote of beauty as 'a matter of size and order', 'unity and wholeness' and 'a certain order in its arrangement of parts*
### Page 73 @ 03 June 2023
*this conception of beauty as essentially harmony between the parts and the whole can be traced down the ages through Giorgio Vasari – who wrote of Michelangelo's David that '[t]he grace of this figure and the serenity of its pose have never been surpassed, nor have the feet, the hands, and the head, whose harmonious proportions and loveliness are in keeping with the rest*
### Page 73 @ 03 June 2023
*Aestheticians writing today would lay much greater stress on the role of socialisation and, therefore, the cultural relativism of all standards of beauty. Be that as it may, it is the former conception which has exercised the predominant influence in the evaluation of art in the Western tradition.*
### Page 75 @ 03 June 2023
*For Burke 'the sublime' designates feelings of awe and terror evoked by great forces, especially natural forces. He lists a number of qualities or characteristics associated with the sublime: darkness, obscurity, deprivation, vastness, magnificence, loudness, suddenness*
### Page 76 @ 03 June 2023
*With the onset of the Enlightenment, Western art became increasingly secularised (though this process had already begun in the Renaissance and in Dutch art of the Golden Age), and the demand that art should be ethically edifying waned. Nevertheless, morality remained as a kind of backstop which could be invoked in condemnation of work that offended established moral codes.*
### Page 76 @ 03 June 2023
*In the twentieth century, moral concerns increasingly merged with political ones. The Far Right have fairly consistently attacked modern art as both communistic and immoral, with the Nazis famously condemning 'degenerate art' and the Republican Right in the United States in the eighties and nineties waging a campaign of denunciation known as the 'culture wars*
### Page 77 @ 03 June 2023
*Moral/political questions have reemerged in a new way with the twenty-first-century 'social turn', in which art projects involve both the participation of 'nonartists' and the ethics of the process by which the work is made – is it sufficiently egalitarian? Is it elitist, authoritarian or even sadistic?*
### Page 80 @ 03 June 2023
*One way of extending the range of realism as a criterion is to deploy a subcategory of 'psychological realism,' as this draws in portraiture. John Berger was right, I think, to deny that the overwhelmingly majority of portraits offer any special insight into the psychology of their sitters.*
### Page 81 @ 03 June 2023
*During certain periods, notably the Middle Ages, painters and sculptors have been seen primarily as artisans working in a tradition in which they attempt merely to achieve the standards of previous 'masters' of the craft. This starts to change with the Renaissance, which reflects the birth of capitalism and its concomitant individualism, and already in the writings of Giorgio Vasari we find praise for artists as 'the first' to do this or that*
### Page 82 @ 03 June 2023
*The pursuit of originality and innovation comes to permeate the culture of modern art for artists, dealers, collectors and critics alike. It is manifested in the concept of 'the avant-garde', in the so-called 'march of the isms' and in the emergence of the artist's manifesto as a literary form in its own right.*
### Page 83 @ 03 June 2023
*The concept of 'critical' art as something praiseworthy contains two strands. The first relates to art which critiques the existing social conditions and the existing political order or dominant ideas within that order; examples would range from Käthe Kollwitz and George Grosz to Banksy. The second strand relates to art which critiques past or present art*
### Page 85 @ 03 June 2023
*But if we move from these rather rarefied theoretical heights to the language of everyday art critical journalism, of gallery notes and art school education, we find that in recent decades 'critical' has become for many, through a kind of trickle-down effect, the buzzword of choice, often combined with 'challenging'. Unsurprisingly, it is often applied rather indiscriminately to work that in reality offers little challenge to either the existing social order or the existing practices of art and the art world, or offers a challenge/critique on a very narrow and limited basis.*