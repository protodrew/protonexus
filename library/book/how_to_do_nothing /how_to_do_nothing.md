---
tags: [mental_health,productivity,capitalism]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---
# How to Do Nothing
## *Resiting The Attention Economy*
# Jenny Odell
---

"How to do Nothing: Resisting the Attention Economy" is an extended version of a talk of the same name by Jenny Odell. Originally conceptualized in the wake of the 2016 election; Odell ruminates on time spent in the Rose Garden in California, the inhuman rate of stimulation we are exposed to, and the dissolution of the boundaries between work and "free" time.

# Chapter 1: The Case for Nothing

Odell looks at art installations like "applause encouraged" which had guests sit and observe the sunset for 45 minutes, as well as techniques like deep listening and "bird noticing" that allow us to grow our internal perception.

Frequent references are made to marxist writer Franco Berardi's book "After the Future", specifically looking at how the proliferation of digital markets like Fiverr have damaged work-life boundaries.

> "In the global digital network, labor is transformed into small parcels of nervous energy picked up by the recombining mahine..The workers are deprived of every individual consisency. Strictly speaking, the workers no longer exist. Their time exists, their time is there, permanently available to connect, to produce in exchange for a temporary salary."
> *Franco Berardi: After the Future*

This level of noise and lack of distinction between personal and exploitable time also tempers the ability of meaningful political agitation

> "\[the modern italian regime\] is not founded on the repression of dissent; nor does it rest on the enforcement of silence. On the contrary, it relies on the proliferation of chatter, the irrelevance of opinion and discourse, and on making thought, dissent, and critique banal and ridiculous."
> *Franco Berardi: After the Future*

"Bird noticing" (an alternative name given to bird watching) specifically comes up several times throughout the first chapter. Allowing not only the growth of understanding when hearing bird sounds, but greater periods of time for reflection and space to see one's self as we are perceived by animals.

> "Inevitably, I began to wonder what these birds ese when they look at me. I assume they just see a human who for some reason pays attention to them. They don't know what my work is, they don't see progress —­they just see recurrence, day after day, week after week. And through them, I am able to inhabit that perspectivem to see myself as the human animal that I am, and when they fly off, to some extent, I can inhabit that perspective too"

Chapter one largerly dedicates itself to an outline of the why and the what of nothing. Explaining that the alienation we experience (both from the marxist and conventional social definitions) cheapens our lifves and makes it more difficult to maintain joy.

> "Gathering all this together, what I'm suggesting is that we take a protective stance toward ourselves, each other, and whatever is left of what makes us human—including the alliances that sustain and surprise us. I'm suggesting that we protect our spaces and our time for non-instrumental, noncommercial activity and thought, for maintenance, for care, for conviviality"

# Chapter 2: The Impossibility of Retreat

The second chapter looks at expamples of individuals or groups that attempted to withdraw from society, and how those utopian projects often fell apart.

> "First, as relatively recent versions of this experiment, the communes exemplify the problems with any imagined escape from the media and effects of capitalist society, including the role of privilege. Second, they show how easily an imagined apolitical "blank slate" leads to a technocratic solution where design has replaced politics, ironically presaging the libertarian dreams of Silicon Valley's tech moguls."

Oddell identifies the link between the hippie commune movement with modern libertarian attempts to create an ideal society is a desire to design the society. This design fails to account for the unpredictability that comes when multiple beings with free will interact, and resort to either recreating hierarchical rule, or collapsing due to interpersonal conflict.

> "As Arendt observes, part of what these escapes from politics are specifically avoiding is the "unpredictability" of "a plurality of agents." It's this ineradicable plurality of real people that spells the downfall of the Platonic city."

other stories in this chapter include a story of a monk named Thomas Merton who wrote a book called "The Seven Storey Mountain" explaining his retreat from society only to later repent from recluding from society.

> "I was suddenly overwhelmed with the realization that I loved all these people, that they were mine and I theirs, that we could not be alien to one another even though we were total strangers. It was like waking from a dream of separateness, of spurious self-isolation in a special world, the world of renunciation and supposed holiness."
> *Thomas Merton: Conjectures of a Guilty Bystander*

> "In one of those books, Contemplation in a World of Action, Merton reflects on the relationship between contemplation of the spiritual and participation in the worldly, two things the Church had long articulated as opposites. He found that they were far from mutually exclusive. Removal and contemplation were necessary to be able to see what was happening, but that same contemplation would always bring one back around to their responsibility to and in the world"

The book "Walden Two" written by the behavioral psychologist B. F. Skinner also comes up as an interesting look into the mind of people who think they have the design for an ideal society, but fail to actually understand the human condition outside of the clinical.

Oddell provides these examples of total retreat from society in order to explain how the purpose of this book is *not* to propose a total retreat from society, but to instead look at ways to disconnect from the attention economy (especially focusing on the environment created in modern media and social platforms).

> "But most important, standing apart represents the moment in which the desperate desire to leave (forever!) matures into a commitment to live in permanent refusal, where one already is, and to meet others in the common space of that refusal. This kind of resistance still manifests as participating, but participating in the "wrong way": a way that undermines the authority of the hegemonic game and creates possibilities outside of it."

# Chapter 3: Anatomy of a Refusal

Oddell then looks at examples of this "living in refusal", starting with Diogenes the philosopher influenced by Socrates, but ultimately spuring social convention to form a more aggressive break with social tradition and customs.

> "Diogenes practiced something closer to performance art. He lived his convictions out in the open and went to great lengths to shock people out of their habitual stupor, using a form of philosophy that was almost slapstick."

> "Diogenes thought every 'sane' person in the world was actually insane for heeding any of the customs upholding a world full of greed, corruption, and ignorance."

Diogones represents a perfect example of this refusal in place, where one continues to operate and engage with society without adhering to customs that misalign with their worldview. However these practices cannot typically insight any real change beyond potentially advancing a philosophical conversation. This is not to disparage the practice (pointing also to the performance artist Tehching Hsieh as someone particularly evokative), but to place it more in the realm of art than activism. It doesn't balance the individual adherence to values with a political desire to make change.

Thoreau is another key example, with his refusal to pay taxes in protest of the war in Mexico and retreating to a cabin near Walden pond (where he wrote the book of the same name) for a simpler life. This individual refusal is an existential hopelessness, and exemplifies the discipline that it takes to exist in this state (if individualistic)

> "This means, however, that even when he's let out of jail, Thoreau's perspective confines him to a life of permanent refusal. He 'quietly declares war on the state' and must live as an exile in a world that shares none of his values. Thoreau's own "state' is in fact what I described previously as 'standing apart.''' Viewing the present from the future, or injustice from the perspective of justice, Thoreau must live in the uncomfortable space of the unrealized. But hope and discipline keep him there, oriented toward 'a still more perfect and glorious State, which also I have imagined, but not yet anywhere seen'."

Oddell moves to discussions of modern labor activism as an example of the scale tilted toward community activism. The individual discipline discussed previously is now a challenge when trying to coordinate sustained refusal.

Practical tacticts like those deployed in the Bus boycotts are discussed, where the organization begins with meetings between individuals to coordinate how to reach outward to the larger community and organize a sustainable, successful movement.

However the difficulty of convincing people is rarely ideological when it comes to things that would obviously better their lives. It is instead a fear of being punished by the system that lives over you, pointing to the increasingly competitive education system, and examples of movements and individuals left burned when attempting to create a better world.

> "Differences in social and financial vulnerability explain why participants in mass acts of refusal have often been, and continue to be, students. James C. McMillan, an art professor at Bennett College who advised students when they participated in the 1960 Greensboro sit-ins, said that black adults were 'reluctant' to 'jeopardize any gains, economic and otherwise,'' but that the students 'did not have that kind of an investment, that kind of economic status, and, therefore, were not vulnerable to the kind of reprisals that could have occurred'"

This squeeze put on the working class has left a more fragile and atomized society where many individuals trapped within it feel unable to make any meaningful change without serious risk of ruining their own chances at success.

> "In the global 'spot market,' companies are driven only by the need to remain competitive, passing the task on to individuals to remain competitive as producing bodies."

> "In the context of attention, I'd further venture that this fear renders young people less able to concentrate individually or collectively. An atomized and competitive atmosphere obstructs individual attention because everything else disappears in a fearful and myopic battle for stability."