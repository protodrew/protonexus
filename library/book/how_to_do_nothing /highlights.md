---
type:
tags: 
creation-date: 2023-10-19
modification-date: 2023-11-14
---
---

tags: [mental_health,productivity,capitalism]
type: book
---
# Highlights
## From [[how_to_do_nothing]]
### Jenny Odell

## Chapter 1: The Case for Nothing

### Page 21 @ 02 July 2023 12:34:04 AM

*passage from Gilles Deleuze in Negotiations:

We're riddled with pointless talk, insane quantities of words and images. Stupidity's never blind or mute. So it's not a problem of getting people to express themselves but of providing little gaps of solitude and silence in which they might eventually find something to say*

### Page 23 @ 02 July 2023 12:45:12 AM

*THESE LAST FEW projects have something important in common. In each, the artist creates a structure—whether that's a map or a cordoned-off area (or even a lowly set of shelves!)—that holds open a contemplative space against the pressures of habit, familiarity, and distraction that constantly threaten to close it.*

### Page 24 @ 02 July 2023 01:01:56 AM

*Deep Listening was one of those techniques. Oliveros defines the practice as "listening in every possible way to every thing possible to hear no matter what you are doing. Such intense listening includes the sounds of daily life, of nature, of one's own thoughts as well as musical sounds."4*

### Page 25 @ 02 July 2023 01:15:00 AM

*This type of embarrassing discovery, in which something you thought was one thing is actually two things, and each of those two things is actually ten things, seems like a simple function of the duration and quality of one's attention. With effort, we can become attuned to things, able to pick up and then hopefully differentiate finer and finer frequencies each time.*

### Page 27 @ 02 July 2023 08:20:05 PM

*this period of reflection convinced Muir that "life was too brief and uncertain, and time too precious, to waste upon belts and saws; that while he was pottering in a wagon factory, God was making a world; and he determined that, if his eyesight was spared, he would devote the remainder of his life to a study of the process."7 Muir himself said, "This affliction has driven me to the sweet fields."8*

### Page 28 @ 02 July 2023 08:26:58 PM

*Of course, there's an obvious critique of all of this, and that's that it comes from a place of privilege.*

### Page 28 @ 02 July 2023 08:27:03 PM

*It's very possible to understand the practice of doing nothing solely as a self-indulgent luxury, the equivalent of taking a mental health day, if you're lucky enough to work at a place that has those.*

### Page 28 @ 02 July 2023 08:26:50 PM

*I come back to Deleuze's "right to say nothing," and just because this right is denied to many people doesn't make it any less of a right or any less important*

### Page 30 @ 02 July 2023 08:28:45 PM

*CityWalk, which one passes through upon leaving the Universal Studios theme park.*

### Page 30 @ 02 July 2023 08:28:35 PM

*In an essay about such spaces, Eric Holding and Sarah Chaplin call CityWalk "a 'scripted space' par excellence, that is, a space which excludes, directs, supervises, constructs, and orchestrates use."*

### Page 30 @ 02 July 2023 08:29:09 PM

*The Rose Garden is a public space. It is a Works Progress Administration (WPA) project from the 1930s, and like all WPA projects, was built by people put to work by the federal government during the Depression.*

### Page 31 @ 02 July 2023 08:30:18 PM

*Currently, I see a similar battle playing out for our time, a colonization of the self by capitalist ideas of productivity and efficiency.*

### Page 31 @ 02 July 2023 08:31:50 PM

*In the global digital network, labor is transformed into small parcels of nervous energy picked up by the recombining machine…The workers are deprived of every individual consistency. Strictly speaking, the workers no longer exist. Their time exists, their time is there, permanently available to connect, to produce in exchange for a temporary salary*

### Page 31 @ 02 July 2023 08:35:18 PM

*The removal of economic security for working people dissolves those boundaries—eight hours for work, eight hours for rest, eight hours for what we will—so that we are left with twenty-four potentially monetizable hours that are sometimes not even restricted to our time zones or our sleep cycles.*

### Page 33 @ 02 July 2023 08:42:30 PM

*this phenomenon—of work metastasizing throughout the rest of life—isn't constrained to the gig economy. I learned this during the few years that I worked in the marketing department of a large clothing brand. The office had instituted something called the Results Only Work Environment, or ROWE, which meant to abolish the eight-hour workday by letting you work whenever from wherever, as long as you got your work done. It sounded noble enough, but there was something in the name that bothered me. After all, what is the E in ROWE? If you could be getting results at the office, in your car, at the store, at home after dinner—aren't those all then "work environments"?*

### Page 34 @ 02 July 2023 08:43:35 PM

*Berardi, contrasting modern-day Italy with the political agitations of the 1970s, says the regime he inhabits "is not founded on the repression of dissent; nor does it rest on the enforcement of silence. On the contrary, it relies on the proliferation of chatter, the irrelevance of opinion and discourse, and on making thought, dissent, and critique banal and ridiculous."*

### Page 34 @ 02 July 2023 08:43:53 PM

*Instances of censorship, he says, "are rather marginal when compared to what is essentially an immense informational overload and an actual siege of attention, combined with the occupation of the sources of information by the head of the company."*

### Page 37 @ 02 July 2023 08:57:17 PM

*Inevitably, I began to wonder what these birds see when they look at me. I assume they just see a human who for some reason pays attention to them. They don't know what my work is, they don't see progress—they just see recurrence, day after day, week after week. And through them, I am able to inhabit that perspective, to see myself as the human animal that I am, and when they fly off, to some extent, I can inhabit that perspective too, noticing the shape of the hill that I live on and where all of the tall trees and good landing spots are.*

### Page 37 @ 02 July 2023 08:57:45 PM

*David Abram, asks in Becoming Animal: An Earthly Cosmology: "Do we really believe that the human imagination can sustain itself without being startled by other shapes of sentience?"20*

### Page 37 @ 02 July 2023 08:58:43 PM

*"Direct sensuous reality," writes Abram, "in all its more-than-human mystery, remains the sole solid touchstone for an experiential world now inundated with electronically generated vistas and engineered pleasures; only in regular contact with the tangible ground and sky can we learn how to orient and to navigate in the multiple dimensions that now claim us."21*

### Page 37 @ 02 July 2023 08:59:09 PM

*When I realized this, I grabbed on to it like a life raft, and I haven't let go. This is real. Your eyes reading this text, your hands, your breath, the time of day, the place where you are reading this—these things are real. I'm real too. I am not an avatar, a set of preferences, or some smooth cognitive force; I'm lumpy and porous, I'm an animal, I hurt sometimes, and I'm different one day to the next. I hear, see, and smell things in a world where others also hear, see, and smell me. And it takes a break to remember that: a break to do nothing, to just listen, to remember in the deepest sense what, when, and where we are.*

### Page 38 @ 02 July 2023 08:59:43 PM

*I think that "doing nothing"—in the sense of refusing productivity and stopping to listen—entails an active process of listening that seeks out the effects of racial, environmental, and economic injustice and brings about real change. I consider "doing nothing" both as a kind of deprogramming device and as sustenance for those feeling too disassembled to act meaningfully.*

### Page 38 @ 02 July 2023 09:00:13 PM

*When overstimulation has become a fact of life, I suggest that we reimagine #FOMO as #NOMO, the necessity of missing out, or if that bothers you, #NOSMO, the necessity of sometimes missing out*

### Page 39 @ 02 July 2023 09:01:02 PM

*self-care "is poised to be wrenched away from activists and turned into an excuse to buy an expensive bath oil."*

### Page 40 @ 02 July 2023 09:03:35 PM

*Our very idea of productivity is premised on the idea of producing something new, whereas we do not tend to see maintenance and care as productive in the same way.*

### Page 43 @ 03 July 2023 09:26:39 PM

*we are all familiar with the phenomenon of selfless care from at least some part of our lives. This phenomenon is no exception; it is at the core of what defines the human experience.*

### Page 43 @ 03 July 2023 09:28:18 PM

*Solnit suggests that the real disaster is everyday life, which alienates us from each other and from the protective impulse that we harbor.*

### Page 44 @ 03 July 2023 09:47:28 PM

*Gathering all this together, what I'm suggesting is that we take a protective stance toward ourselves, each other, and whatever is left of what makes us human—including the alliances that sustain and surprise us. I'm suggesting that we protect our spaces and our time for non-instrumental, noncommercial activity and thought, for maintenance, for care, for conviviality.*

### Page 44 @ 03 July 2023 09:47:55 PM

*most of this era's transcendent technological visions remain motivated by a fright of the body and its myriad susceptibilities, by a fear of our carnal embedment in a world ultimately beyond our control—by our terror of the very wildness that nourishes and sustains us."*

### Page 44 @ 03 July 2023 09:51:43 PM

*Certain people would like to use technology to live longer, or forever*

### Page 44 @ 03 July 2023 09:51:56 PM

*To such people I humbly propose a far more parsimonious way to live forever: to exit the trajectory of productive time, so that a single moment might open almost to infinity.*

### Page 44 @ 03 July 2023 09:54:52 PM

*look down at my phone and wonder if it isn't its own kind of sensory-deprivation chamber. That tiny, glowing world of metrics cannot compare to this one*

## Chapter 2: The Impossibility of Retreat

### Page 50 @ 03 July 2023 09:59:37 PM

*What's especially ironic about this is the exploitation of the basic and profound kernel of truth that Felix had initially started with as a collapsed workaholic. The answer he'd found was not a weekend retreat to become a better employee, but rather a total and permanent reevaluation of one's priorities—presumably similar to what had happened to him on his travels. In other words, digital distraction was a bane not because it made people less productive but because it took them away from the one life they had to live.*

### Page 51 @ 03 July 2023 10:03:30 PM

*Quite contrary to the modern-day meaning of the word epicurean—often associated with decadent and plentiful food—what the school of Epicurus taught was that man actually needed very little to be happy, as long as he had recourse to reason and the ability to limit his desires.*

### Page 53 @ 03 July 2023 10:06:25 PM

*First, as relatively recent versions of this experiment, the communes exemplify the problems with any imagined escape from the media and effects of capitalist society, including the role of privilege. Second, they show how easily an imagined apolitical "blank slate" leads to a technocratic solution where design has replaced politics, ironically presaging the libertarian dreams of Silicon Valley's tech moguls.*

### Page 64 @ 03 July 2023 10:41:28 PM

*Hannah Arendt's classic 1958 work The Human Condition, in which she diagnoses the age-old temptation to substitute design for the political process. Throughout history, she observes, men have been driven by the desire to escape "the haphazardness and moral irresponsibility inherent in a plurality of agents*

### Page 64 @ 03 July 2023 10:41:17 PM

*"the hallmark of all such escapes is rule, that is, the notion that men can lawfully and politically live together only when some are entitled to command and the others forced to obey."38 Arendt traces this temptation specifically to Plato and the phenomenon of the philosopher-king, who, like Frazier, builds his city according to an image:*

### Page 64 @ 03 July 2023 10:42:07 PM

*This substitution introduces a division between the expert/designer and the layman/executor, or "between those who know and do not act and those who act and do not know."*

### Page 66 @ 04 July 2023 01:39:43 AM

*As Arendt observes, part of what these escapes from politics are specifically avoiding is the "unpredictability" of "a plurality of agents." It's this ineradicable plurality of real people that spells the downfall of the Platonic city.*

### Page 67 @ 04 July 2023 01:43:41 AM

*Preemptively calling it a "peaceful project" avoids the fact that regardless of how high-tech your society might be, "peace" is an endless negotiation among free-acting agents whose wills cannot be engineered.*

### Page 67 @ 04 July 2023 01:43:56 AM

*Politics necessarily exist between even two individuals with free will; any attempt to reduce politics to design (Thiel's "machinery of freedom") is also an attempt to reduce people to machines or mechanical beings.*

### Page 70 @ 04 July 2023 02:13:57 AM

*Even the crowd-shunning Epicurus, who taught that one shouldn't speak in public unless requested to do so, showed some orientation toward the outside world by using his house as a base for publishing the writings of the school. It's only for this reason that in 2018, someone (me) is reading them in another garden. It's in this exchange that such experiments become valuable for the world, as points in a dialogue between inside and outside, real and unrealized.*

### Page 70 @ 04 July 2023 02:14:36 AM

*Indeed, so instinctively do we understand the value of an outsider's perspective that history is full of people seeking remote hermits and sages, desperate for knowledge from a mind unconcerned with familiar comforts.*

### Page 71 @ 04 July 2023 02:33:53 AM

*In any narrative of escape, this is a pivotal point. Do you pack all your things in a van, say, "Fuck it," and never look back? What responsibility do you have to the world you left behind, if any? And what are you going to do out there? The experiences of the 1960s communes suggest that these are not easy questions to answer.*

### Page 73 @ 04 July 2023 02:42:55 AM

*In one of those books, Contemplation in a World of Action, Merton reflects on the relationship between contemplation of the spiritual and participation in the worldly, two things the Church had long articulated as opposites. He found that they were far from mutually exclusive. Removal and contemplation were necessary to be able to see what was happening, but that same contemplation would always bring one back around to their responsibility to and in the world*

### Page 73 @ 04 July 2023 02:44:53 AM

*Here's what I want to escape. To me, one of the most troubling ways social media has been used in recent years is to foment waves of hysteria and fear, both by news media and by users themselves. Whipped into a permanent state of frenzy, people create and subject themselves to news cycles, complaining of anxiety at the same time that they check back ever more diligently*

### Page 74 @ 04 July 2023 02:44:44 AM

*this kind of hyper-accelerated expression on social media is not exactly helpful (not to mention the huge amount of value it produces for Facebook). It's not a form of communication driven by reflection and reason, but rather a reaction driven by fear and anger. Obviously these feelings are warranted, but their expression on social media so often feels like firecrackers setting off other firecrackers in a very small room that soon gets filled with smoke.*

### Page 74 @ 04 July 2023 02:53:01 AM

*Our aimless and desperate expressions on these platforms don't do much for us, but they are hugely lucrative for advertisers and social media companies, since what drives the machine is not the content of information but the rate of engagement*

### Page 76 @ 04 July 2023 02:55:31 AM

*But most important, standing apart represents the moment in which the desperate desire to leave (forever!) matures into a commitment to live in permanent refusal, where one already is, and to meet others in the common space of that refusal. This kind of resistance still manifests as participating, but participating in the "wrong way": a way that undermines the authority of the hegemonic game and creates possibilities outside of it.*

## Chapter 3: Anatomy of a Refusal

### Page 79 @ 05 July 2023 10:42:20 PM

*Diogenes had come under the influence of Antisthenes, a disciple of Socrates. He was thus heir to a development in Greek thought that prized the capacity for individual reason over the hypocrisy of traditions and customs, even and especially if they were commonplace. But one of the differences between Socrates and Diogenes was that, while Socrates famously favored conversation, Diogenes practiced something closer to performance art. He lived his convictions out in the open and went to great lengths to shock people out of their habitual stupor, using a form of philosophy that was almost slapstick.*

### Page 79 @ 05 July 2023 10:44:46 PM

*Diogenes thought every "sane" person in the world was actually insane for heeding any of the customs upholding a world full of greed, corruption, and ignorance.*

### Page 82 @ 05 July 2023 10:50:37 PM

*to a question like "Will you or will you not participate as asked?" Diogenes would have answered something else entirely: "I will participate, but not as asked," or, "I will stay, but I will be your gadfly." This answer (or non-answer) is something I think of as producing what I'll call a "third space"—an almost magical exit to another frame of reference*

### Page 85 @ 05 July 2023 10:56:02 PM

*we believed that everything were merely a product of fate or disposition, Cicero reasons, no one would be accountable for anything and therefore there could be no justice. In today's terms, we'd all just be algorithms. Furthermore, we'd have no reason to try to make ourselves better or different from our natural inclinations*

### Page 87 @ 05 July 2023 10:59:49 PM

*In a 2012 interview, Hsieh says that he is not an endurance artist, yet he also says that the most important word to him is "will."26 This makes sense if we accept that Cage Piece is less a feat of endurance than an experiment. In the interview, Hsieh, who was preoccupied with time and survival, described the process by which people fill up their time in an attempt to fill their lives with meaning. He was earnestly interested in the opposite: What would happen if he emptied everything out? His search for this answer occasioned the experiment's many harsh "controls"—for it to work, it needed to be pure. "I brought my isolation to the public while still preserving the quality of it," he said.*

### Page 88 @ 05 July 2023 11:13:47 PM

*Like Plato with his allegory of the cave, Thoreau imagines truth as dependent on perspective. "Statesmen and legislators, standing so completely within the institution never distinctly and nakedly behold it," he says. One must ascend to higher ground to see reality: the government is admirable in many respects, "but seen from a point of view a little higher they are what I have described them; seen from a higher still, and the highest, who shall say what they are, or that they are worth looking at or thinking of at all?"*

### Page 88 @ 05 July 2023 11:21:12 PM

*In a society where men have become law-abiding machines, the worst men are the best, and the best men are the worst. The soldiers going to fight the war in Mexico "command no more respect than men of straw or a lump of dirt"; the government would "crucify Christ, and excommunicate Copernicus and Luther, and pronounce Washington and Franklin rebels," and the only place in town where Thoreau feels truly free is prison. For him, to be alive is to exercise moral judgment, but by those standards, almost everyone around him is already dead.*

### Page 89 @ 05 July 2023 11:29:13 PM

*This means, however, that even when he's let out of jail, Thoreau's perspective confines him to a life of permanent refusal. He "quietly declares war on the state" and must live as an exile in a world that shares none of his values. Thoreau's own "state" is in fact what I described previously as "standing apart." Viewing the present from the future, or injustice from the perspective of justice, Thoreau must live in the uncomfortable space of the unrealized. But hope and discipline keep him there, oriented toward "a still more perfect and glorious State, which also I have imagined, but not yet anywhere seen."*

### Page 93 @ 05 July 2023 11:41:30 PM

*pay attention to one thing is to resist paying attention to other things; it means constantly denying and thwarting provocations outside the sphere of one's attention. We contrast this with distraction, in which the mind is disassembled, pointing in many different directions at once and preventing meaningful action*

### Page 94 @ 05 July 2023 11:46:16 PM

*I draw the connection between individual and collective concentration because it makes the stakes of attention clear. It's not just that living in a constant state of distraction is unpleasant, or that a life without willful thought and action is an impoverished one. If it's true that collective agency both mirrors and relies on the individual capacity to "pay attention," then in a time that demands action, distraction appears to be (at the level of the collective) a life-and-death matter. A social body that can't concentrate or communicate with itself is like a person who can't think and act*

### Page 94 @ 05 July 2023 11:46:49 PM

*This "schizoid" collective brain cannot act, only react blindly and in misaligned ways to a barrage of stimuli, mostly out of fear and anger. That's bad news for sustained refusal. While it may seem at first like refusal is a reaction, the decision to actually refuse—not once, not twice, but perpetually until things have changed—means the development of and adherence to individual and collective commitments from which our actions proceed.*

### Page 94 @ 05 July 2023 11:47:20 PM

*The actual play-by-play of the bus boycott is a reminder that meaningful acts of refusal have come not directly from fear, anger, and hysteria, but rather from the clarity and attention that makes organizing possible*

### Page 95 @ 05 July 2023 11:48:37 PM

*THE PROBLEM IS that many people have a lot to fear, and for good reason. The relationship between fear and the ability to refuse is clear when we consider that historically, some can more easily afford to refuse than others. Refusal requires a degree of latitude—a margin—enjoyed at the level of the individual (being able to personally afford the consequences) and at the level of society (whose legal attitude toward noncompliance may vary).*

### Page 95 @ 05 July 2023 11:49:00 PM

*Even Diogenes, who would seem to have nothing to lose, existed in a kind of margin. Navia quotes Farrand Sayre, a critic of Diogenes, who suggests that Greek cities were friendly to him in their laws and weather:*

### Page 95 @ 05 July 2023 11:49:39 PM

*Differences in social and financial vulnerability explain why participants in mass acts of refusal have often been, and continue to be, students. James C. McMillan, an art professor at Bennett College who advised students when they participated in the 1960 Greensboro sit-ins, said that black adults were "reluctant" to "jeopardize any gains, economic and otherwise," but that the students "did not have that kind of an investment, that kind of economic status, and, therefore, were not vulnerable to the kind of reprisals that could have occurred."*

### Page 96 @ 05 July 2023 11:56:58 PM

*his book on the San Francisco General Strike, Selvin describes the futility of individual acts of refusal before the 1933 National Industrial Recovery Act guaranteed the right to join trade unions:

In a free labor market, of course, the longshoreman or seaman was free to accept the shipowner's offer or leave it; in practical terms, standing alone, without resources, living on the edge of subsistence, the longshoreman or seaman was powerless to resist.46*

### Page 98 @ 05 July 2023 11:58:38 PM

*In the global "spot market," companies are driven only by the need to remain competitive, passing the task on to individuals to remain competitive as producing bodies.*

### Page 98 @ 06 July 2023 12:00:20 AM

*This "new contract," alongside other missing forms of government protection, closes the margin for refusal and leads to a life lived in economic fear. When Hacker describes the new situation faced by those for whom precarity was not already a matter of course, the margin has eroded completely: "Americans increasingly find themselves on an economic tightrope, without an adequate safety net if—as is ever more likely—they lose their footing."50 Any argument about mindfulness or attention must address this reality*

### Page 99 @ 06 July 2023 12:04:03 AM

*In 2016, the writer and blogger Talia Jane took the risk of protest and lost. She had been working as a customer-service representative at Yelp, but was having trouble making ends meet due to the high cost of living in the Bay Area. After writing an open letter to Yelp about her situation and asking for a living wage, she was fired, given $1,000 severance, and banned from returning. Yelp later raised its wages, though it denied that she had anything to do with it. Jane's story became a touch point in the conversation about Millennials, making her something of a public figure. But in September 2018, she was still looking for meaningful work.*

### Page 100 @ 06 July 2023 12:05:04 AM

*Stanford duck syndrome." This phrase, which imagines students as placid-seeming ducks paddling strenuously beneath the water, is essentially a joke about isolated struggle in an atmosphere obsessed with performance. In "Duck syndrome and a culture of misery," Stanford Daily writer Tiger Sun describes seeing a friend pull two consecutive all-nighters on her birthday weekend. Sun and his friends grow concerned when they notice her face is flushed, so they take her temperature: 102.1 degrees. But when they implore her to stop, she keeps working. Sun writes:

It's a testament to this toxic "grind or die" atmosphere at universities that, even in the face of major illness, we put the pedal to the metal and continue to drive our health off a cliff. It's not like this is a conscious decision to be miserable, but sometimes it feels as if taking care of our own health is a guilty pleasure…We subliminally equate feeling burned out to being a good student.*

### Page 100 @ 06 July 2023 12:06:27 AM

*However much the squeeze is humorously acknowledged, and however much Stanford or even the students among themselves might emphasize self-care, they're running up against the same market demands haunting all of us.*

### Page 100 @ 06 July 2023 12:06:59 AM

*Blowing off steam by commenting "legit me" on a meme about sleep deprivation, or even allowing oneself a day off to catch up on sleep (!), can't help with the overarching issue of economic precarity that awaits the student—and indeed has already reached less privileged students who must work in addition to studying. It does nothing about the specter of student debt, nor about the fear of ending up outside a shrinking pool of security.*

### Page 101 @ 06 July 2023 12:08:12 AM

*Knowing this, I can forgive my students for getting frustrated that my art classes aren't "practical" in any easily demonstrable sense. I've come to suspect that it's not a lack of imagination on their parts. Rather, I'd venture that it's an awareness of the cold hard truth that every minute counts toward the project of gainful employment. In Kids These Days: Human Capital and the Making of Millennials, in which Malcolm Harris takes us through the ruthless professionalization of childhood and education, Harris writes that "[i]f enough of us start living this way, then staying up late isn't just about pursuing an advantage, it's about not being made vulnerable."*

### Page 102 @ 06 July 2023 12:09:12 AM

*In the context of attention, I'd further venture that this fear renders young people less able to concentrate individually or collectively. An atomized and competitive atmosphere obstructs individual attention because everything else disappears in a fearful and myopic battle for stability*

## Chapter 4: Exercises in Attention

### Page 110 @ 22 July 2023 02:02:59 PM

*"[w]e do not look at the world from a distance; we are in it, and that's how we feel."*

### Page 111 @ 22 July 2023 02:04:23 PM

*These disjunctures and discrepancies in size undermine any sense of continuity or punctum. Without the familiar framework of a consistent vanishing point, the eye roams across the scene, dwelling in small details and trying to add it all up. This process forces us to notice our own "construction" of every scene that we perceive as living beings in a living world.*

### Page 113 @ 22 July 2023 02:09:36 PM

*In this sense, what Hockney and countless other artists offer is a kind of attentional prosthesis. Such an offering assumes that the familiar and proximate environment is as deserving of this attention, if not more, than those hallowed objects we view in a museum*

### Page 114 @ 22 July 2023 02:31:41 PM

*The film is called The Exchange, by Eran Kolirin, and to be honest, it doesn't have much of a plot. A PhD student forgets something at home, goes back to get it, and finds that his apartment looks unfamiliar at that particular time of the day. (I'm convinced that many of us have had this experience as a child, coming home sick from school in the middle of the day and finding that our home feels strange.) Critically unmoored from the familiar, the man spends the rest of the film doing things like pushing a paperweight matter-of-factly off a coffee table, throwing a stapler out a window, standing in bushes, or lying on the floor of his apartment's basement level.*

### Page 115 @ 22 July 2023 02:32:01 PM

*Like the visitors to the Hockney piece who reported "seeing things" afterward—or like myself walking down Grove Street transfixed by sound—the film's turning point is entirely perceptual. It has to do with how endlessly strange reality is when we look at it rather than through it.*

### Page 115 @ 22 July 2023 02:33:59 PM

*Through attention and curiosity, we can suspend our tendency toward instrumental understanding—seeing things or people one-dimensionally as the products of their functions—and instead sit with the unfathomable fact of their existence, which opens up toward us but can never be fully grasped or known*

### Page 117 @ 22 July 2023 02:35:51 PM

*For Buber, the question is misguided because it relapses into I-It thinking: "must you again divide the indivisible? What I encounter is neither the soul of a tree nor a dryad, but the tree itself."11*

### Page 119 @ 22 July 2023 03:34:31 PM

*But an actual painting, as opposed to a picture, confronts us in physical space:

[Newman's new] paintings are objects in their own right. A picture represented something other than itself; a painting represents itself. A picture mediates between a viewer and an object in pictorial space; a painting is an object to which the viewer relates without mediation…It is on the surface and in the same space as we are. Painting and viewer coexist in the same reality*

### Page 120 @ 22 July 2023 03:36:58 PM

*These paintings taught me about attention and duration, and that what I'll see depends on how I look, and for how long. It's a lot like breathing. Some kind of attention will always be present, but when we take hold of it, we have the ability to consciously direct, expand, and contract it.*

### Page 120 @ 22 July 2023 04:07:28 PM

*all of the artworks I've described so far could be thought of as training apparatuses for attention. By inviting us to perceive at different scales and tempos than we're used to, they teach us not only how to sustain attention but how to move it back and forth between different registers*

### Page 123 @ 22 July 2023 04:12:34 PM

*"To [break a habit], Devine said, you have to be aware of it, motivated to change, and have a strategy for replacing it."*

### Page 125 @ 22 July 2023 04:32:43 PM

*James Williams (of Time Well Spent) lays out the stakes:

We experience the externalities of the attention economy in little drips, so we tend to describe them with words of mild bemusement like "annoying" or "distracting." But this is a grave misreading of their nature. In the short term, distractions can keep us from doing the things we want to do. In the longer term, however, they can accumulate and keep us from living the lives we want to live, or, even worse, undermine our capacities for reflection and self-regulation, making it harder, in the words of Harry Frankfurt, to "want what we want to want*

### Page 130 @ 22 July 2023 05:06:42 PM

*As a response to the attention economy, the argument for ethical persuasion happens on a two-dimensional plane that assumes that attention can only be directed this way or that way. I am not as interested in that plane as I am interested in a disciplined deepening of attention.*

### Page 133 @ 22 July 2023 05:15:38 PM

*I began to notice animal communities, plant communities, animal-plant communities; mountain ranges, fault lines, watersheds. It was a familiar feeling of disorientation, realized in a different arena. Once again, I was met with the uncanny knowledge that these had all been here before, yet they had been invisible to me in previous renderings of my reality*

### Page 133 @ 22 July 2023 05:16:16 PM

*as well as an appreciation for the complex web of relationships among those actors. More than observation, it also suggests a way of identifying with place, weaving oneself into a region through observation of and responsibility to the local ecosystem. (Asked where he was from, Peter Berg, an early proponent of bioregionalism, used to answer, "I am from the confluence of the Sacramento River and San Joaquin River and San Francisco Bay, of the Shasta bioregion, of the North Pacific Rim of the Pacific Basin of the Planet Earth."28) In these ways, bioregionalism is not just a science, but a model for community*

### Page 133 @ 22 July 2023 05:17:14 PM

*my experience suggests that while it initially takes effort to notice something new, over time a change happens that is irreversible. Redwoods, oaks, and blackberry shrubs will never be "a bunch of green." A towhee will never simply be "a bird" to me again, even if I wanted it to be. And it follows that this place can no longer be any place.*

### Page 136 @ 22 July 2023 05:21:43 PM

*Long before cars drove from Whole Foods to the Apple campus, the creek moved water from Table Mountain to the San Francisco Bay. It continues to do this just as it always has, and whether I or any other humans care to notice. But when we do notice, like all things we give our sustained attention to, the creek begins to reveal its significance.*

### Page 136 @ 22 July 2023 05:23:23 PM

*In that sense, the creek is a reminder that we do not live in a simulation—a streamlined world of products, results, experiences, reviews—but rather on a giant rock whose other life-forms operate according to an ancient, oozing, almost chthonic logic. Snaking through the midst of the banal everyday is a deep weirdness, a world of flowerings, decompositions, and seepages, of a million crawling things, of spores and lacy fungal filaments, of minerals reacting and things being eaten away—all just on the other side of the chain-link fence.*

### Page 136 @ 22 July 2023 05:29:30 PM

*Realities are, after all, inhabitable. If we can render a new reality together—with attention—perhaps we can meet each other there*

## Chapter 5: Ecology of Strangers

### Page 138 @ 22 July 2023 05:31:35 PM

*the paramedics assumed I was her friend and that we had been walking together. No, I said, I was just a passerby. At this, one of the paramedics thanked me for staying, implying that this was an inconvenience. But that other world—in which I had been walking to the grocery store to buy things for dinner—was so remote that I could barely remember what I was supposed to have been doing.*

### Page 138 @ 22 July 2023 05:33:26 PM

*Just as earthquakes remind us that we live on floating plates, once I'd been confronted with the fragility of another person's life, I was momentarily unable to see anything as given.*

### Page 139 @ 22 July 2023 05:49:50 PM

*As it turns out, that choice is basically one of attention:

if I don't make a conscious decision about how to think and what to pay attention to, I'm going to be pissed and miserable every time I have to food-shop, because my natural default-setting is the certainty that situations like this are really all about me, about my hungriness and my fatigue and my desire to just get home, and it's going to seem, for all the world, like everybody else is just in my way,*

### Page 139 @ 22 July 2023 05:49:56 PM

*But if you've really learned how to think, how to pay attention, then you will know you have other options. It will actually be within your power to experience a crowded, loud, slow, consumer-hell-type situation as not only meaningful but sacred, on fire with the same force that lit the stars—compassion, love, the sub-surface unity of all things.*

### Page 142 @ 23 July 2023 02:01:36 PM

*I thought about how it's possible to move to a place without caring about who or what is already there (or what was there before), interested in the neighborhood only insofar as it allows one to maintain your existing or ideal lifestyle and social ties. Like Buber's "I-It" relationship, a newcomer might only register other people and things in the neighborhood to the extent that they seem in some way useful, imagining the remainder as (at best) inert matter or (at worst) a nuisance or inefficiency.*

### Page 146 @ 23 July 2023 02:08:49 PM

*When the language of advertising and personal branding enjoins you to "be yourself," what it really means is "be more yourself," where "yourself" is a consistent and recognizable pattern of habits, desires, and drives that can be more easily advertised to and appropriated, like units of capital. In fact, I don't know what a personal brand is other than a reliable, unchanging pattern of snap judgments: "I like this" and "I don't like this," with little room for ambiguity or contradiction.*

### Page 147 @ 23 July 2023 02:12:52 PM

*If I think I know everything that I want and like, and I also think I know where and how I'll find it—imagining all of this stretching endlessly into the future without any threats to my identity or the bounds of what I call my self—I would argue that I no longer have a reason to keep living*

### Page 147 @ 23 July 2023 02:13:16 PM

*Extrapolating this into the realm of strangers, I worry that if we let our real-life interactions be corralled by our filter bubbles and branded identities, we are also running the risk of never being surprised, challenged, or changed—never seeing anything outside of ourselves, including our own privilege.*

### Page 148 @ 23 July 2023 02:17:13 PM

*What's especially tragic about a mind that imagines itself as something separate, defensible, and capable of "efficiency" is not just that it results in a probably very boring (and bored) person; it's that it's based on a complete fallacy about the constitution of the self as something separate from others and from the world.*

### Page 151 @ 23 July 2023 02:22:20 PM

*When I try to think about thinking, for instance retracing where an idea of mine came from, the limitations of English force me to say that "I" "produced" an "idea." But none of these things are stable entities, and this grammatical relationship among them is misleading. The "idea" isn't a finished product with identifiable boundaries that one moment sprung into being*

### Page 151 @ 23 July 2023 02:23:30 PM

*Using examples like the coevolution of vision with certain colors that occur in nature, they fundamentally complicate the idea that perception merely gives information about what's "out there." As they put it, "Cognition is not the representation of a pre-given world by a pre-given mind but is rather the enactment of a world and a mind"*

### Page 152 @ 23 July 2023 02:25:25 PM

*She adds that "[a]s our human dominance has grown, we have become more isolated, more lonely when we can no longer call out to our neighbors."

I looked over at my neighbor, the song sparrow, and thought about how just a few years ago, I wouldn't have known its name, might not have even known it was a sparrow, might not have even seen it at all.*

### Page 154 @ 23 July 2023 02:28:17 PM

*Kimmerer tells us that in the Anishinaabe creation story, Nanabozho, the first man, is placed on earth with the instruction to absorb the wisdom of other inhabitants and to learn their names. Kimmerer imagines a friendly rapport between Anishinaabe and Carl Linnaeus, the father of the modern taxonomic system. Walking together and observing the local flora and fauna, the two observers complement each other: "Linnaeus lends Nanabozho his magnifying glass so he can see the tiny floral parts. Nanabozho gives Linnaeus a song so he can see their spirits. And neither of them are lonely."*

### Page 154 @ 23 July 2023 02:32:48 PM

*In Becoming Animal, David Abram writes about what is lost when we speak and think about the rest of the world as less than animate:

If we speak of things as inert or inanimate objects, we deny their ability to actively engage and interact with us—we foreclose their capacity to reciprocate our attentions, to draw us into silent dialogue, to inform and instruct us.13*

### Page 155 @ 23 July 2023 02:33:44 PM

*Even though I know I am often getting an insufficient English (and written) version of them, I have long appreciated the way that indigenous stories animate the world. They are not only repositories of observations and analyses made over millennia, but also models of gratitude and stewardship.*

### Page 155 @ 23 July 2023 02:34:16 PM

*Kimmerer writes about overseeing a study by her graduate student on the decline of sweetgrass, a plant traditionally harvested by Kimmerer's ancestors and which figures in the Anishinaabe creation story. The study revealed that the sweetgrass was suffering not from over-harvesting but from under-harvesting. The species had co-evolved with specific indigenous harvesting practices, which in turn had specifically evolved to increase the success of the plant. A specific type of human attention, use, and stewardship had become environmental factors on which the plants depended on, and without these things, they've begun to disappear.15*

### Page 156 @ 23 July 2023 02:49:34 PM

*"you cannot love game and hate predators; you cannot conserve waters and waste the ranges; you cannot build the forest and mine the farm. The land is one organism."17 Even if you cared only about human survival, you'd still have to acknowledge that this survival is beholden not to efficient exploitation but to the maintenance of a delicate web of relationships.*

### Page 157 @ 23 July 2023 02:50:30 PM

*just as attention may be the last resource we have to withhold, the physical world is our last common reference point. At least until everyone is wearing augmented reality glasses 24/7, you cannot opt out of awareness of physical reality. The fact that commenting on the weather is a cliché of small talk is actually a profound reminder of this, since the weather is one of the only things we each know any other person must pay attention to.*

### Page 159 @ 23 July 2023 04:28:49 PM

*Resisting definition like headwaters resist pinpointing, we emerge from moment to moment, just as our relationships do, our communities do, our politics do. Reality is blobby. It refuses to be systematized. Things like the American obsession with individualism, customized filter bubbles, and personal branding—anything that insists on atomized, competing individuals striving in parallel, never touching—does the same violence to human society as a dam does to a watershed.*

### Page 160 @ 23 July 2023 04:46:32 PM

*"Difference must be not merely tolerated, but seen as a fund of necessary polarities between which our creativity can spark like a dialectic," she says. "Only then does the necessity for interdependency become unthreatening."*

### Page 161 @ 23 July 2023 04:48:05 PM

*A community in the thrall of the attention economy feels like an industrial farm, where our jobs are to grow straight and tall, side by side, producing faithfully without ever touching. Here, there is no time to reach out and form horizontal networks of attention and support—nor to notice that all the non-"productive" life-forms have fled.*

### Page 161 @ 23 July 2023 04:50:02 PM

*Schulman struggles to account for this "weird passivity that accompanies gentrification."21 I would venture that the newer tenants, though they were troubled by the conditions, ran up against the wall of individualism. Once they understood that something was not just their problem but a collective problem, requiring collective action and identification with a community to be solved, it was preferable to them to just drop it. That is, even rats and dark hallways were not too high a price to pay for the ability to keep the doors of the self shut to outsiders, to change, and to the possibility of a new kind of identity.*

### Page 162 @ 23 July 2023 04:50:45 PM

*Bioregionalism teaches us of emergence, interdependence, and the impossibility of absolute boundaries. As physical beings, we are literally open to the world, suffused every second with air from somewhere else; as social beings, we are equally determined by our contexts. If we can embrace that, then we can begin to appreciate our and others' identities as the emergent and fluid wonders that they are.*

## Chapter 6: Restoring the Grounds for Thought

### Page 167 @ 23 July 2023 01:50:52 PM

*Alice E. Marwick found that Twitter users who had built the most successful personal brands did so by recognizing the fact that they no longer really knew who their audience was. To tweet was to throw a message into a void that could include close friends, family, potential employers, and (as recent events have shown us) sworn enemies. Marwick and boyd describe how context collapse creates a "lowest-common-denominator philosophy of sharing [that] limits users to topics that are safe for all possible readers*

### Page 170 @ 28 July 2023 01:22:50 PM

*In Martin Luther King, Jr.'s, description of the planning that led up to the Montgomery bus boycott, he describes meetings of varying sizes, all happening in different rooms of homes, schools, and churches over the course just a few days.7 These meetings were anywhere from very small (King deliberating with his wife at home) to small (King, E. D. Nixon, and Ralph Abernathy alternating calling each other on the phone) to midsize (a meeting with King, L. Roy Bennett, E. D. Nixon, and a handful of others at a church) to large (Montgomery's black leaders from different businesses and organizations, at King's church) to very large (a meeting open to the public, at another church). It was at the smaller meetings that they strategized how to run the larger meetings, collaborating quickly and intensely on ideas that would be put into play in successively wider contexts.*

### Page 171 @ 28 July 2023 01:25:00 PM

*First, instantaneous communication threatens visibility and comprehension because it creates an information overload whose pace is impossible to keep up with. Activists, Barassi says, "have to adapt to the pace of information and constantly produce content." Meanwhile, information overload creates the risk that nothing gets heard.*

### Page 171 @ 28 July 2023 01:25:08 PM

*Everybody says that there is no censorship on the internet, or at least only in part. But that is not true. Online censorship is applied through the excess of banal content that distracts people from serious or collective issues.9*

### Page 172 @ 28 July 2023 01:26:17 PM

*Second, the immediacy of social media closes down the time needed for "political elaboration." Because the content that activists share online has to be "catchy," "activists do not have the space and time to articulate their political reflections."*

### Page 172 @ 28 July 2023 01:26:09 PM

*Lastly, immediacy challenges political activism because it creates "weak ties." Barassi's research suggests that networks built on social media "are often based on a common reaction / emotion and not on a shared political project and neither on a shared understanding of social conflict."*

### Page 172 @ 28 July 2023 01:27:21 PM

*What becomes clear in Barassi's analysis is that thought and deliberation require not just incubation space (solitude and/or a defined context) but incubation time.*

### Page 178 @ 28 July 2023 01:33:25 PM

*for social media companies, "the public sphere is an historically elapsed phase from the twentieth century they now exploit for their own interests by simulating it."16*

### Page 184 @ 28 July 2023 01:43:49 PM

*The history of collective action—from artistic movements to political activism—is still one of in-person meetings in houses, in squats, in churches, in bars, in cafés, in parks. In these federated spaces of appearance, disagreements and debates were not triggers that shut the whole discussion down, but rather an integral part of group deliberation, and they played out in a field of mutual responsibility and respect.*

### Page 185 @ 28 July 2023 02:18:11 PM

*This is where I think the idea of "doing nothing" can be of the most help. For me, doing nothing means disengaging from one framework (the attention economy) not only to give myself time to think, but to do something else in another framework.*

### Page 186 @ 28 July 2023 02:57:22 PM

*When I try to imagine a sane social network it is a space of appearance: a hybrid of mediated and in-person encounters, of hours-long walks with a friend, of phone conversations, of closed group chats, of town halls. It would allow true conviviality—the dinners and gatherings and celebrations that give us the emotional sustenance we need,*

### Page 187 @ 28 July 2023 03:02:31 PM

*I grew up thinking that parks were somehow just "leftover" spaces, but I've learned that the story of any park or preserve is absolutely one of "redemption preserve[ing] itself in a small crack in the continuum of catastrophe*

### Page 187 @ 28 July 2023 03:03:09 PM

*If, as I've argued, certain types of thought require certain types of spaces, then any attempt at "context collection" will have to deal not only with context collapse online, but with preserving public and open space, as well as the meeting places important to threatened cultures and communities*

### Page 189 @ 28 July 2023 03:04:57 PM

*It's a bit like falling in love—that terrifying realization that your fate is linked to someone else's, that you are no longer your own. But isn't that closer to the truth anyway? Our fates are linked, to each other, to the places where we are, and everyone and everything that lives in them*

### Page 190 @ 28 July 2023 03:16:25 PM

*I FIND THAT I'm looking at my phone less these days. It's not because I went to an expensive digital detox retreat, or because I deleted any apps from my phone, or anything like that. I stopped looking at my phone because I was looking at something else, something so absorbing that I couldn't turn away*