---
tags: [marxism, politics, economics, philosophy]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# In Dialogue with Marxism

from [[socialism_with_chinese_characteristics]]

Boer spends a significant portion of the introduction discussing the origin of the term "socialism with Chinese characteristics" from a philosophical perspective. He maintains that the utility of the term highlights that socialism is the center of contemporary Chinese politics and philosophy, not some new "ism" or holistic philosophical upheaval.

> Chinese Characteristics … entail China's specific practice of Marxism, there era in which China finds itself, and China's culture and history.
> *Roland Boer*

As for the origin of the term, it can be traced back to the Zunyi Conference of January 1935 which led to the sino-soviet split, and reinforced the desire to root the development of socialism as an expression of and framework for action, rather than a purely academic endeavor.

To this day the CPC maintains Marx's writings are a central axis upon which Socialism with Chinese Characteristics exists, but seeks to adapt and modernize the theory without imposing their particular revolutionary strategy onto developing socialist projects.

> The "target" is the Chinese revolution, the "arrow" is Marxism-Leninism
> *Mao Zedong*
