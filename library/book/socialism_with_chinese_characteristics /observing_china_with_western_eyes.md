---
tags: [marxism, politics, economics, philosophy]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Observing China with Western Eyes

from [[socialism_with_chinese_characteristics]]

Boer contends that a large contributing factor to the misunderstanding of China as a socialist project lies in interpreting Chinese practices, culture, and history through the current dominant neoliberal ideology.

Through this warped lens we understand politics as an antagonistic conflict between parties, civil society against the state, and democracy itself redefined through the elections between different political parties.

The western leftist is not immune to this through defining markets as antithetical to socialism. This leads to views of Deng Xiaoping and the great reopening as a betrayal of Chinese Marxism and the end of a revolutionary project.

> This whole framework and its usually unquestioned assumptions produces strange works that seek to analyze China as an emerging capitalist market economy, with arising middle class that would demand its liberal 'freedom and democracy' were it not for a repressive Communist Party that is 'conservative' to the core.
> *Roland Boer*