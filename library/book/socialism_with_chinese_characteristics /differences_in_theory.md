---
tags: [marxism, politics, economics, philosophy]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Differences in Theory

## Between Contemporary Leftists and Chinese Communists

from [[socialism_with_chinese_characteristics]]

These observations come from a 2012 conference on Lenin held in Wuhan in 2012 that had a large number of Western delegates. Roland observes that the Western delegates were largely interested in pre-revolutionary or "pre October" Lenin, while the Chinese delegates sought to focus discussion of "post October" Lenin. This is understandable as the western delegates come from countries that has yet to have a successful worker's revolution, while the Chinese communists were interested in the relatively more difficult task of building and maintaining a socialist society.
