---
tags: [marxism, politics, economics, philosophy]
type: book
creation-date: 2023-10-19
modification-date: 2023-11-14
---

# Historical Nihilism

from [[socialism_with_chinese_characteristics]]

> Historical nihilism is the favored tool of those hostile to the communist project, those who seek to vilify and slander China and its path

Historical nihilism is a denial of proletarian revolution, negating the leadership of the CPC (or any socialist project), and ignoring Marxism or suggesting that Marxism is outdated and that China has abandoned Marxism.

This is one of the many tools utilized by former or current colonizers who; through that colonization, were able to assert dominance over the cultural goals of societies, as well as a dominant narrative that can be peddled to the masses.
