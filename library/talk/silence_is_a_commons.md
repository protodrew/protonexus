---
tags: [technology, labor, theory, culture]
type: talk
creation-date: 2023-10-17
modification-date: 2023-11-14
---
# Silence Is A Commons
## Computers Are Doing to Communication what Fences Did to Pastures and Cars Did to Streets
## Ivan Illich
### [source](https://www.inist.org/library/1982-03-21.Illich.Silence%20is%20a%20Commons.pdf)

[[talks]]

---

Silence is a Commons attempts to connect the increasing noise needed to function in a world of constant communication to the encroaching on the commons of the people by capital. In the same way the pastures were enclosed to make way for large commercial farmers and the roads expanded to remove the public street from the poor, the idea of a "Computer-Managed Society" seemed like a scary idea at the time.

In *>current year<*, this is a very grim prophecy. Computer Managed society has fully engulfed modern imperial capitalism, and the consequences are fully realized. The ever-increasing amounts of noise in our society has become so pervasive and demoralizing that it is negatively affecting our lives in enumerable ways.