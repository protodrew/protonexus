---
tags:
  - " philosophy "
  - " psychology "
  - " culture "
type: paper
creation-date: 2023-10-17
modification-date: 2023-11-14
---

# Things Could Be Better

## Adam Mastroianni

### [source](https://experimentalhistory.substack.com/p/things-could-be-better)

---

Adam Mastroianni outlines some experiments that he conducted to try and figure out why some things seem good and other things seem bad. The initial hypothesis was that out brains work comparatively, where saying something is bad is really saying its worse than something else, but that something else may or may not exist or even be feasible.

However, through a series of experiments, he came to an interesting conclusion.

> When people imagine how things could be different, they almost always imagine how things could be better

### Methodology

They started by asking 91 people to list things that they regularly interact, removed things that not everyone would have, and added everything else that had 6 or more votes to a list.

They then reran the study with a variety of different methodologies and framings (if you want the full breakdown you can read the article), and found overwhelming evidence that humans almost always imagine ways things could be better even though it is more mentally involved.

### Reasoning

There isn't a super clear reason; so the author falls back to a natural selection explanation, but is clearly not satisfied with that as a full explanation.

### Extrapolations

This could provide insight into why humans have a hard time being satisfied with the way things are, aka the hedonic treadmill. If we are always imagining the ways things could be better, it could be a major contributing factor to the constant desire for better.

#### Personal Feelings

This is a very refreshing thing to hear, and reinforces my personal belief that human nature (if it exists at all), is far more positive than it is negative. When I see studies that do that, I always think about how I would react if the results were the opposite. I would probably interpret them as contingent on our society that encourages greed and media that thrives off of fear. However, I think that my interpretation of this is not a warping of reality, and it remains a new source of optimism about how that energy can be harnessed as a force for good. (but there I go thinking about how things can be better)
