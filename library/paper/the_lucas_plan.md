---
creation-date: 2023-10-17
modification-date: 2023-11-14
tags: [labor,economics,politics]
type: paper
---
# The Lucas Plan
## [source](https://archive.org/details/the-lucas-plan)
---

The Lucas Plan was an organizational structure proposed by Lucas Aerospace in 1976. Workers were facing large quantities of layoffs, and argued their right to socially useful production rather than redundancy.

Although the plan was rejected by management and the government, the plan grew beyond Lucas Aerospace and became indicative of a larger movement to commit to innovation over profit. It consisted of over 150 designs for alternative products complete with training programmes, market analysis, and plans to organize the workforce into teams that combine the designers and the shop floor workers.