---
type:
tags: [tonalism]
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Arrangement in Black and Brown: The Fur Jacket
## Whistler

![[../../images/arrangement_in_black_and_brown.png|250]]

worked in monochromes in a time where there was a lot of focus on color, made his own frames, inspired by japanese artists