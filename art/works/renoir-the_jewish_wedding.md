---
type:
tags: [impressionism]
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# The Jewish Wedding
## Renoir

![[../../images/the_jewish_wedding.png]]

proto impressionist working on theory of vibrance of colors, each brush stroke is same length vs ephermeral Italian styles (sargent)