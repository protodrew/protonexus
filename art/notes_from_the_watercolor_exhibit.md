---
type:
tags: 
creation-date: 2023-08-28
modification-date: 2023-11-14
---
# The Paintings that I Had Things to Say about in the Worcester Art Museum's Watercolors Unboxed Exhibit

**big takeaways**

The thing I think makes this exhibit stand out from the other exhibits we have on display is that it is the only one that provides details on the *paper* that the pieces are on. I think that the increased tangibility makes analyzing the pieces more interesting, because you can see how the various mediums interacted with the papers. Things like the sponging and scraping grow in complexity when you start to see how even the use of waxy crayons causes other papers to nearly buckle.

It really is an exhibit about what we don't have as much as it is what we do. The lack of still lives and abstract watercolors (in addition to the lack of diverse collecting in general) are palpable. That isn't to take away from the exhibit, which I feel like has a lot of really cool narratives running throughout. It just shows how much of the issues that exhibits have are inherited from more biased predecessors.
## Muddy Alligators

Sargent is a funny dude because he had a really personal relationship with our (and several other new england museums) art curators. In addition to brokering the sell of the enormous portrait on the second floor, he would also return from trips where he did watercolors to sell directly to museums. He apparently offered us first pick several times, only for us to show up to find they were already sold. After a few rounds of this we finally were offered first pick for real, and that's where muddy alligators comes from.

I like that he claims alligators to be "uninteresting" in one breath, and yet contacted us to change the name (we called the painting alligators since it came to us untitled) to "muddy alligators" to explain the color difference.

My favorite visual element is how sparse the background is, which places the focus squarely on the slippery anatomy of the alligators, without making the piece look unfinished.

## Papuan Woman

Once again most of what I have to say here is about the artist: Emil Nolde. He is a fascinating example of conflicting politics playing out within and in relation to him. He goes on a trip to New Guinea, hired as an ethnographic artist to paint the flora and fauna. During this trip he paints the papuan woman (unique amongst his portraits for being one of the few of a woman and fewer that were profile), and becomes an anti colonialist activist; writing about his concern that European powers will dillute or eradicate the cultures of indigenous soccieties.

However he also became a supporter of the Nazi party until at least the mid 1930s, which was ironic given that a significant portion of "Die Ausstellung 'Entartete Kunst'": the degenerate art exhibit that the nazis showcased for public derision. Nolde himself was barred from painting or showing any of his art, and so his time was spent creating a lot of "unpaintings" that were only seen after WWII.

I'm really conflicted about this piece, it's a rare example of a drawing of a black figure that feels like it was made with no negative lens, yet the creator held both extremely progressive and extremely regressive views that feel incompatible to an outside viewer. The painting itself is really beautiful with it's use of color and lighting with such a sparse amount of detail and nearly no background.

## The Grand Canyon Ones

These were both sketches for woodblock print designs, which shows in their use of a limited palate of vibrant colors (as well as the tape around the paper. The main benefactor of "frontier" art like this at the time were the railroad companies, who were looking to promote the west to travelers and settlers alike.

Apparently they would take artists out to the Grand Canyon and blindfold them for the final leg of the trip, so that the beauty and wonder would hopefully be communicated even more dramatically.

## Old Friends

This is a great example of the physicality present in the whole exhibit. The thick (possibly homemade) paper allows for so much scraping that the trunk physically stands out from the paper. It's a really pretty piece that I just find myself staring at a lot because it invokes so much.

