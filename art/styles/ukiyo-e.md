---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Ukiyo-E

*japanese wood block printing*

## [Examples](https://www.are.na/kim-hart/ukiyo-e)
---

Ukiyo-E was a standardized style of wood block printing that utilized durable cherry wood blocks carved to create an image.

In the case of color prints, each color would have its own separate block, and glue was mixed with the pigments to assist in adherence or prevent bleeding. Artists would also thin their pigments so they would bleed slightly creating gradiation with surrounding colors.

Both the dimensions and number of pigments were regulated, and publishers would have to send the final product to censors before they could be produced at scale.

One way to tell Meiji period prints apart from Edo period prints was the use of bold red and deep blue pigments. These pigments were chemical based, and not produced in Japan. While they sacrificed durability for vibrance, they became iconic with use in prints like The Great Wave off Kanagawa by Hokusai

Hosho paper, made of Mulberry tree bark fibres, was commonly used for the final printing. It was chosen for its absorbancy while also being durable enough to withstand the process.

Each print was typically made by a team of a carver, designer, printer, and publisher. Once travel and production restrictions loosened jn the 1900s many printmakers began doing the entire process themselves.