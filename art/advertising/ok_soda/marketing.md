---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-12-26
---
# Marketing of [[ok_soda|Ok Soda]]

> "People who are 19 years old are very accustomed to having been manipulated and knowing that they're manipulated,"
> — *Sergio Zyman*

![[../../../images/ok_ad.webm]]

The Marketing of Ok Soda relied on a guerilla media approach that involved spreading rumors, writing a near-meaningless "manifesto", and actively disparaging the product. The project was directed by Charlotte Moore, who also was a co creative director for windows 95¹.

![[../../../images/ok_manifesto.png|400]]

Below is an excerpt from a 1994 episode of [All Things Considered](https://web.archive.org/web/20041016183939/http://home.pacifier.com/~ntierney/npr.htm) on NPR

ANNOUNCER: OK soda does not subscribe to any religion or endorse any political party or do anything other than feel OK. Folks, there is no real secret to feeling OK. Attributed to OK soda, 1998.

> ADAMS: That is an OK soda telephone message. Today, we talk with Tom Pirko, a marketing consultant to a number of major soft drink companies. He's done work both for Coke and for Pepsi. Pirko attended an informal tasting of OK soda.
>
> TOM PIRKO, Marketing Consultant: It's suitably wacky. It tastes a little bit like going to a fountain and mixing a little bit of Coke with a little root beer and Dr. Pepper, and maybe throwing in some orange.
>
> ADAMS: Coke, root beer, Dr. Pepper and some orange.
>
> Mr. PIRKO: Yes, a wonderful combination. You would certainly be impressed by the fact that it isit's an out there sort of drink. But once again, the audience that this product is aimed for is an audience is accustomed to sort of being smashed on the side of the head, so you have to have a flavor that is sort of out there.
>
> ADAMS: You mean in the-in the pit at the slam dance concerts, you're talking about.
>
> Mr. PIRKO: Oh sure, right. \[unintelligible\]
>
> ADAMS: So, it has a bold taste, you're saying.
>
> Mr. PIRKO: I'm saying that you wouldn'tthis is not a drink for the timid. The whole-the whole premise of the drink is just sort of to rock the taste buds, I think, so this drink really steps forward and does affect you.

A lot of this weird marketing was due to a desire to market to the "notoriously difficult" market of gen-xer's by bluntly explaining what it's intentions were and hoping that the cynical teens of the era would recognize and appreciate this act.

see also [[tidbits]]

1. [PORTFOLIO 4](https://web.archive.org/web/20140104224505/http://www.charlottemoore.it/charlottemoore.it/PORTFOLIO_4.html)