---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# [[ok_soda|OK Soda]] Postmortem

The soda failed, attributed to the gen x's difficulty to market to

I actually dispute this claim, I think it didn't have anything to do with the market being difficult to corner, but the fact that the market was still grappling with a generation that grew up with nearly unregulated advertising as a result of Reagan's policies in the 80s. The market had bred a generation that thrives on excess and those who were disillusioned with that weren't likely to pick up a new product due to advertising that encouraged that disillusionment from a large company.

> So convincing were the early stereotypes that three years ago, Coca-Cola, targeting teens and Gen Xers, test-marketed a new drink called OK soda. The gray cans featured grim designs, including one of a doleful youth slumped outside two idle factories. Slogans on the cans read, "Don't be fooled into thinking there has to be a reason for everything" and "What's the point of OK soda? Well, what's the point of anything?" The nine-city campaign fizzled. And the company that a quarter-century ago had celebrated the baby boom with the jingle, "I'd like to teach the world to sing," killed the product. Meanwhile, a grunge-themed Subaru campaign that told viewers its cars were "like punk rock" fell flat, and Converse was surprised to find that Gen Xers were put off by a spot showing an All Star-shod youth spray-painting his name on a building.

Read more: [Great Xpectations of So-Called Slackers](http://content.time.com/time/magazine/article/0,9171,986481-1,00.html)

Many companies found success by using elements of postmodernism with advertising. Surreal images can help consumers disassociate the product from a corporation, and projecting your image onto the product. OK soda built it's brand around disillusion and marketed exclusively toward the people who wouldn't want that kind of image projected at them from the CocaCola Corporation™©®