---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Can Design

from [[ok_soda]]

The Cans had illustrations from Charlie Burns, Daniel Clowes, and Calef Brown. They followed a two-tone neo-noir look that is rarely used in package design, and had slogans that seemed out of place or disparaged the product.

prize cans were randomly included in vending machines which the top could be peeled off (like a soup can) to reveal some free merchandise and two quarters to buy another can.

![[ok_soda_cans.webp]]

![[ok_soda1.webp]]

![[ok_soda2.webp]]

![[ok_soda3.webp]]

![[ok_soda4.webp]]
