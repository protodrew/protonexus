---
type:
tags: 
course: main
description: '*This recipe was a freeform exercise loosely expired by my prev sweet potato stir fry.*'
serves: '4'
creation-date: 2023-07-17
modification-date: 2023-10-23
---
# sweet_potato_stew

## Ingredients
- 3 tbsp lime juice
- 5 cloves garlic
- 2 tsp white pepper
- 3 large sweet potato
- 6 cups water
- 3 tbsp harissa
- 1 tbsp onion powder
- some, 5 tsp ground black pepper
- 1 tbsp warm water
- 0.25 cups sweet potato cooking water
- 1 tsp garlic powder
- 0.25 cups tomato puree
- 1 tbsp, 1 tsp salt
- 1 cup greek yogurt
- 4 tbsp sesame oil
- 2 tbsp pudina marsala
- 4 tbsp curry powder
- 1 bunch parsely

## Steps
 1. slice sweet potato into half-3/4 inch rounds
	[sweet potato: 3 large]
 2. place potatoes in a large saucepan salt water and boil for a few minutes, turn off stove and leave until needed
	[salt: 1 tbsp; water: 6 cups]
 3. slice parsely, and finely chop garlic, place in a pan with butter{1%stick}
	[garlic: 5 cloves; parsely: 1 bunch]
 4. heat a large skillet with sesame oil until shimmering, then add the green onion and pepper until fragrant
	[sesame oil: 4 tbsp]
 5. add harissa, curry powder, tomato puree, garlic powder, white pepper, onion powder, cayenne{1/4%tsp}, smoked paprika{1%tsp}, pudina marsala, lime juice, & ground black pepper
	[curry powder: 4 tbsp; garlic powder: 1 tsp; ground black pepper: 5 tsp; harissa: 3 tbsp; lime juice:
	2 tbsp; onion powder: 1 tbsp; pudina marsala: 2 tbsp; tomato puree: 0.25 cups; white pepper: 2 tsp]
 6. strain sweet potato and add to pan with the sauce alonside sweet potato cooking water, cover and leave on medium-low for 20 minutes, stirring occasionally
	[sweet potato cooking water: 0.25 cups]
 7. make yogurt sauce with greek yogurt, lime juice, warm water, salt, ground black pepper to taste.
	[greek yogurt: 1 cup; ground black pepper: some; lime juice: 1 tbsp; salt: 1 tsp; warm water: 1 tbsp]
 8. serve with turmeric rice and yogurt sauce
