---
type:
tags: 
serves: '4'
source: https://ctrl-c.club/~protodrew/pages/notes/recipes/coconut_curry_grain_bowl.html
description: '*Improved this recipe loosely based on bon appetit''s coconut creamed
  corn*'
course: main
creation-date: 2023-08-13
modification-date: 2023-11-14
---
# Coconut Curry Grain Bowl
## Ingredients
- 4 cloves garlic
- 1 green chili
- 2 cans kidney beans
- 1 scallion
- 2 tbsp curry powder
- 14-15 oz cans coconut milk
- some, 1 cup farro
- 2 scallions
- some, 0.25 cups oil
- 4 dried chiles
- 0.25 bunches cilantro
- some, 4 ears corn
- 3 inches ginger
- 0.5 jars thai red curry paste

## Steps
 1. cook farro & cut corn kernels off of cob, set both aside.
	[corn: 4 ears; farro: 1 cup]
 2. heat oil in a skillet on medium heat, add dried chiles
	[dried chiles: 4; oil: 0.25 cups]
 3. slice garlic & scallion and add to heat. When brown strain oil into container
	[garlic: 2 cloves; scallion: 1]
 4. combine crispy aromatics with thinly sliced scallions, chopped green chili, thinly sliced
	garlic, and ginger cut into matchsticks.
	[garlic: 2 cloves; ginger: 3 inches; green chili: 1; scallions: 2]
 5. add oil to skillet on medium-high, add all aromatics and cook until fragrant. Add drained and
	rinsed kidney beans, corn, and cook for 5-10 minutes (corn should get golden brown).
	[corn: some; kidney beans: 2 cans; oil: some]
 6. add farro and cook until crispy. Add thai red curry paste, coconut milk, curry powder, and
	combine until cohesive.
	[coconut milk: 14-15 oz cans; curry powder: 2 tbsp; farro: some; thai red curry paste: 0.5 jars]
 7. serve with chopped cilantro
	[cilantro: 0.25 bunches]