---
type:
tags: 
course: main
description: '*This is based on the sweet potato stir fry by Swasthi, check out their recipe to the right*'
serves: '4'
source: https://www.indianhealthyrecipes.com/sweet-potato-stir-fry-chilakada-dumpa-fry/
creation-date: 2023-07-02
modification-date: 2023-10-23
---
# sweet_potato_stir_fry

## Ingredients
- 4 tsp pudina masala
- 4 cloves garlic
- 2 tsp sumac
- 4 tbsp butter
- 2 tsp ginger
- 600 g sweet potato
- 1 tbsp sweet curry powder (or garam marsala)
- 0.25 cups cilantro
- 1 green pepper
- 0.5 tsp cayenne pepper
- 1 tbsp, 1 tsp salt
- 4 tbsp sesame oil
- 6 cups water
- 4 green onion
- 2 tsp black pepper

## Cookware
- medium saucepan
- medium skillet

## Steps
 1. slice sweet potato into quarter moons (or half moons if the potato is narrow)
	[sweet potato: 600 g]
 2. place potatoes in a medium saucepan with salt water and warm to a bare simmer, turn off stove and leave until needed
	[salt: 1 tsp; water: 6 cups]
 3. slice cilantro, garlic, and green onion. mince green pepper, and ginger
	[cilantro: 0.25 cups; garlic: 4 cloves; ginger: 2 tsp; green onion: 4; green pepper: 1]
 4. heat a pan with sesame oil until shimmering, then add the green onion and pepper until fragrant
	[sesame oil: 4 tbsp]
 5. add ginger, garlic, salt, pudina masala, sumac, black pepper, cayenne pepper, sweet curry powder
	(or garam marsala) and sauteé for 1 minute
	[black pepper: 2 tsp; cayenne pepper: 0.5 tsp; pudina masala: 4 tsp; salt: 1 tbsp; sumac: 2 tsp;
	sweet curry powder (or garam marsala): 1 tbsp]
 6. strain sweet potato and add to pan with the butter, cover and leave on medium-low for 15-20 minutes, stirring occasionally
	[butter: 4 tbsp]
 7. when a sweet potato gives to a fork press, add the cilantro and leave until ready to serve