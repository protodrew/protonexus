---
tags: [side]
type: recipe
description: '*Coconut rice has kind of been my white whale for a while, dialing in the sweetness and coconut flavor to be perfect for my preferences. What follows is a nice balance with a hint of sweetness and a nutty flavor*'
serves: 4
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Coconut Rice

*Coconut rice has kind of been my white whale for a while, dialing in the sweetness and coconut flavor to be perfect for my preferences. What follows is a nice balance with a hint of sweetness and a nutty flavor*

## Ingredients

| Name         | Quantity | Unit    | Notes                |
| ------------ | -------- | ------- | -------------------- |
| Coconut Oil  | 1        | tbsp    |                      |
| Coconut Milk | 1 can    | 13.5 oz | shake before opening |
| Warm Water   | ~6       | oz      |                      |
| Basmati Rice | 1 1/2    | cup     |                      |
| salt         | 1        | tsp     |                      |
| sugar        | 2        | tsp     |                      |
|              |          |         |                      |

## Directions

Melt the coconut oil in a small saucepan on medium-high heat. Once melted and shimmering, add the rice and toast.

Once some of the grains appear lightly golden brown, add in the coconut milk and warm water and stir until combined. Cover and bring to a boil.

Once boiling, reduce the heat to medium-low and add the salt and sugar. Cook for 12-15 min, and turn off the heat. The rice can sit on the burner covered until ready to serve.

Fluff with a fork or spatula before serving and enjoying!
