---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# Structure
Each character will have 2 things they want to talk about and 2 things they don't, here is a table

|          | bananulon                   | sql                   | scunge              |
| -------- | --------------------------- | --------------------- | ------------------- |
| LIKES    | gambling, conversations     | computers, flirting   | cooking, philosophy |
| DISLIKES | objectification, small talk | questioning, othering | appearance, past    |

Upon the timer completing, the game will log your compatibility score, which will determine if they are a second date candidate, then you go to the next date

## Date 2

Each date 2 takes place at a unique location and is a simple score based game

| person    | game                                                 |
| --------- | ---------------------------------------------------- |
| bananulon | blackjack (simplified)                               |
| sql       | asteroids with hacking theme                         |
| scunge    | cooking at it's place while talking about philosophy |
