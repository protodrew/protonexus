---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Art Pipeline

from [[godot_wild_jam]]

Once I assembled a piece of art, I add red, blue and green noise (around 30-40% depending on the image), and decrease the brightness while increasing the contrast slightly. The final step is a slight pixellation, with a pixel size of 5-6, or 3-4 for smaller images.