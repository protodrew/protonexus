---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# godot_wild_jam
## Theme

![[Pasted image 20220812150433.png]]

After doing the brain dump and fleshing out my ideas, I think I've come to the conclusion that the dating game idea is the one that I think will be the most fun for me to make and for people to play. The plan for the game is in the scope document (listed below), and the kanban is populated with everything I think I will need

## Schedule

| Sat                     | Sun                           | Mon                       | Tues                         | Wed                     | Thu   | Fri             | Sat             | Sun      |
| ----------------------- | ----------------------------- | ------------------------- | ---------------------------- | ----------------------- | ----- | --------------- | --------------- | -------- |
| menuing                 | whitebox structure            | finish character dialogue | finish art hunt              | Game structure complete | tunes | minigame polish | post processing | bugfixes |
| integration w/ dialogic | write some character dialogue | whitebox minigames        | process art                  |                         | menus | ui polish       | bugfixes        | polish and publish         |
|                         |                               | begin art hunt            | integrate dialogue with game |                         |       |                 |                 |          |