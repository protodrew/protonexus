---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-12-26
---
# Narrator Intro

It's been a cold few weeks on the moon, you heard the local bar Pizzamoré is doing a little speed dating night. Maybe you can meet someone special here.

*fade into bar*

you see four people with nametags, it's free mingle, who do you want to talk to

# Dates

## Lady Angio

Oh hey sweetie, thanks for sitting down, tell me sugar, why are you here.

> looking for love + 10

Ooh? Trying to find that special someone. Well, I'm afraid I might not be on the market in that way, but I love to have a good time with people.

> Just trying to meet some folks + 15

Ooh, another lover of the game. I think it's so fun to meet new people and just peel away their layers

*she looks at me like it's my time to ask a question*

> What's a pretty thing like you doing all the way out here? -10

Well I'm not much of a "thing" but I appreciate the gesture. My home system of Musa is such a drag, so I came here to try and get some big wins at the legendary casinos

> What is your greatest fear? -10

Oh, geez starting off pretty heavy… uhm I guess just being another face in the crowd. That or losing big in the casino

> Have you ever hit the tables over here? + 10

As a matter of fact I have, not up or down much at the moment, but it's good to hear there's another lover of chance here

Do you like to play the local casinos?

> I do but I'm not much of a winner + 15

Oh none of us are sweetie. The house always wins in the end around there, but we keep coming back don't we

> not really much of a gambler, but I love the atmosphere + 10

Honestly dear I feel like I agree with that more and more these days, the new technologists have all the games solved at this point. Maybe if this goes well you can accompany me for a bit of extra luck

> That sounds like fun +5
> maybe… +0

> I feel like people are always so uptight during these events, I don't want to see a polished version of someone, I want the real deal +20

Oh my gosh darling you're exactly right. I love coming to these events to meet new people, but I feel like so many people are too scared to be vulnerable.

	> it's like they don't want to be themselves

	> I can't blame them, people can be judgemental

> so do you like movies - 10

… I've watched a lot of movies

I want to know… how do you feel about the drinks at this place?

> they're alright, but lux has the best drinks around +10

Ooh, their drinks are good, but so expensive. A few wins at their tables help ease the cost though, let me get you something here

> they're pretty good, but I haven't had enough yet

let's see if we can fix this *she motions to get you another drink, the bartender hands you a flute glass with some green liquid*

> down it quick -10

*you try to take it like a shot, and immediately the back of your throat burns*

Oh I'm sorry, I should have warned you that was more of a sipping drink

	> laugh it off +15 (go to timer dings)

	> look flustered (date ends)

> take a deep sip

*The drink is strong as hell, but very complex, it has a piney aftertaste that reminds you of home*

> thank you, what was that? + 10

It was a martian absynthe made from sandwyrmwood. One of SQL's favorite drinks, *she motions to the ominous floating crystal talking to a large glob of slime*

> he can drink? - 10

*They* are never really seen drinking it, but they definitely enjoy ordering them.

> Very sophisticated + 10

I would agree, but it's worth giving new friends a taste of the finer things in this world.

*a timer dings*

Well it seems that's our time for now. See you later sugar

## SQL

Oh, hey what's up, how's it going?

> Pretty good Want a drink? +10

always, why don't you pick something out for me

> martian absynthe + 25
> lunar lander IPA - 5
> gin and hypertonic + 0
> lunar absynthe + 15

Positive response : Ooh an absynthe, my favorite? How did you know

> lucky guess + 5

Well looks like luck is in your favor tonight

> Was talking to Angio, and she mentioned you like them + 10

That's really considerate of you, Angio's great

neutral / negative response: hey thanks

> So what are some of your interests? + 10

Oh, mostly computers. Being a silicon-based lifeform, I love how your species was able to create complexity out of inanimate silicon. I also like gaming here at the barcade.

> why is that so cool for you? You seem far more advanced than any computer I've worked with

That may be true, but to you, it might be like if someone was able to create a simulated brain by shooting electricity through proteins or carbon.

> So where are you from? - 10

Uhh, I don't really like to talk about it

> that's all good. What do you like to do here?

**same response as interests**

> I love your look, What's a cute thing like you doing out here? + 20

*You've never seen a prism blush before, but you're pretty sure they are*

Aw jeez… I'm just here because I like hanging out with new people, the amoré lets me be myself and learn about Terra from a safe distance… Plus I have to keep my lightgun high score

> oh you like games?

Oh, yeah I love them, racking up a new high score is one of my favorite things to do

> I love games too! +10

Really? That's super awesome, maybe we can play together some time

> I used to be a big gamer, hard to find time these days + 20

yeah, I know what you mean. It's really easy to get lost in the big picture, but I always tell people it's worth slowing down to take time for things we enjoy, maybe we can play together sometime

> Why's your name SQL_Injection

I noticed a lot of terrans have names that are somewhat intimidating like "Hunter", I figured something like that but for computers would suit me

> I think it suites you very well + 10

Thank you!

> interesting… - 20

…

> So what is your favorite computer around here?

Probably the Navigation Computers they use at the spacedock. I've been able to hang out there once before, and seeing the high pressure environment was really invigorating.

> I just want to say that you're really fun to talk to

*They look surprised*, Oh wow thank you, I've had a really good time as well! Maybe you can come by my place soon

end message

## The Scunge

*as you walk up the slime mass doesn't acknowledge you, at least… you think it doesn't*

> tap it on the shoulder

You feel a sticky sensation envelop your finger and both of you are startled. You hear "AHH", but as if it was coming from your own thoughts.

> clear your throat + 10

It turns out The Scunge was not paying attention. You hear "AHH", but as if it was coming from your own thoughts.

> say hello + 10

The Scunge pivots 180° towards you, you realized this side is obviously the front, as the eye sockets from the humanoid skeleton inside of it are now staring at you. You hear the word "Oh, hello" coming from your own thoughts.

> Woah are you telapathic?

Yes I am, but I can only broadcast, not read your mind.

> Hello, nice to meet you

It's nice to meet you as well

Sorry I should Introduce myself I am *you hear a collection of soft gurgles*, but most people call us "The Scunge"

> Us?

Yes, us. We are a collection of millions of individual slime mold organisms that were able to collectivize our efforts to achieve higher consciousness.

> Oh wow, what's that like? - 10

My species is able to use bioelectrical signals to rapidly communicate desires to each other, once a majority decision has been made we all act on it as one.

> Do you find it hard to relate to single-minded entities here?

On the contrary we find it difficult to relate to our home species, our love of intellectual pursuits led us to Terra to study its people and learn philosophy.

> Hey… more to party with - 10

I suppose that is technically true, though we function very similarly to a single-minded organism.

Do you have a love for philosphical discussions?

> Not really, I am more focused in our current circumstances

We find it easy to slip into that ourselves, though remember without a framework of understanding with which you can look at the world, you fall at risk of being shaped by it.

> I guess that's a good point

> Fair enough

> Yes, thinking in the abstract helps me live in the moment

We found similar solace in interpreting the world through our interpretation and the interpretations of others.

We find philosophy is a solace that allows us to avoid taking the machinations of our existence for granted.

Through understanding ourselves, we can better understand the world and find purpose through it.

> Finding purpose is a constant struggle for me

We understand that. Our collective consciousness is effectively immortal, though we are very young. Looking into infinity can be terrifying if there is nothing to create, complete, or leave behind.

> Is that what brought you to the amoré?

In a way. The terrans did not respect or understand us, and we have found the moon to be far more welcoming to those who live outside of the norm.

> What are you looking for here

First and foremost, we are looking for people who accept us. Our appearance and way of life is offputting to a lot of people, and because of that it is hard to find a place where we are not feared.

Secondly, we are looking for a companion who wants to learn with us and share our journey across the moon. We currently live in the tar pits and have very little in the way of friendships or regular interactions.

> What's with the Skeleton

It was a dear friend of mine on terra. When he was on the brink of death, he asked me to absorb him, so that his memories can live on.

> I would love to spend more time with you

If we enjoy each other's presence, one day you can come to the tar pits and I will tell you the tale of the man who's skeleton is inside of me

> That feels kind of creepy

Each person has their own relationship with mortality, it is not our place or yours to judge.

> Do you want a drink?

Alcohol is extremely poisonous to us, however the lovely bartender here gives me a large jug of simple syrup that I can pour into myself for glucose.

> What do you value most

Memories. Millions of us are created and die every day, only through communication with each other can our experiences be passed on. Knowledge is the only eternal thing we can leave behind.

# Date 2
## The Scunge

Welcome, I'm so glad you could come. As a thank you and a promise to my friend, I will now tell you his tale.

… I made it to the antarctic after a long and arduous voyage. The hope is to establish accurate samples of permafrost across the surface to study the development of the planet

… Day 3, on the ice, I set up a simple shelter and have built up some solar panels to get electricity for some much-needed comfort

… Day 7, on the ice, I severely underestimated the brutal cold here, even in the hemespherial summer, it is still brutal and unrelenting

I've had some luck collecting samples, but have been unable to make it far past the comfort of my tent. I hope I won't have to keep packing up and moving camp, that would be a huge pain in the ass.

… Day 9, I've found something in the ice, it is green in appearance and seems to resemble some sort of lichen

This is even more exciting than I could have ever hoped. Could this indicate that there was the beginning of primary succession in the antarctic

… Day 10, After bringing a sample back to camp I attempted to slowly melt the frost from it by putting it on my solar panel.

What I originally thought was a lichen turns out to actually be a slime mold, whats more is it seems to still be alive. Preserved within the frost.

Some of the slime mold seeped into the cracks of my solar panel, which has been causing it to have a less reliable connection, I hope it will dry soon.

… Day 11, the power went out… shit

… Day 12, I've never felt cold like this before, but I can barely contain my excitement. I have heard messages from the slime mold.

Whoever hears this may think I am insane, but I assure you I am of sound mind, and relatively sound body. The mold told me it has been frozen in the ice for an unknown amount of time

I will return to the site of it tomorrow, and use my remaining flare to excavate the remaining mold, so that this being can be united and tell me it's tale

… hopefully the power will be back

… Day 14, I am back in the tent and the power is still out. I have almost every layer I own on, as well as 2 thermal blankets.

Yet again, my excitement far exceeds my discomfort.

The mold and I have been speaking for the past 2 days and I have learned so much. We spoke of philosophy, religion, war, and art.

I told them about my desire to bring new discoveries to the world, they told me of their desire to experience everything they can.

They have this extraordinary ability to extract the memories from those that they absorb. I am fascinated to learn more from them

… Day 17, Things are looking dire, the power has not returned, and the cold is seeping in

I told the slime of my struggles, and they feel remorse, though they could not have known. I assured that they shouldn't feel remorse for actions that were my stupidity

… Day 19, the power has not returned and I fear that I may pass away soon. I have told the slime to absorb me when I can no longer respond to them.

If you are hearing this, I have passed away. Do not feel sad for me, my memories will live on inside the most extraordinary creature I have ever discovered.

-----

END DOCUMENT