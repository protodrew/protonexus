---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Intergalactic Singles Night
## A Lunar Speed Dating Event

from [[godot_wild_jam]]

Intergalactic Singles Night will be a visual novel game with a high score minigame at the end to encourage replays.

# Meet the Singles

![[Pasted image 20220812191457.png|200]]

## Lady Angio (formerly bananulon)

from the planet fruitopia

*The best part of meeting new people is peeling away their nervousness*

Lady bananulon is a serial speed dater, and seems to do it more for the love of the game than anything else. She's a spritely 100 cycles old, and enjoys spending her time at the blackjack table, hoping for that big win

![[Pasted image 20220812192006.png|300]]

## Sql Injection

from [REDACTED]

*I hope we can interface, I am a good listener and an even better lover*

SQl is a bit of an anomaly, literally. Nobody knows where they came from, including them. They're a lover of computers; being a crystalline being themselves, and for that the owner of the bar special ordered a hacking arcade game to keep them out of trouble. They seem to always order an absinthe, despite never being seen drinking

![[Pasted image 20220812192425.png|300]]

## The Scunge

from Antarcita, Terra

*I acquired this skeleton through ethical means*

The scunge is a large mass of semisolid ooze, its origins are unknown, because it doesn't like to talk about itself too much. It loves to come to the bar for a judgement-free space where it can be itself, and always orders a soda water with 1 quart of simple syrup, poured directly on it's head.

![[Pasted image 20220812192846.png|300]]

## ~~Brian~~ CUT FROM FINAL GAME

from witchita, Terra

*I'm just here to make friends*

Brian looks incredibly unassuming, and uses this to his advantage in the field as an intergalactic hitman. He's just in town to lie low after completing a job about 2 parsecs away, but is always looking for a crewmate, partner in crime, or just somebody to talk to.