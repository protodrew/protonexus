---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# Moving from GNOME to Sway

I'm not going to start this off with some indictment of GNOME or talk about how it's a shit desktop environment. It isn't. I've been using GNOME for 4 years and the whole time for my use case it's been perfect. I use the 13in [framework](https://frame.work) laptop with wayland to improve stuttering, I started with Pop! OS and switched to openSUSE a few months back. I loved the pop-shell tiling extension, so kept using it (as it is available in the tumbleweed package manager) until it was broken in gnome 44. I found a workaround, and then it was re-broken in 44.1, so I figured it was time to look for alternatives. I got pop-shell running to a decent degree (swapping over gradually), but am not confident it will continue to be updated, as System76 is moving to their rust-based [COSMIC](https://blog.system76.com/post/november-at-system76-products-promos--cosmic-de) Desktop environment. So with that in mind, I cracked my knuckles and installed sway.

OpenSUSE actually has its own config for sway called [openSUSEway](https://en.opensuse.org/Portal:openSUSEway) that made the switch much easier. I would recommend trying it in a VM if you want to see what a full sway setup would look like with essentially default configs.

After installing (for instructions for openSUSE, see the [wiki](https://en.opensuse.org/Sway)), I booted into sway and… wasn't sure how to do much. By sheer dumb luck I hit the terminal key; and so determined to do as much as possible through terminal. I used w3m to open the wiki page, which had basic information on configuration. I used that to get Firefox open and started looking for the components people liked.

First impressions, I was really surprised at how seamless getting started and doing basic configuration was. Going from using gnome-tweaks, gnome-extensions, and gnome-settings as separate applications to just copying the default sway config to ~/.config/sway and being able to figure it out from the comments was kind of refreshing. If you are cool with whatever the default config is, it's honestly quite nice out of the box. However, if you (like I) are a little freak that needs everything to conform to a theme, then it's time for…

## Configuration

(if you just want to see my dotfiles go [here](https://codeberg.org/protodrew/dotfiles))

### Applications

#### [Foot - Terminal](https://codeberg.org/dnkl/foot)

openSUSEway uses a great terminal called [alacritty](https://alacritty.org/) by default, but I ended up using `foot` because it has far less functionality in favor of a bit more performance. I'm not a power user when it comes to terminal emulators, and use things like tmux for paneling internally, so the simplest option is the best for me.

#### [Fuzzle - Launcher](https://github.com/Psykopear/fuzzle)

Fuzzle is the launcher that I chose. Pressing super+enter provides a list of the most recent apps I've used, with an active search bar for searching. I don't really use desktop icons or fancy hotkeys most of the time (notable exceptions being super+T for terminal and super+F for my file explorer (nautilus)), so the simplicity and ease of theming appealed to me.

#### [Mako - Notifications](https://github.com/emersion/mako)

This was a simple plug and play notification manager, found a nord theme and called it a day.

#### [Waybar - Status Bar](https://github.com/Alexays/Waybar)

Waybar is probably the most complex of the tools to configure, and I grabbed my config from the openSUSEway config and removed a few things, I also changed some colors around in the `style.css`, but I am not going to go into that because there are better resources for that.

#### [Grim - Screenshots](https://github.com/emersion/grim)

Grim is a great little screenshot tool with potential that I have not ever looked into. I just take a capture of all active displays and crop it in the Gnu Image Manipulation Program (wish they had a better name. My config has two hotkeys for screenshotting, super+p and the `print screen` key on my keyboard.

```
bindsym $mod+p exec grim $HOME/pictures/capture-$(date +%Y-%m-%d_%H-%m-%s).png
bindsym Print exec grim $HOME/pictures/capture-$(date +%Y-%m-%d_%H-%m-%s).png
```

I like having all my folder names in lowercase, so I don't use the default `Pictures/ Documents/ Downloads/ `folders, replace it with the case-sensitive folder name because grim will not attempt to create a folder if a folder doesn't exist.

#### [Greetd / GTKgreet - Login Manager](https://sr.ht/~kennylevinsen/greetd/)

Greetd works pretty plug & play with openSUSEway and I haven't looked into theming it. For some reason, would stop using the GTK frontend in favor of a TTY based one, and I couldn't figure out how to fix this for the longest time. The solution I found is to create or copy a file named `sway-config` to `/etc/greetd/` that contains the following

```
exec dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK

input "type:touchpad" {
  tap enabled
}

exec "gtkgreet -l -s /etc/greetd/style.css; swaymsg exit"

bindsym Mod4+shift+e exec swaynag \
	-t warning \
	-m 'What do you want to do?' \
	-b 'Poweroff' 'systemctl poweroff' \
	-b 'Reboot' 'systemctl reboot'

exec /usr/libexec/polkit-gnome-authentication-agent-1

#include /etc/sway/config.d/
```

### Media Keys

One thing you are going to need is an application to manage display brightness and volume. I ended up using [amixer] for volume controls and [light] for brightness. If you don't know the key command names for your media keys, you will want to use a tool like [wev] to figure that out. Here was the final addition to my config file

```
# Modify Brightness with Media Keys
bindsym XF86MonBrightnessUp exec light -A 10
bindsym XF86MonBrightnessDown exec light -U 10

# Modify Volume with Media Keys 
bindsym XF86AudioRaiseVolume exec amixer set Master 10%+
bindsym XF86AudioLowerVolume exec amixer set Master 10%-
bindsym XF86AudioMute exec amixer set Master 0%

# Modify media play state with Media Keys
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous
bindsym XF86AudioPlay exec playerctl play-pause
```

**note on using light**

Light needs to be run as sudo, and I found the easiest way was to just add it to the whitelist of commands that don't need to be run as sudo by running:

```sh
sudo chmod +s /usr/bin/light
```

### Compose Key

The compose key is a really useful tool for creating complex Unicode symbols like the degree symbol "°" or the copyright sign "© "by inputting a shortcut. I like to replace the caps lock functionality with the compose key because I can just hold shift when I need a lot of caps keys. To do that, simply insert the following line to your config:

```
input * xkb_options compose:caps
```

### Display Configs

The config file explains how to do this, but I ran `swaymsg -t get_outputs` so that I could manually force the correct resolutions for each monitor. One thing that impresses me is the seamlessness of having some displays that are not always connected. I work on a laptop that I connect to an EGPU with 2 monitors, and was able to configure them by just adding their configs to my default file, and they're ignored when not needed.

```
output eDP-1 {
    mode 2256x1504@60.0Hz
    scale 1.25
}

output DP-6 {

    mode 1920x1080@164.917Hz
    scale 1
}
output DP-7 {
    mode 1920x1080@164.917Hz
    scale 1
}
```

#### Wallpaper

I have all displays use the same wallpaper by adding the following to my config file:

```
output * bg /home/protodrew/.config/sway/wall.png fill
```

But if you want individual wallpapers, you could add a similar line in the above code block inside each display definition.

#### Misc

I also close my laptop when it's docked into the GPU, and was able to have it disable when the lid is closed and enable when it wasn't. This worked perfectly:

```
bindswitch lid:on output eDP-1 disable
bindswitch lid:off output eDP-1 enable
```

### A Note on GTK Theme Stuff

I have 0 idea how to consistently configure QT themes, and the few cursory searches I tried made me more confused than ever, but to set a GTK theme just put the theme in `~/usr/share/themes` or icon theme in `~/usr/share/icons/` and add the following:

```
set $gnome-schema org.gnome.desktop.interface
exec_always {
gsettings set $gnome-schema gtk-theme "THEME_NAME"
gsettings set $gnome-schema icon-theme "ICON_THEME_NAME"
gsettings set org.gnome.desktop.wm.preferences theme "THEME_NAME"
}
```

### The Scratchpad (aka the Program Bag of holding)

The scratchpad is a great tool that I feel like should be talked about more. pressing super+shift+minus pops the active program out of all displays and into a background queue and can be retrieved by pressing super+minus. The functionality of the latter command is a bit confusing, but pressing super+minus while a scratchpad program is active will send it back to the bag, pressing it once more to pull the next program out if there is one.

This is the most elegant way I've seen of minimizing programs in a tiling window manager and while I haven't tried i3 which this is likely from, I just want to evangelize how much I love this feature. Super helpful

## Conclusion

Honestly that's been my experience with sway throughout, it is simple and once I picked up the basic hotkeys for moving around and manipulating windows, it feels comfortably out of the way: centering whatever I'm working on in my brain. I like working with my computer, and want to spend as little time as possible working 'on' my computer (though I have to fight the urge to tinker constantly), so this is a perfect tool for me.

I would also really recommend checking sway out and if you aren't tied to a particular distro, give the openSUSE Tumbleweed openSUSEway config a try. It automatically installs and configures all of the programs you would reasonably want for a good out of the box experience, and their YAST system management tool is a good replacement for a lot of the things you would normally turn to your desktop environment settings for like changing the time zone.

If you end up trying sway let me know how it goes! I feel like there is a lot of room for expanding on this and I might make a follow up post in the future.