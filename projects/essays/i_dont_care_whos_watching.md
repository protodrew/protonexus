---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# I Don't Care Who's Watching

That's a lie,

I need the numbers

how many clicks and scrolls

how far each person reads

whether they like it, whether they like me

I need the value of my thoughts

of my skills

of myself

said back to me every day

graphed out, averaged, and proven

It is only through this that I can

grow

achieve

succeed

I have to appeal to the onslaughts

stay true to my own thoughts

and gather, collect, and report my success

It is only through the validation of others that I know my work is good

but it is not throught you, rather the royal you, all viewers quickly clicking to get their quick fix before moving on

forgetting

forsaking

It is through catching the algorithm that my ideas can have worth

that I can have worth

that I can succeed.

Every idea must be battle tested, new and unique, and more of the same

Spontenaity, Sincerity, Skill, and Speed must work in harmony

but I can't do it anymore

I need to move away from it

towards community

towards friendship

towards growth

because despite the constant shifts in tastes, styles, and individuals

the algorithm is static, unchanging, and bland.

The people around me have ideas that span the styles and will grow and surpass the ability to be classified

Writing in a place that can't tell me the number of seconds someone spent thinking about me is agony

at first

but then I grow and create, in my down time, in my own place, for myself

And it is through this that I am able to

grow

achieve

and succeed

on my own terms
