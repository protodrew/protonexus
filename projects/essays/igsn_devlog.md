---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Intergalactic Singles Night Devlog

I've been thinking a lot about games recently. I've decided to take a break from college for an indeterminate amount of time, and I have started thinking about what I want to put into the world. It's been far too long since I've completed a project that I'm proud of, so with that in mind as I traveled back to my parents house to visit, I resolved to participate in the Godot Wild Jam.

I have three larger prototypes I've been working on in the back of my mind, but I found it hard to work on them. I'm not sure if it was leftover fatigue from finish school, covid, or anxiety about the future, but nevertheless I was feeling burnt out. The game jam was a great opportunity to put all of that aside and work on a small standalone project, both to further develop my skills with godot, and to prove to myself that I can finish a project.

## Starting Off

Once the theme "Moon" was announced, I immediately hopped into brainstorming and came up with the following ideas

- a fishing game where you catch different fish based on the phases of the moon
- a surfing game where you change the phases of the moon to influence the tides
- a score based alien shooter that takes place on the moon
- a short narrative game about looking at the moon with a friend
- **surreal mspaint / flying Luna clipper style space alien dating sim
- **space croquet (suggestion from my mom)**
- **juicy moon pinball game like that one space cadet pinball game**

The ones in bold are ones I thought deserved some more fleshing out, so I took to a pencil and paper and made some terrible concept sketches. After sketching and taking notes on my thoughts for around an hour, I decided to go on a drive to clear my head, as well as asking my friends which of the ideas they liked better. At the end of the day, it was pretty clear that the dating sim was the idea I could go the farthest with, so I started scoping it out.

The original plan was it would be a game that takes place within a bar on the moon, where you would meet 4 (later cut to 3 for time) singles that you could impress. If you managed to impress them enough, after talking to all 3 you could play a minigame from one of them. This idea developed over time and my kanban board grew.

## The Process

I quickly set up a lot of the foundational structure in Godot, like the game_manager that would handle a lot of global operations and store the compatibility dictionary. This was the first game I was making after learning quite a bit more about godot, so I could fully take advantage of some of the integrated features of the engine I had previously ignored such as typing, UI components, and more advanced use of the [Dialogic](https://dialogic.coppolaemilio.com/) plugin, which powers most of the game.

The first few days were focused on building the aesthetic, writing the game, and testing small slices of the writing within dialogic. I also began work on a shooting gallery minigame, which was originally intended for the 4th character. I decided to implement it instead of my idea for SQL_Injections's hacking minigame, as it wasn't that fleshed out. The shooting minigame itself was inspired by playing Hideo Kojima's Snatcher for the Sega CD, which has been my game of choice for the past week or so.

The majority of the next few days were spent outside godot, working on art (which was mostly made of public domain clipart that I assembled and processed to make crunchier), finding some music, and writing the date dialogue. It was around day 5 that I started to worry I wouldn't have enough time to finish the game at all, but I put that aside and kept tinkering away. I implemented the base logic for the blackjack game in one big, untested burst, which fortunately mostly worked.

## Post Mortum

The final 3 days, I sat at my laptop pretty much all day and worked through as much as I could. I finished the writing on day 7, fleshed out the minigames on day 8, and put the final coat of polish on day 9.

For my post mortum of the game, there's really only 2 areas I wish I could have spent more time. The blackjack minigame was originally designed to be a time attack game similar to the shooting gallery, where the goal was to get as many wins as possible in a short amount of time, but due to time constraints and generally being exhausted with the process, it stands as mostly a tech demo at the moment.

The UI design could have also been a lot better. I still need to spend time with Godot's theme system and learn its inner workings more, though I did have some success working with actual buttons and sliders this time, rather than scratch-made jankier implementations I have used in the past.

## Final Thoughts

Overall, I'm proud of this game. It's got some of my best fiction writing, godot code, and I think I'm beginning to develop a unique style for my art, influenced by "The Flying Luna Clipper", early 00s PC games, early computer software design.

I hope you enjoyed it, and if you haven't played it, you can do so on [itch.io](https://protodrew.itch.io/intergalactic-singles-night)