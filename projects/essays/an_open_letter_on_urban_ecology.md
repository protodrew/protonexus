---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# An Open Letter on Urban Ecology

The time has come for a new way of thinking about green space.

The automobile industry pushed it to the side, and made it an afterthought to fill in all the empty spaces in their massive roads. The public spaces we can enjoy nature in are relegated to areas outside the urban center, while inside the cities we are treated to small patches of manicured itchy invasive grass. Our trees (when we have them) provide little in the way of shade, and simply exist to greenwash the crushing weight of industrial capitalism and the toll it has on the people who live there. It is time to think about our ecology in a new way.

Native plants should no longer be seen as "invasive" or weeds that must be plucked from the earth. It is time to utilize the benefits that our unique climates provide us with. Utilizing plants like clover, moss, and even the little patches of native grass we all call weeds will both cultivate a much richer relationship between flora and fauna, and also allow for the plants we grow in our common spaces to strengthen our local ecology.

The reason we have fallen so far and have almost unanimously decided to worsen our environments is largely due to two factors. The first being the need to pursue traditional aesthetics that were formed in the 50s, and the second being the parasitic nature of fertilizer and seed mega corporations like Monsanto that seek to create fertilizer dependent non self-seeding ecosystems that will create a consistent cashflow. This is detrimental both to the cities who purchase from these companies, and the people who live within these cities. It is time for a dual approach to urban ecology, which allows both local growers to form community gardens, and cities to plant native plants only. This will be more cost-effective (as the city won't need to constantly buy fertilizer), easy to maintain (as the plants are adapted to the city's climate), and will create a strong sense of local identity, as the city will stand out in pictures rather than looking like a carbon copy of every major city.

We need a way for people to plant locally now more than ever. This, however, is where a gap in our collective knowledge is most obvious. There is no one service that anyone can visit to see what plants are native to their area. Obviously there are local sites and papers, but the knowledge is spread out and often incredibly disorganized and incomplete. I believe that the key technique to utilize to maximize functionality and usability is an open-source web frontend, that allows a Wikipedia style system of editing and adding information. Each zip code in the US should have a planting calendar, a table for native plants, and maybe even a list of local CSA's or gardening organizations. Over time a dedicated group of volunteers could populate the site with information and utilize its open-source nature to ensure the data is open and transparent, as well as workable to allow other software-engineers to create tools utilizing this backend. Maybe one day I will start working on it, maybe someone else will get to it before me, either will need your support both in signal-boosting its existence, and contributing your local information.
