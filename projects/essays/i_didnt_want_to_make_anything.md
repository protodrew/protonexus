---
type:
tags: 
creation-date: 2023-08-23
modification-date: 2023-11-14
---
# I Didn't Want to Make Anything
for the first time in a long time

I hated myself for it. I spent months trying to find new hobbies or fields or tools to throw myself at. Things to be bad at and feel bad for being bad at. Projects to get overwhelmed by and get mad at myself for thinking about more than working on. I spent months thinking I was done with these patterns, only to realize that rather than contributing to my burnout, it was a manifestation of it. What I had to realize was that I was also grappling with a fear of not mattering. Not in relation to my friends and family, but to my communities: physical and online. I was fortunate enough to have a vacation with family coming up, and used it as an opportunity to table every incomplete project I had, barring a new website for me and one for my partner.

I took the time to pick up the book "How to do Nothing, *Resisting the Attention Economy*" by Jenny Odell for the 3rd time after not making it past the introduction. This time I started at chapter one, and I've spent most days thinking about it since. I am not sure what I was expecting or when I first heard about it (I think it was from a Public Books 101 podcast episode?), and I was really surprised at how "for me" this book felt. It covered the topic of attention, this idea of interacting with and "using" time, and thoroughly analyzed it through lenses ranging from art to music to Marxism. Stripping so much of our societal pretexts away until there was nothing left but the human desire to experience the world around them.

It was exactly what I needed, and honestly I'm glad I didn't read it before then because I think that that vacation was the most malleable that I would be to these ideas. I soaked it up and followed up on a bunch of the references and sources that she used, and found myself really

Her mention of "deep listening" —a thing that I associated with symphony orchestra as "active listening"— reminded me about why I enjoy music, and the backdrop of the ocean provided a gentle on ramp to truly perceiving the surrounding environment, rather than trying to block it off. While I had been listening and enjoying to a lot of experimental and avant-garde music, I failed to recall the interviews where John Cage would try to recouple the environment around an artistic experience and the art itself:

> "I find that most interesting when one finds something in the environment to look at. If you're in a room and a record is playing, and the window is open and there's some breeze and a curtain is blowing, that's sufficient, it seems to me, to produce a theatrical experience".
*Cage, J., Kirby, M., & Schechner, R. (1965). An Interview with John Cage. The Tulane Drama Review, doi:10.2307/1125231*

I took time to be okay with doing my day job and relaxing. By planning for the future and enjoying the present, I was able to enter a dialogue with myself that felt less hostile, and this dialectic compassion allowed me to re discover a desire to create. Not to have an output, or to become well known or well respected or rich, but to express a tiny fraction of the world I see and the world I envision to the people around me. I needed to remember that I have worth beyond what I have made or have the potential to make. Remembering that in trying to express the richness of the human experience, I needed to reacquaint myself with a slower, more human life and state of mind.

It took a lot of getting used to, and honestly I originally wanted to come at this from a completely different perspective (more of a "finally I want to make things again"). However, I sat with this for a bit on the porch and listened to the cars drive by, and realized that I didn't think they were annoying anymore.