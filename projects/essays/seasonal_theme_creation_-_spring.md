---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Seasonal Theme Creation - Spring 2020

I have utilized this year's spring equinox as a sort of transition truly into 2020. I consider all of the chaos and misery a part of 2019 and I consider the physical transition of seasons a much more impactful transition into a new year than an arbitrary date. <!--more-->

With this in mind, it was time to choose a new theme. Themes are [a system by which you organize your life](https://www.thethemesystem.com/) in a much more concrete way than choosing a set of halfhearted resolutions.

 Personally, the concept of maintaining a rigid structure like that is far too daunting, and 2019 as the "Year of Reallocation" I set was a failure. With this knowledge, I am going to make themes on a seasonal basis. My reasoning for this is twofold:

     1. Human existence is so dynamic that unless you go into a theme with incredibly vague 

        goals (a valid option, but not personally my goal) you will almost certainly have trouble maintaining a consistent path.

     2. The inherent weather changes the season brings will act as a nice coda to a goal and 

        bring new importance to the equinoxes and solstices the year brings.

               With that in mind, my seasonal theme will be <h2>Structure</h2>

Why Structure?

   I have a problem with procrastination. And I also suffer from depression which can often cause my productivity to plummet. Now I am not in favor of productivity for its own sake, but this is an issue that has adversely affected my health both physical and mental. With that in mind, I have chosen structure in an effort to create good habits and routines. However, I do not wish for these to be so rigid that I compromise whimsy or my own health to make a deadline or complete a goal. With that in mind, I believe structure is a good term because I can utilize the framework of structure to reallocate a task to a new day.

What does Structure Mean?

   This has been semi explained in the Why Structure? section, but I will outline my concrete goals as they are right now.

     1. If possible, complete a task that has been given to me the same day or the minimum amount of days 
        needed (AKA don't wait until the last second).
     2. Set yourself awake time on free days and do not snooze the alarm.
     3. Set goals at the beginning of the day
     4. Utilize the [debord-buendia program](https://pensinspace.net/protodrew/the-debord-buendia-program) to reconcile my emotions and embrace transcendentalism.

I will post one update halfway through the season, and one update at the end of the season discussing how this undertaking went.
