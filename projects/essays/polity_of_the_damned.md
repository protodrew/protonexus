---
type:
tags: 
creation-date: 2023-07-02
modification-date: 2023-11-14
---
# Polity of the Damned

> Men make their own history, but they do not make it as they please; they do not make it under self-selected circumstances… The tradition of all dead generations weighs like a nightmare on the brains of the living. And just as they seem to be occupied with revolutionizing themselves and things, creating something that did not exist before, precisely in such epochs of revolutionary crisis they anxiously conjure up the spirits of the past to their service, borrowing from them names, battle slogans, and costumes in order to present this new scene in world history in time-honored disguise and borrowed language.

We live in a time where to many it seems hopeless. Surrounded by the final belches of decaying empires & on the brink of environmental and social collapse, the sedation of our society seems the most damning of all. Communities that form around wanting to make the world a better place will wax poetic about utopian ideals and imagining how things could have gone if they were different. But they didn't and wont be different by continuing to refine a mirage of a better world.

This is not to say that envisioning the utopia you wish to strive toward is worthless, only by positioning ourselves within a struggle towards a better world may we call upon the streoongth of our revolutionary forefathers and continue to utilize the inertia they have left for us. Revolutionary politics, however, are not dogma and do not exist in a vacuum. As the internet continues to seep it's way into the congregation and conversation of revolutionaries, we find ourselves lost in opining for a better world without actually making steps to move forward.

When the labor movements in the United States ebbed, corporations seeped in to innovate in our exploitation, one where work is more erratic, and self definable. Cloaked in freedom and choice, this new form of work proudly touts it's working hours as less than the 19th century, but fails to remind us that we work on average 500 hours more per year than the average 14th century serf. The workday has lost it's clear boundaries with our day-to-day lives, and the need for many to take multiple part time jobs to make ends meet (since many companies choose to only hire part time in order to prevent giving employees benefits) has only further blurred the lines. Average working hours did go down 2.1% from 1979 to 2015, which represents the lowest decrease of any 1st world country, largely comparing favorably to those we colonized and underdevloped.

This is not an accident. The further atomization of the modern worker from both their co-workers and community members through the automobile, increased rhetoric on personal freedoms and "right to work", as well as technocratic serfdom couched in a new "gig economy" has crippled the abilities of the modern activist to connect with their community.