---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-12-26
---
# What You Can Do to Help OpenStreetMap

If you aren't familiar with Open Street Map, you've probably at least used something that relies on them. They create some of the best mapping out there and release it all under the [Open Data Commons Open Database License](https://opendatacommons.org/licenses/odbl/), and these maps are completely volunteer supported. Here's how you can help make them better.

## Step One: Use Their App

this one seems like a no-brainer, download [OsmAnd](https://osmand.net/) or [Organic Maps](https://organicmaps.app/) (my preference) if you are on android or IOS, and start using it instead of Google Maps whenever you can. Keep in mind that they are always improving and to make liberal use of that feedback button in the help menu.

## Step Two: Contributing Data

There are a ton of ways to contribute to OpenStreetMap. Their [wiki](https://wiki.openstreetmap.org/wiki/Beginners%27_guide) provides a ton of different ways you can contribute, but I will show you my favorite two below.

### Step Two A: OsmGo

[OsmGo](https://OSMGO.com) is an android app that allows you to easily click things around you and edit their information, it can be as simple as adding the hours of your favorite restaurant, or as complex as adding an entire city block of data. They have a quick little [guide](https://dofabien.github.io/OsmGo/) you can check out if you are interested. I have used this for months and its a great little thing to do instead of checking Reddit or the tweetstagrams.

### Step Two B: Street Complete

[Street Complete](https://github.com/streetcomplete/StreetComplete) makes this process even easier. From their Github:

> StreetComplete automatically looks for nearby places where a survey is needed and shows them as quest markers on its map. Each of these quests can then be solved on site by answering a simple question. For example, tapping on a marker may show the question "What is the name of this road?", with a text field to answer it. More examples are shown in the screenshots below.
>
> The user's answer is automatically processed and uploaded directly into the OSM database. Edits are done in meaningful changesets using the user's OSM account. Since the app is meant to be used on a survey, it can be used offline and is economic with data usage.

This is my personal favorite way to help out. I filtered out the one's about pavement materials or streetlights, and now I can go around adding accessibility guides, small configurations, or hours to things around me. It also has a simple little badge system that really tickles the collector part of my brain.

## Step Three: Marketing

Tell your friends! Share them this article, or just convince them to make the switch away from Google! The 7 million + contributors of OpenStreetMap will thank you, and we love to see people using our work!

## Step Four: Donate

Enough said. [Kick em a few bucks](https://donate.openstreetmap.org/)… The've earned it.

Honestly I just love OpenStreetMap, its so easy and nice and has a huge [app ecosystem](https://wiki.openstreetmap.org/wiki/Software#Mobile_software_by_platform) that has feature rich beautiful apps, and hyperspecific apps for hikers and bikers alike. I'm sure you will find something you love using and hopefully will join me and the rest of the gang in contributing to the map.
