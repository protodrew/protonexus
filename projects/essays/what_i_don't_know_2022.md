---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# What I Don't Know
## 2022

Continuing my personal recap series from last year (two's a pattern), I am reflecting on what I learned this year, what I would like to learn next year, as well as my yearly theme¹ from last year and my What I don't Know² 2021 post.

### Constructing the Foundation

Constructing the Foundation was the name of my theme in 2022. A markdown file summarizing it can be found on my codeberg¹. In short, I wanted to focus on building positive aspects of myself rather than criticizing parts of myself I don't like. I would overall call this year a success with my theme, but I don't have another complete theme year to compare it to.

That in general was an important goal for me, I was able to maintain my journaling, theme, and general mental health for the whole year. I haven't made progress in all the areas I wanted, but I haven't gone backwards anywhere, and I want to congratulate myself for that.

- Focus more on the parts of myself I like, and how to expand them, over the parts I don't and how to cut them out
- Build good habits rather than trying to break bad ones
- Set realistic goals and be ok with failing
- Building the parts of myself I want to maintain going forward

These were the goals I set up for myself over the year. I would say I succeeded in a lot of aspects, but have a lot I want to improve on in 2023. It was a hectic year for me, I dropped out of college, got and lost several jobs, struggled with a lot of mental illness, and I am leaving the year in similar health and worse finances than the start. However, I have learned a lot about myself, made some cool experiments, developed my skills, and found new priorities.

I have improved my self-image a lot, and refined an aesthetic that I think fits my vibe, compliments my body, and wardrobe. I have figured out my mental health in a lot of new ways, and even though I have not been able to find a long-term working relationship with a therapist, I am still on the hunt and proud of the individual progress that I have made.

### What I Learned

#### Raylib

The technical thing I spent the most time on this year outside of classes was probably learning raylib. I tinkered around with it quite a bit in both lua and C, and want to learn even more beyond the basic rendering of shapes. Getting into generative art is something I've envied for years, and I think it is something I am going to prioritize in 2023.

#### Godot

Godot remains my favorite way to make games, and I'm loving it more and more over time. In 2023, I'm going to solidify a lot of the non 'coding-based' projects I work on to be in godot. I learned a lot about the built-in systems and also experimented with some of the Godot 4 betas (struggling to make that work on linux w/ wayland, but such is life).

#### Writing

I started writing a little bit of fiction with my architecture game, and it has been a fun little experiment. I am proud of what I've done and excited to keep working on it.

I also wrote 8 posts on my blog, 9 if you count this one. I've been really enjoying writing blog posts and have a lot of ideas for future ones, so I hope to do at least 12 this upcoming year.

#### Reading

I started reading more articles, books, and papers this year. I've found that I always want to have notes on something, but actually taking notes on them at the moment makes it harder to read the actual thing, so reading it always comes first.

### What I Don't Know

#### Architecture

I want to really deepen my knowledge of architecture over the next year. Both as research for my potential future project, and just out of a genuine interest. I really want to focus on regenerative and revolutionary architecture from various eras, but also want to reinforce my understanding of traditional architecture beyond what I learned in art history like 4 years ago.

#### Genart

I really want to learn more complex and interactive generative art. Raylib seems like the easiest tool to work with in my experience, and so it is going to be something I think about a lot in 2023. I want to mess around with it mostly in C, but if I keep learning Lua or forth I might see what me options are from there. I am also looking really closely at uxn, and maybe making learning the ins and outs of its system the focus for my 2023 technical journey, who knows.

#### Programming

I am not as advanced of a programmer as I would like to be, and struggle with making and completing projects as obstacles come up. With that in mind, I want to make and complete several technical projects this year, even if they are just tiny shell scripts or little problem-solvers. It feels good to learn new technical things and to develop them over time, and I want to keep doing it.

#### Music Production

I used to make ambient music under the name cvlt_th1nk, and when I switched to Linux I stopped making music as The DAW I learned doesn't work on Linux (it was a copy of propellerhead reason my cousin had). I want to start making music again, and have had ideas for tunes all the way from hyperpop stuff to field recording ambient music. I have been looking a lot at bitwig as a potential tool as it looks a lot like Ableton which had always seemed like a sensible interface to me, but I'm not sure what I'll use. All I know is I want to make some cool music this year.

---

And that's it. It's been a rocky year, but I'm thankful for everyone I have in my life and around me for making 2022 so much better. I hope to have all sorts of podcasts, blog posts, games, and music coming at you so make sure to check my mastodon³ for updates! I hope you have a good 2023, and I'll see you need year with another "what I don't know"

---

[1. 2022 Theme](https://codeberg.org/protodrew/protonexus/src/branch/main/personal/2022_theme.md)

[2. What I don't know 2021](https://blog.protodrew.website/what_i_dont_know_2021)

[3. @protodrew@merveilles.town](https://merveilles.town/@protodrew)