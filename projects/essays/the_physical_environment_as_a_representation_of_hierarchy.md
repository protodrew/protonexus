---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# The Physical Environment as a Representation of Hierarchy

I am by no means the first person to discuss this, but I wanted to articulate my thoughts and propose the solution I see in an abstract sense and what we can do to achieve that in the modern world.

<!--more-->

I have been taking part in [Harvard University's free online courses](https://online-learning.harvard.edu/catalog?keywords=&paid%5B1%5D=1&max_price=&start_date_range%5Bmin%5D%5Bdate%5D=&start_date_range%5Bmax%5D%5Bdate%5D=) as a way to both expand my current resume, and to prepare myself for college as a part of [The season of Structure](https://pensinspace.net/protodrew/seasonal-theme-creation). Many of these classes have been really exciting and enlightening, and I feel like it is far more engaging than the average online class. One of the classes I have enjoyed the most is [The Past, Present and Future of Urban Life](https://online-learning.harvard.edu/course/citiesx-past-present-and-future-urban-life), and it has made my thoughts on unitary urbanism and societal engineering resurface. As a way to reconcile these thoughts I am going to write this article explaining the problems with capitalist urban/suburban/rural structuring, and propose a few steps we can make to achieve my vision of utopia.

<h2> What is Unitary Urbanism?</h2>

First we need to get some terminology out of the way. Unitary Urbanism, as defined by wikipedia is "the critique of status quo "urbanism", employed by the Letterist International and then further developed by the Situationist International between 1953 and 1960." I believe this definition leaves a lot to be desired. Unitary Urbanism is the belief that urban spaces should be defined by the needs of the people within them, and exist as a comprehensive space that allows for comprehensive experience and personal impact on the space you inhabit. Now that is a bit dense, but basically it is about removing the current urban division of "sectors" (the market square, the office park, the apartment complex) and removing the divide from capital and the people who inhabit the city.

This cannot and should not be attempted within a capitalist framework, as it will simply lead to a lack of definition of work and personal time that will be utilized by capitalists to squeeze more efficiency out of the working class. This must exist in a system that not only does not perpetuate hierarchy, but actively dismantles previous hierarchy, and that includes the modern city structure.

<h2> How Does the Urban Landscape Perpetuate Hierarchy?</h2>

This is a pretty obvious answer, but it is often taken as a given in society. You have the nice side of town and the not so nice side of town. However, these two sides are not organically brought about due to the people that live in them, but rather shaped by those in power to favor those that they believe should live comfortably, and comfort for one in a capitalist system comes at the cost of the discomfort of another.

This manifests in the poor neighborhood (which also happens to coincide with a non-white neighborhood for some weird reasons), the gerrymandered district (which again takes a racial skew for some reason), and the positioning of social services and services in general. This form of hierarchy has manifested in the concept of property values, where the neighborhood becomes a self perpetuating cycle of decay as the houses get worse and so their value gets lower, all until some up and coming developer buys the land, bulldozes it, and makes it into a trendy whole foods.

Shockingly, in a society that values white people more than others, our cities prefer white values, white aesthetics, & white neighborhoods. This does not function in an egalitarian society, and as such should be dismantled in an urbanist utopia.

<h2>What is to be done?</h2>

In the abstract Unitarian Urbanism addresses this through the concept of having the citizens create the space they live in. This doesn't mean physically building the apartment you are going to live in, but rather removing the detachment of art from structure, and no longer prioritizing one specific aesthetic. Urban landscapes should not prioritize one type of person, and as such Urban engineering should not prioritize one type of person.

In the concrete, there are movements that represent the interests of more people growing across the international stage. Tenants Unions, Art Collectives, Anarchist Communes, Food Not Bombs, Trade Unions, and the [YIMBY](https://en.wikipedia.org/wiki/YIMBY) (short for yes in my backyard) movement are all great systems that are beginning to restructure society from within that society, and if you wish to build an egalitarian urban society you should participate in those. But you've heard of all that before. Whats important is the way you perceive society and having you radically space the shape you live in. Plant trees on your walk to work so eventually you'll have more shade, fight against gentrification in your area, Organize against your landlord to prevent exploitation, Create art that mimics the aesthetic you want to see in the space around you.

Unitary Urbanism is more than a non euclidean pipe dream, it is a mindset that must be utilized to dismantle the racial, class, and social hierarchy that has been embedded in the urban structure since the Roman empire.
