---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-11-14
---
# Animal Crossing Thoughts

Overall I have really been enjoying ACNH so far and am extremely excited to sink as much time into it as I did New Leaf. Here are my thoughts after about 10 hours. <!--more-->

<h2>The Great</h2>

- The art style is incredibly charming and the music accompanies both it and the island setting amazingly.
- The voice chatter is charming and refined, and the tonal differences between both the animals and what they are doing (speaking through a PA, talking to someone else) is a great continuation of what it has been.
- The core gameplay loop is very faithful to the originals (which I love but you might not), but they have given you more to do with the products of those activities.
- Diversity in gender, both in its fluidity and lack of denotation, is super nice, it is far removed from gendering the words cool and cute in New Leaf to denote what gender the player has at the start.

<h2>The Meh</h2>

- I could take or leave tool durability, and for my tastes, it could be a bit more forgiving, but I do recognize its utility to both give more of an incentive to craft, to not just instantly give the player the best tools, and to discourage the player from just selling every resource they get
- I think the animals are sometimes lacking in dynamic speech, and while it is improved from other games, I feel like they could go further than just observations of things you are wearing or something they want you to do.
- The concept of direction in the game is still incredibly finicky, and it is emphasized whenever you are fishing and just miss where the cast should go, or are digging for something but are facing a bit too away from it.
- Navigating menus in the game is clunky at the beginning, there are not a lot of settings you can tweak and you have to do them 1 at a time for some reason (KK being there makes it better though).

<h2>The Bad</h2>

- There are very few indications of what you can or can't do in certain menus or indoors vs outdoors (ex: you can't put an item from your inventory into storage in the storage menu, you have to do it in the inventory menu)
- The crafting system is very annoying if you want to make a lot of a consumable item (fish bait AHHHHH)
- The tool wheel, while incredibly useful, is extremely difficult to use due to the precision it requires with the left stick, and I can especially see this being a problem for people with disabilities, and I wish the game would have allowed for more accessibility options in general.
