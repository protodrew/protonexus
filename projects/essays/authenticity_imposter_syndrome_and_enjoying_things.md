---
type:
tags: 
creation-date: 2023-06-16
modification-date: 2023-12-26
---
# Authenticity, Imposter Syndrome, and Enjoying Things

I think about other people a lot. Not any specific individuals, not even groups, just the vague amorphous reactions of "others" if I talked about certain things. I think about how they will form opinions of me, but more often than not I'm thinking about it in an even more meta context. Am I enjoying things authentically, or just enjoying them because I think other people will like them?

The first time I came into contact with this idea was when I became a fan of contemporary art and minimalist art, specifically the works of Cy Twombly, Rothko, Barnett Newman, and Carl Andre. I heard people deriding it as something that people like just to seem smart, and since I was at a very self-conscious point in my life, I haven't really been able to shake that.

I was re-watching the hilarious video of Zizek talking about putting a plastic dildo into a plastic vagina to satisfy the superego and just get on with a date, and he said something at the beginning I forgot about. "Even when we are alone, we are to some extent preforming 'ourselves'". He reads this as a good thing, believing that people are by and large monsters (somewhat humorously); I on the other hand, have a harder time grappling with it.

It isn't necessarily that I want to portray a completely honest version of myself at all times, or that authenticity is something I look for in everything I engage with. However, I have had periods of my life where I was dishonest about the character of myself in order to satisfy what I thought was a deficit in my personality. This is something I have been able to by and large move on from, but I'm always thinking about it. In moments of particular self-doubt or self-loathing, I have a way to deride anything I enjoy. I am reading this polished New York Times bestseller to appear smarter, I am reading this zine to feel punk, I am listening to underground music because I want to be indie. None of these things characterize me to myself, but they are thoughts that I grapple with frequently, and I am guessing I'm not alone in this.

I hear a lot of people refer to things as guilty pleasures, particularly in the context of media, and it is something I have deliberately tried to separate myself from, because I don't want to qualify the things I enjoy in order to express things about them to someone else. I feel like instead of unlearning this concept, I moved the conversation completely internally, and it is something I have to grapple with every day. I want to move on from it, because at times it legitimately hampers my enjoyment of things, and I want to stop living for others and start truly living for myself when it comes to my tastes and enjoyment.

Links:

[that zizek talk]((https://www.youtube.com/watch?v=7xYO-VMZUGo)
