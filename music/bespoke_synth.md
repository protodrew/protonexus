---
type: note
tags: [music, projects]
creation-date: 2023-10-23
modification-date: 2024-01-07
aliases: [bespoke]
---
# Bespoke Synth

Bespoke is a software modular synthesizer, daw, & livecoding environment. I have worked with it quite a bit in the past, and got involved with them during Google Summer of Code 2022, but ended up parting ways after it proved more complex than I was expecting. That compounded with some mounting anxieties and stressors to make it something that I felt too bad to keep working with it. A year and a bit later, and I have returned it with a desire to get back into making ambient and noise music, so this is my note for any useful tips and tricks.

- shift-clicking on a wire start point will duplicate the wire, useful for sending one note or pulse to many locations
- alt-clicking will duplicate a node, *including its connection endpoint*
- the settings menu itself is a node and can have all of its values dynamically changed
- when you save a file it will not automatically add the file extension to the name so make sure to type out .pfb and .bsk so it can find them earlier
- for quick melody midi you can generate a pattern in a drum sequencer and scale it up a couple octaves
- use a slow note sequencer sending to several chorders for easy accompaniment
- ~ brings up the dev console, and typing `home` will recenter you

#todo 
- [ ] see if I can route record player into [[bespoke_synth]] and record scratch some stuff. 